<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => "Jaime Sánchez",
                'email' => "admin@corporativoventura.mx",
                'password' => bcrypt('S3ST2M1S'),
                'status' => 1,
            ],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;


class ProgramacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('evento_tipos')->insert([
            [
                'nombre'=>'Literario',
                'tipo'=>'Programación',
                'status'=>'1'
            ],
            [
                'nombre'=>'Juvenil',
                'tipo'=>'Programación',
                'status'=>'1'
            ],
            [
                'nombre'=>'Chamacos',
                'tipo'=>'Programación',
                'status'=>'1'
            ],
            [
                'nombre'=>'Bebeteca',
                'tipo'=>'Programación',
                'status'=>'1'
            ],
            [
                'nombre'=>'Talleres',
                'tipo'=>'Programación',
                'status'=>'1'
            ],
            [
                'nombre'=>'Visitas escolares',
                'tipo'=>'Programación',
                'status'=>'1'
            ],
            [
                'nombre'=>'Suena la FILO',
                'tipo'=>'Programación',
                'status'=>'1'
            ],
            [
                'nombre'=>'Profesionales',
                'tipo'=>'Programación',
                'status'=>'1'
            ],
            [
                'nombre'=>'Academico',
                'tipo'=>'Programación',
                'status'=>'1'
            ],
            [
                'nombre'=>'Presentacion editorial',
                'tipo'=>'Tipo evento',
                'status'=>'1'
            ],
            [
                'nombre'=>'Mesa',
                'tipo'=>'Tipo evento',
                'status'=>'1'
            ],
            [
                'nombre'=>'Conferencia',
                'tipo'=>'Tipo evento',
                'status'=>'1'
            ],
            [
                'nombre'=>'Espectaculo',
                'tipo'=>'Tipo evento',
                'status'=>'1'
            ],
            [
                'nombre'=>'Lecturas',
                'tipo'=>'Tipo evento',
                'status'=>'1'
            ],
            [
                'nombre'=>'Talleres',
                'tipo'=>'Tipo evento',
                'status'=>'1'
            ],

        ]);
    }
}


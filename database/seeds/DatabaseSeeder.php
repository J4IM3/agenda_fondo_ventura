<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*
        $this->call(ProgramacionTableSeeder::class),
        $this->call(ForosTableSeeder::class),
        $this->call(AerolineasTableSeeder::class)
        */
        $this->call([
            UsersTableSeeder::class,
            ProgramacionTableSeeder::class,
            SedesTableSeader::class,
            ForosTableSeeder::class,
            AerolineasTableSeeder::class,
            HotelTableSeeder::class,
        ]);
    }
}

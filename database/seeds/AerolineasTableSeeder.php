<?php

use Illuminate\Database\Seeder;

class AerolineasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('aerolineas')->insert([
            [
                'name'=>'Interjet',
                'status'=>1
            ],
            [
                'name'=>'Aeromexico',
                'status'=>1
            ],
            [
                'name'=>'Delta',
                'status'=>1
            ],
            [
                'name'=>'Volaris',
                'status'=>1
            ]
        ]);
    }
}

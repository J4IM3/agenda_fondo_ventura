<?php

use Illuminate\Database\Seeder;

class HotelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hotels')->insert([
           [
               'name'=>'Restaurante Catedral',
               'direccion'=>'García Vigil N°105, Col. Centro',
               'telefono'=>'516 3285',
               'contacto'=>'Carlos de Jesús Díaz',
               'tipo'=>'restaurant',
               'status'=>1
           ],
            [
                'name'=>'Sabina Sabe ',
                'direccion'=>'5 de mayo N°209, Col. Centro',
                'telefono'=>'514 3494',
                'contacto'=>'Bélgica Martínez',
                'tipo'=>'restaurant',
                'status'=>1
            ],
            [
                'name'=>'Hotel  Boutique Casa Oaxaca',
                'direccion'=>'García Vigil No. 407, Col Centro',
                'telefono'=>'51 4 41 73',
                'contacto'=>'Betsaida Mariscal',
                'tipo'=>'hotel',
                'status'=>1
            ],
            [
                'name'=>'Hotel One',
                'direccion'=>'Calz. De la República No. 205 Col. Centro',
                'telefono'=>'501 6300',
                'contacto'=>'Patricia ',
                'tipo'=>'hotel',
                'status'=>1
            ],
        ]);
    }
}

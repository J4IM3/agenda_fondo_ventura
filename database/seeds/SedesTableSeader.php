<?php

use Illuminate\Database\Seeder;

class SedesTableSeader extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sedes')->insert([
            [
                'name'=>'Centro de Convenciones',
                'direction'=>'Santa Lucia',
                'status'=>'1'
            ],
        ]);
    }
}

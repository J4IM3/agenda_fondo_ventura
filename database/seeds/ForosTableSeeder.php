<?php

use Illuminate\Database\Seeder;

class ForosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('foros')->insert([
            [
                'nombre'=>'Foro FILO',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1,
            ],
            [
                'nombre'=>'Foro Panóramico',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ],
            [
                'nombre'=>'Foro 2 - Gran Salón ',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ],
            [
                'nombre'=>'Foro 3 - Gran Salón',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ],[
                'nombre'=>'Foro Juvenil ',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ],[
                'nombre'=>'Foro El Recreo',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ],[
                'nombre'=>'Foro Chamacos',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ],[
                'nombre'=>'Foro Audiovisual',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ],[
                'nombre'=>'Bebeteca',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ]
            ,[
                'nombre'=>'Bebeteca 2 ',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ],
            [
                'nombre'=>'Talleres',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ],
            [
                'nombre'=>'Pabellón IEEPO',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ],
            [
                'nombre'=>'Suena la FILO',
                'status'=>'1',
                'type'=>'Presencial',
                'sede_id' => 1
            ],
        ]);
    }
}

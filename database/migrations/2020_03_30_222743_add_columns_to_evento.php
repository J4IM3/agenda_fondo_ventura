<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToEvento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eventos', function (Blueprint $table) {
            $table->string('ruidoso')->nullable();
            $table->string('prueba_sonido')->nullable();
            $table->integer('visible')->default(1)->comment('1 para visualizar el evento en el programa 0 ocultarlo')	;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eventos', function (Blueprint $table) {
            $table->string('ruidoso');
            $table->string('prueba_sonido');
            $table->integer('visible')->default(1)->comment('1 para visualizar el evento en el programa 0 ocultarlo')	;
        });
    }
}

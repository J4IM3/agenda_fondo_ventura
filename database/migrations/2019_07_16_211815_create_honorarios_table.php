<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHonorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('honorarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('invitado_id');
            $table->float('honorarios')->nullable();
            $table->float('monto_hotel')->nullable();
            $table->float('monto_alimentos')->nullable();
            $table->float('monto_viaticos')->nullable();
            $table->float('monto_transporte')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('honorarios');
    }
}

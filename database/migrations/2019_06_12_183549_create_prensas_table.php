<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrensasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prensas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('invitado_id')->unsigned();
            $table->foreign('invitado_id')->references('id')->on('invitados');
            $table->string('participante')->nullable();
            $table->string('medio_id')->nullable();
            $table->string('entrevistador')->nullable();
            $table->date('fecha');
            $table->time('hora_inicio');
            $table->time('hora_fin');
            $table->integer('tipo_evento_id');
            $table->string('lugar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prensas');
    }
}

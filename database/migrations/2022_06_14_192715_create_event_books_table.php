<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo',200);
            $table->boolean('is_presenter')->nullable();
            $table->integer('event_id');
            $table->integer('quantity_for_presenter');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_books');
    }
}

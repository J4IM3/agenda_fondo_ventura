<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsDetailsOrigenToItinerariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerarios', function (Blueprint $table) {
            /*$table->date('fecha_origen_salida_internacional')->nullable();
            $table->time('hora_origen_salida_internacional')->nullable();*/
            $table->date('fecha_llegada_internacional_origen')->nullable();
            $table->time('hora_llegada_internacional_origen')->nullable();
            $table->string('aerolinea_retorno_internacional_id')->nullable();
            $table->string('num_vuelo_retorno_internacional')->nullable();
            $table->string('destino_internacional')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerarios', function (Blueprint $table) {
            $table->date('fecha_origen_salida_internacional')->nullable();
            $table->time('hora_origen_salida_internacional');
        });
    }
}

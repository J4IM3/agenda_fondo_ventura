<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDetallesOriginToItinerariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerarios', function (Blueprint $table) {
            $table->date('fecha_origen_salida')->nullable();
            $table->time('hora_origen_salida');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerarios', function (Blueprint $table) {
            $table->date('fecha_origen_salida');
            $table->time('hora_origen_salida');
        });
    }
}

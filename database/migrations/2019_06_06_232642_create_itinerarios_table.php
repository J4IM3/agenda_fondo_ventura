<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItinerariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itinerarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('invitado_id')->unsigned();
            $table->foreign('invitado_id')->references('id')->on('invitados');
            $table->integer('aerolinea_internacional_id')->unsigned()->nullable();
            $table->foreign('aerolinea_internacional_id')->references('id')->on('aerolineas');
            $table->string('num_vuelo_internacional')->nullable();
            $table->date('fecha_salida_internacional_origen')->nullable();
            $table->time('hora_salida_internacional_origen')->nullable();
            $table->date('fecha_regreso')->nullable();
            $table->time('hora_regreso')->nullable();
            $table->string('origen_internacional')->nullable();

            $table->string('origen');
            $table->integer('aerolinea_id')->unsigned()->nullable();
            $table->foreign('aerolinea_id')->references('id')->on('aerolineas');
            $table->string('num_vuelo')->nullable();
            $table->date('fecha_llegada');
            $table->time('hora_llegada')->nullable();
            $table->date('fecha_salida');
            $table->time('hora_salida')->nullable();


            $table->integer('hotel')->unsigned();
            $table->foreign('hotel')->references('id')->on('hotels');
            $table->integer('estancia');
            $table->integer('anfitrion_id')->nullable();
            $table->longText('observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itinerarios');
    }
}

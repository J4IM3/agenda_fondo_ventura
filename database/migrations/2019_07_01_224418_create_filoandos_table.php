<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiloandosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filoandos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('institucion_id');
            $table->string('nivel');
            $table->date('fecha');
            $table->time('hora_inicio_evento');
            $table->time('hora_final_evento');
            $table->string('direccion');
            $table->string('contacto');
            $table->string('telefono_contacto');
            $table->enum('tipo_transporte',['Filo','Institucion']);
            $table->longtext('requerimientos');
            $table->string('ubicacion_inicial');
            $table->string('ubicacion_final');
            $table->time('hora_traslado_ida');
            $table->time('hora_traslado_regreso');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filoandos');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha');
            $table->time('hora');
            $table->time('hora_final');
            $table->string('actividad');
            $table->string('lugar');
            $table->integer('invitado_id');
            $table->string('ciclo')->nullable();
            $table->string('tipo_evento')->nullable();
            $table->string('tipo_programa')->nullable();
            $table->string('moderadores')->nullable();
            $table->string('presentadores')->nullable();
            $table->string('descripcion')->nullable();
            $table->string('invitados');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividades');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramasTems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programa_temps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->string('info');
            $table->string('participantes');
            $table->string('lugar');
            $table->string('modera');
            $table->string('presentador');
            $table->enum('categoria',['Literario','Jóvenes','Chamacos','Taller','Visitas escolares','Suena la FILO','Profesionales','Académico','Presentación editorial','Mesa','Conferencia','Espectaculo','Lectura','Talleres','Bebeteca','Charla ','Presentación editorial externa','Rodada']);
            $table->dateTime('fini');
            $table->time('hora_ini');
            $table->string('ciclo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programa_temps');
    }
}

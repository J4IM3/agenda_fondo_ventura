<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddColumnToitinerarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itinerarios', function (Blueprint $table) {
            $table->date('llegada_estancia')->nullable();
            $table->date('salida_estancia')->nullable();
            $table->string('transporte_retorno')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itinerarios', function (Blueprint $table) {
            //
        });
    }
}

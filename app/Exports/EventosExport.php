<?php

namespace App\Exports;

use App\Http\Entities\Eventos;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class EventosExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $frase = "";
        $fraseModera = "Modera";
        $fraseProgramacion = "A cargo de ";
        $frasePresenta = "Presenta";
        $eventos = Eventos::with(
            'invitados',
            'programacionTipo','programacion',
            'moderador.moderadoresNombre','presentador')
            ->orderBy('fecha','ASC')
            ->orderBy('hora_inicio','ASC')
            ->get();

        foreach ($eventos as $evento)
        {
            foreach ($evento->invitados as $invitado) {
                $participantes[] = $invitado->invitadosNombre[0]->slug;
                $evento->participantes = implode(", ", $participantes);
            }

            foreach ($evento->sedes as $sedes)
            {
                $foros[] = $sedes->sedesNombre->nombre;
                $evento->sedes = implode(", ", $foros);
            }

            foreach ($evento->moderador as $value)
            {
                $moderador[] = $value->moderadoresNombre[0]->name;
                $evento->modera = implode(", ", $moderador);
            }

            foreach ($evento->presentador as $item)
            {
                $presentador[] = $item->presentadores[0]->name;
                $evento->presenta = implode(", ", $presentador);
            }

            unset($participantes);
            unset($moderador);
            unset($presentador);
            unset($foros);

            if ($evento->programacion->nombre == "Espectaculo")
            {
                $evento->participantesFrase = $fraseProgramacion.": ".$evento->participantes.". ";
            }else{
                $evento->participantesFrase = $evento->participantes.". ";
            }

            if (!empty($evento->modera))
            {
                $evento->moderaFrase = $fraseModera.": ".$evento->modera." .";
            }else{
                $evento->moderaFrase = "";
            }

            if (!empty($evento->presenta))
            {
                $evento->presentaFrase = $frasePresenta.": ".$evento->presenta." .";
            }else{
                $evento->presentaFrase = "" ;
            }
        }

        foreach ($eventos as $evento)
        {
            $evento->participantesTxt = $evento->participantesFrase." ".$evento->presentaFrase." ".$evento->moderaFrase;
        }
        return $eventos;
    }

    public function map($row): array
    {
        return [
            $row->nombre_evento,
            $row->descripcion,
            $row->programacionTipo->nombre,
            $row->programacion->nombre,
            $row->fecha,
            $row->hora_inicio,
            $row->hora_fin,
            $row->ciclo,
            $row->formato,
            $row->participantesTxt,
            $row->sedes,
        ];
    }

    public function headings(): array
    {
        return [
            'Nombre',
            'Descripcion',
            'Programacion',
            'Tipo de evento',
            'Fecha',
            'Hora inicio',
            'Hora fin',
            'Ciclo',
            'Formato',
            'Participantes',
            'Sedes',
        ];
    }
}

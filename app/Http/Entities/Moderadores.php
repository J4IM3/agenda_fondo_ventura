<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Moderadores extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'moderador_id', 'evento_id'
    ];

    public function moderadores_eventos()
    {
        return $this
            ->hasMany(Eventos::class,'id','evento_id');
    }

    public function moderadoresNombre()
    {
        return $this
            ->hasMany(Invitado::class,'id','moderador_id');
    }

    public function moderadores()
    {
        return $this
            ->belongsTo(Eventos::class);
    }

    /**
     *
     */

    public function invitadoModerador()
    {
        return $this
            ->belongsTo(Invitado::class);
    }

    public function moderadorEventos()
    {
        return $this
            ->hasOne(Eventos::class,'id','evento_id');
    }
}

<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class EventoTipo extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','nombre','status','tipo'
    ];

    public function programacion(){
        return $this
            ->belongsTo(Eventos::class);
    }

}

<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class FiloandoInvitados extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','invitado_id','evento_id'
    ];

    public function invitado(){
        return $this
            ->belongsTo(Eventos::class);
    }

    public function invitadosNombre(){
        return $this
            ->hasMany(Invitado::class,'id','invitado_id');
    }

    public function eventos(){
        return $this
            ->hasMany(Eventos::class,'id','evento_id');
    }

    public function filoando()
    {
        return $this
            ->belongsTo(Filoando::class);
    }

    public function eventosFiloando()
    {
        return $this
            ->hasMany(Filoando::class,'id','evento_id');
    }

    public function filoandoItems()
    {
        return $this
            ->hasOne(Filoando::class,'id','evento_id');
    }

}

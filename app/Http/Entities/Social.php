<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Social extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','invitado_id','lugar_id','hora_inicio','fecha','hora_fin','actividad','traslado_ida','traslado_regreso'
    ];

    public function socialInvitado(){
        return $this
            ->hasMany(Invitado::class,'id','invitado_id');
    }

    public function socialHotel(){
        return $this
            ->hasOne(Hotels::class,'id','lugar_id');
    }

    public function InvitadoSocial(){
        return $this
            ->belongsTo(Invitado::class);
    }
}

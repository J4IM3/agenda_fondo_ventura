<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 05/08/2019
 * Time: 06:39 PM
 */

namespace App\Http\Entities;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
class Medios extends AuthenticaTable
{
    use Notifiable;
    protected $fillable = [
        'id','nombre','status'
    ];

    public function medios()
    {
        return $this
            ->belongsTo(Prensa::class);
    }

    public function prensaMedios()
    {
        return $this
            ->belongsTo(Prensa::class);
    }

}

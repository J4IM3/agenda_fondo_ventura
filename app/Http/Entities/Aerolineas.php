<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Aerolineas extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','status'
    ];

    public function aerolinea(){
        return $this
            ->belongsTo(Invitado::class);
    }

    public function aerolineasInterRetorno()
    {
        return $this
            ->belongsTo(Itinerarios::class);
    }
}

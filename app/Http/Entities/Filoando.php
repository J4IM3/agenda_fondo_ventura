<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Filoando extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','institucion_id','nivel','fecha','hora_inicio_evento','hora_final_evento','contacto','telefono_contacto','tipo_trasnporte',
        'requerimientos','ubicacion_inicial','ubicacion_final','hora_traslado_regreso','hora_traslado_ida','direccion'
    ];

    public function foros(){
        return $this
            ->belongsTo(Eventos::class);
    }

    public function filoando()
    {
        return $this
            ->hasMany(FiloandoInvitados::class,'evento_id','id');
    }

    public function eventosFiloando()
    {
        return $this
            ->belongsTo(FiloandoInvitados::class);
    }

    public function instituciones()
    {
        return $this
            ->hasOne(Instituciones::class,'id','institucion_id');
    }
}

<?php


namespace App\Http\Entities;
use Illuminate\Database\Eloquent\Model;

class EventBooks extends Model
{
    protected $fillable = ['titulo','is_presenter','event_id','quantity_for_presenter'];
    public $timestamps = false;

    public function eventBooks() {
        return $this
            ->belongsTo(Eventos::class);
    }
}

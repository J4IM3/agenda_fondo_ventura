<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Honorarios extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','invitado_id','monto_hotel','monto_alimentos','monto_viaticos','monto_transporte','honorarios'
    ];


    public function honorarios(){
        return $this
            ->belongsTo(Itinerarios::class);
    }

    public function presupuesto(){
        return $this
            ->belongsTo(Itinerarios::class);
    }
}

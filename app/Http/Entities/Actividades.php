<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Actividades extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','fecha','hora','hora_final','actividad','lugar','invitado_id','ciclo','tipo_evento',
        'tipo_programa','moderadores','presentadores','descripcion','invitados','anfitrion','telefono_anfitrion'
    ];

}

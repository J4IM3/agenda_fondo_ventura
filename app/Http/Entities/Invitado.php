<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Invitado extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','slug','email','avatar','semblanza','sexo','phone','residencia'
    ];

    /**
     *Le cambie a has One para exportarInvitadosExcel
     */
    public function itinerarioVuelo(){
        return $this
            ->hasOne(Itinerarios::class,'invitado_id','id');
    }

    public function invitado()
    {
        return $this->belongsTo(Itinerarios::class);
    }

    public function invitadosNombre()
    {
        return $this
            ->belongsTo(Evento_participantes::class);
    }
    public function invitadoItinerario()
    {
        return $this
            ->belongsTo(itinerarios::class);
    }

    public function prensaInvitado()
    {
        return $this
            ->belongsTo(Prensa::class);
    }



    /**
     * Le hice un cambio al return
     *         ->hasMany(Itinerarios::class,'invitado_id','invitado_id');
     * estaba asi , lo cual no tenia razon por que invitado_id solo existe en la
     * tabla de itinerario
     */
    public function itinerario(){
        return $this
            ->hasMany(Itinerarios::class,'invitado_id','id');
    }
    public function itinerario2(){
        return $this
            ->hasMany(Itinerarios::class,'invitado_id','invitado_id');
    }

    public function invitadoPrensa(){
        return $this
            ->hasMany(Prensa::class,'invitado_id','id');
    }

    public function invitadoSocial(){
        return $this
            ->hasMany(Social::class,'invitado_id','id');
    }

    public function presupuesto(){
        return $this
            ->hasMany(Honorarios::class,'invitado_id','id');
    }

    public function prensa(){
        return $this
            ->belongsTo(Invitado::class);
    }

    public function socialInvitado(){
        return $this
            ->belongsTo(Social::class);
    }

    public function presentadores()
    {
        return $this
            ->belongsTo(Presentadores::class);
    }

    public function moderadoresNombre()
    {
        return $this
            ->belongsTo(Moderadores::class);
    }

    public function ParticipanteEnEventos()
    {
        return $this
        ->hasMany(Evento_participantes::class,'invitado_id','id');
    }

    public function anfitrion()
    {
        return $this->hasOne(Anfitrion::class,'id','anfitrion_id');
    }

    /**
     *
     */

    public function invitadoEventoParticipante()
    {
        return $this
            ->hasMany(Evento_participantes::class,'invitado_id','id');
    }

    public function invitadoPresentador()
    {
        return $this
            ->hasMany(Presentadores::class,'presentador_id','id');
    }

    public function invitadoModerador()
    {
        return $this
            ->hasMany(Moderadores::class,'moderador_id','id');
    }
}

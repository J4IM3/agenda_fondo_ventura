<?php

namespace App\Http\Entities;
use App\Http\Services\EventoParticipantesService;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Eventos extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id','tema','fecha','hora_inicio','hora_fin','nombre_evento','descripcion','tipo_id','formato',
        'programacion_id','ciclo','foro_id','requirimientos','status','colaborador','ruidoso','prueba_sonido','visible',
        'user_id'
    ];

    /**
     *
     * Relacion entre un eventos y los participantes en un evento
     */
    public function invitados(){
        return $this
            ->hasMany(Evento_participantes::class,'evento_id','id');
    }

    public function eventos(){
        return $this
            ->belongsTo(Evento_participantes::class);
    }

    public function foros()
    {
        return $this
            ->hasOne(Foro::class,'id','foro_id');
    }

    public function programacion()
    {
        return $this
            ->hasOne(EventoTipo::class,'id','tipo_id');
    }

    public function programacionTipo()
    {
        return $this
            ->hasOne(EventoTipo::class,'id','programacion_id');
    }

    public function eventBooks() {
        return $this
            ->hasMany(EventBooks::class,'event_id','id');

    }

    public function onlyEventsProgram()
    {
        return $this->hasOne(EventoTipo::class)
            ->whereColumn('tipo','Programación');

    }

    public function presentador()
    {
        return $this
            ->hasMany(Presentadores::class,'evento_id','id');
    }

    public function presentadores_eventos()
    {
        return $this
            ->belongsTo(Presentadores::class);
    }

    public function moderadoresEvento()
    {
        return $this
            ->belongsTo(Moderadores::class);
    }

    public function moderador()
    {
        return $this
            ->hasMany(Moderadores::class,'evento_id','id');
    }

    public function tipo()
    {
        return $this
            ->hasOne(EventoTipo::class,'id','programacion_id');
    }

    public function foroEvento()
    {
        return $this
            ->belongsTo(Foro::class);
    }

    public function soundTest()
    {
        return $this
            ->hasOne(PruebaSonido::class,'evento_id','id');
    }

    public function eventSoundTest()
    {
        return $this
            ->belongsTo(PruebaSonido::class);
    }

    public function sedes()
    {
        return $this
            ->hasMany(DetailEvents::class,'event_id','id');
    }

    /**
     *
     */

    public function invitadoParticipanteEvento()
    {
        return $this
            ->belongsTo(Eventos::class);
    }

    public function invitadoParticipanteEventoDetalles()
    {
        return $this
            ->hasMany(DetailEvents::class,'event_id','id');
    }

    public function presentadorEvento()
    {
        return $this
            ->belongsTo(EventoParticipantesService::class);
    }

    public function presentadorEventoDetalle()
    {
        return $this
            ->hasMany(DetailEvents::class,'event_id','id');
    }

    public function moderadorEventos()
    {
        return $this
            ->belongsTo(Moderadores::class);
    }
    public function moderadorEventoDetalle()
    {
        return $this
            ->hasMany(DetailEvents::class,'event_id','id');
    }


}

<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Programa_temp extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','titulo','participantes','lugar','info','modera','categoria','fini','hora_ini','ciclo','presentador'
    ];

    public function anfitrion()
    {
        return $this
            ->belongsTo(itinerarios::class);
    }
}

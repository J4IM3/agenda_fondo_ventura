<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Itinerarios extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','invitado_id',
        'origen_internacional','aerolinea_internacional_id','num_vuelo_internacional',
        'fecha_salida_internacional_origen','hora_salida_internacional_origen',
        'fecha_llegada_internacional_origen','hora_llegada_internacional_origen',

        'destino_internacional',
        'aerolinea_retorno_internacional_id',
        'num_vuelo_retorno_internacional',
        'fecha_regreso', 'hora_regreso',
        'origen','aerolinea_id','num_vuelo','hora_llegada','fecha_llegada',
        'fecha_salida','hora_salida','hotel','estancia','anfitrion_id','observaciones','transporte_retorno',
        'salida_estancia','llegada_estancia','origen_salida','fecha_origen_salida','hora_origen_salida',
    ];

    public function invitado(){
        return $this->hasOne(Invitado::class,'id','invitado_id');
    }

    public function itinerario(){
        return $this
            ->belongsTo(Invitado::class);
    }

    public function invitadoItinerario()
    {
        return $this
            ->belongsTo(Invitado::class);
    }

    //tenia hasMany la cambie para ocuparlo en el listado ajax de itinerario linea 160 aprox
    public function aerolineas(){
        return $this
            ->hasOne(Aerolineas::class,'id','aerolinea_id');
    }

    public function honorarios(){
        return $this
            ->hasMany(Honorarios::class,'invitado_id','invitado_id');
    }

    //tenia hasMany la cambie para ocuparlo en el listado ajax de itinerario linea 160 aprox
    public function aerolineasInter()
    {
        return $this
            ->hasOne(Aerolineas::class,'id','aerolinea_internacional_id');
    }

    public function aerolineasInterRetorno()
    {
        return $this
            ->hasOne(Aerolineas::class,'id','aerolinea_retorno_internacional_id');
    }

    public function hotel(){
        return $this
            ->hasMany(Hotels::class,'id','hotel');
    }

    public function hoteles(){
        return $this
            ->hasMany(Hotels::class,'id','hotel');
    }

    public function itinerarioVuelo(){
        return $this
            ->belongsTo(Invitado::class);
    }

    public function anfitrion()
    {
        return $this
            ->hasOne(Anfitrion::class,'id','anfitrion_id');
    }

}

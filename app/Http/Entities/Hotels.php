<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Hotels extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','direccion','telefono','contacto','tipo','status'
    ];

    public function hotel(){
        return $this
            ->belongsTo(Invitado::class);
    }

    public function socialhotel(){
        return $this
            ->belongsTo(Social::class);
    }
}

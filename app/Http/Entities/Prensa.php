<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Prensa extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','invitado_id','participante','medio_id','entrevistador','fecha','hora_inicio','hora_fin','lugar','tipo_evento_id'
    ];

    public function prensa(){
        return $this
            ->hasMany(Invitado::class,'id','invitado_id');
    }

    public function prensaInvitado(){
        return $this
            ->hasOne(Invitado::class,'id','invitado_id');
    }

    public function invitadoPrensa(){
        return $this
            ->belongsTo(Invitado::class);
    }

    public function medios()
    {
        return $this
            ->hasMany(Medios::class,'id','medio_id');
    }

    public function prensaMedios()
    {
        return $this
            ->hasOne(Medios::class,'id','medio_id');
    }

    public function prensaTipos()
    {
        return $this
            ->hasOne(PrensaTipos::class,'id','tipo_evento_id');
    }


}

<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Anfitrion extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','nombre','telefono','status'
    ];

    public function anfitrion()
    {
        return $this
            ->belongsTo(itinerarios::class);
    }

    public function anfitrionInvitado()
    {
        return $this
            ->belongsTo(Invitado::class);
    }
}

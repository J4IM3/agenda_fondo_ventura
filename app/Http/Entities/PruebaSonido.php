<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-04-14
 * Time: 18:03
 */

namespace App\Http\Entities;


use Illuminate\Database\Eloquent\Model;

class PruebaSonido extends Model
{
    protected  $fillable = ['fecha','hora_inicio','hora_fin','foro_id','evento_id'];

    public function soundTest()
    {
        return $this
            ->belongsTo(Eventos::class);
    }

    public function eventSoundTest()
    {
        return $this
           ->hasOne(Eventos::class,'id','evento_id');
    }

    public function forumSoundTest()
    {
        return $this
            ->hasOne(Foro::class,'id','foro_id');
    }
}
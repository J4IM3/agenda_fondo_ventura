<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class DetailEvents extends Model
{
    protected $table = "details_events";
    protected $fillable = ['fecha', 'hora_inicio', 'hora_fin', 'sede_id', 'event_id'];

    public function sedes()
    {
        return $this
            ->belongsTo(Eventos::class);
    }

    public function sedesNombre()
    {
        return $this
            ->hasOne(Foro::class,'id','sede_id');
    }

    public function detailEvents()
    {
        return $this
            ->belongsTo(Evento_participantes::class);
    }

    /**
     *
     */
    public function invitadoParticipanteEventoDetalles()
    {
        return $this
            ->belongsTo(Eventos::class);
    }

    public function invitadoParticipanteEventoDetallesSedes()
    {
        return $this
            ->hasOne(Foro::class,'id','sede_id');
    }

    public function presentadorEventoDetalle()
    {
        return $this->belongsTo(Eventos::class);
    }

    public function presentadorEventoDetallesSedes()
    {
        return $this
            ->hasOne(Foro::class,'id','sede_id');
    }

    public function moderadorEventoDetalle()
    {
        return $this->belongsTo(Eventos::class);
    }


}

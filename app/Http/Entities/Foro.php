<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Foro extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','nombre','status','type','sede_id'
    ];

    public function foroSede()
    {
        return $this
            ->hasOne(Sedes::class,'id','sede_id');
    }

    public function foroEvento()
    {
        return $this
            ->hasMany(Eventos::class,'foro_id','id');
    }
    public function foros(){
        return $this
            ->belongsTo(Eventos::class);
    }

    public function forumSoundTest()
    {
        return $this
            ->belongsTo(Foro::class);
    }

    public function sedesNombre()
    {
        return $this
            ->belongsTo(DetailEvents::class);
    }

    public function getSede()
    {
        return $this
            ->belongsTo(Evento_participantes::class);
    }

    /**
     *
     */
    public function invitadoParticipanteEventoDetallesSedes()
    {
        return $this
            ->belongsTo(DetailEvents::class);
    }

    public function presentadorEventoDetallesSedes()
    {
        return $this
            ->belongsTo(DetailEvents::class);
    }


}

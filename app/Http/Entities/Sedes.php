<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Sedes extends Model
{
    protected $fillable = ['name','direction','status'];

    public function foroSede()
    {
        return $this
            ->belongsTo(Foro::class);
    }
}


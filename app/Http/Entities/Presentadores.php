<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Presentadores extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','presentador_id','evento_id'
    ];

    public function presentador()
    {
        return $this->belongsTo(Eventos::class);
    }

    public function presentadores()
    {
        return $this
            ->hasMany(Invitado::class,'id','presentador_id');
    }

    public function presentadores_eventos()
    {
        return $this
            ->hasMany(Eventos::class,'id','evento_id');
    }

    public function invitadoPresentador()
    {
        return $this
            ->belongsTo(Invitado::class);
    }

    public function presentadorEvento()
    {
        return $this
            ->hasOne(Eventos::class,'id','evento_id');
    }

}

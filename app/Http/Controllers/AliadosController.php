<?php

namespace App\Http\Controllers;

use App\Http\Entities\Hotels;
use App\Http\Repositories\HotelesRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
class AliadosController extends Controller
{
    /**
     * @var HotelesRepo
     */
    private $hotelesRepo;

    public function __construct(HotelesRepo $hotelesRepo)
    {

        $this->hotelesRepo = $hotelesRepo;
    }

    public function index()
    {
        return view('aliados.index');
    }

    public function getDataByAjax()
    {
        $aliados = $this->hotelesRepo->all();
        return DataTables::of($aliados)->make(true);
    }

    public function save(Request $request)
    {
        return $this->hotelesRepo->save($request->all());
    }

    public function edit(Request $request)
    {
        return $this->hotelesRepo->edit($request->all());
    }
}

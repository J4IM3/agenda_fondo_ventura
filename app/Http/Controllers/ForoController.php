<?php

namespace App\Http\Controllers;

use App\Http\Entities\Foro;
use App\Http\Entities\Sedes;
use App\Http\Repositories\ForoRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
class ForoController extends Controller
{
    /**
     * @var ForoRepo
     */
    private $foroRepo;

    public function __construct(ForoRepo $foroRepo)
    {
        $this->foroRepo = $foroRepo;
    }

    public function index()
    {
        $sedes = Sedes::where('status',1)->get();
        return view('foros.index',compact('sedes'));
    }

    public function getDataByAjax()
    {
        $foros = Foro::with('foroSede')->get();
        return DataTables::of($foros)->make(true);
    }

        public function create(Request $request)
    {
        return $this->foroRepo->create($request->all());
    }

    public function edit(Request $request)
    {
        return $this->foroRepo->edit($request->all());
    }

    public function changeStatus(Request $request)
    {
        return $this->foroRepo->changeStatus($request->get('id'));
    }
}

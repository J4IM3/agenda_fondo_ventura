<?php

namespace App\Http\Controllers;

use App\Http\Entities\Sedes;
use App\Http\Repositories\SedesRepo;
use App\Http\Requests\SedeRequest;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SedesController extends Controller
{
    private $sedesRepo;

    public function __construct(SedesRepo $sedesRepo)
    {
        $this->sedesRepo = $sedesRepo;
    }

    public function index()
    {
        return view('sedes.index');
    }

    public function getData()
    {
        return DataTables::of(Sedes::all())->make(true);
    }

    public function add(SedeRequest $request)
    {
        return $this->sedesRepo->add($request->all());
    }

    public function edit(SedeRequest $request)
    {
        return $this->sedesRepo->edit($request->all());
    }

    public function changeStatus(Request $request)
    {
        return $this->sedesRepo->changeStatus($request->get('id'));
    }
}

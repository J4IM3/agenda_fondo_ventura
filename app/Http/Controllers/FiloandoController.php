<?php

namespace App\Http\Controllers;

use App\Http\Entities\Filoando;
use App\Http\Repositories\FiloandoRepo;
use App\Http\Repositories\InstitucionesRepo;
use App\Http\Repositories\InvitadosRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class FiloandoController extends Controller
{
    /**
     * @var InvitadosRepo
     */
    private $invitadosRepo;
    /**
     * @var FiloandoRepo
     */
    private $filoandoRepo;
    /**
     * @var InstitucionesRepo
     */
    private $institucionesRepo;

    public function __construct(InvitadosRepo $invitadosRepo,FiloandoRepo $filoandoRepo,InstitucionesRepo $institucionesRepo)
    {
        $this->invitadosRepo = $invitadosRepo;
        $this->filoandoRepo = $filoandoRepo;
        $this->institucionesRepo = $institucionesRepo;
    }

    public function getDataFiloando()
    {
        $eventosFiloando = Filoando::with('instituciones','filoando.invitadosNombre')->get();

        $invitados[] = "";
        unset($invitados);

        foreach ($eventosFiloando as $evento)
        {
            foreach ($evento->filoando as $invitadosEvento)
            {
                foreach ($invitadosEvento->invitadosNombre as $invitado)
                {
                    $invitados[] = $invitado->name;
                    $evento->invitados = implode(", ", $invitados);
                }
            }
            unset($invitados);
        }

        return DataTables::of($eventosFiloando)->make(true);
    }

    public function index(){
        $invitados = $this->invitadosRepo->all();
        $instituciones = $this->institucionesRepo->getInstituciones();

        return view('filoando.index',compact('invitados','instituciones'));
    }

    /**
     * Crear nuevo evento filoando
     */

    public function create(Request $request)
    {
        return $this->filoandoRepo->create($request->all());
    }

    public function update(Request $request)
    {
        return $this->filoandoRepo->update($request->all());
    }

    /**
     * Recibe como parametro el id de un evento de la seccion de filoando para eliminarlo
     */

    public function delete(Request $request){
        return $this->filoandoRepo->delete($request->get('id'));
    }

}

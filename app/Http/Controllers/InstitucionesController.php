<?php

namespace App\Http\Controllers;

use App\Http\Repositories\InstitucionesRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class InstitucionesController extends Controller
{
    /**
     * @var InstitucionesRepo
     */
    private $institucionesRepo;

    public function __construct(InstitucionesRepo $institucionesRepo)
    {
        $this->institucionesRepo = $institucionesRepo;
    }

    public function index()
    {
        return view('instituciones.index');
    }

    public function getDataInstituciones()
    {
        $instituciones = $this->institucionesRepo->all();
        return DataTables::of($instituciones)->make(true);
    }

    public function create(Request $request)
    {
        return $this->institucionesRepo->create($request->all());
    }

    public function update(Request $request)
    {
        return $this->institucionesRepo->update($request->all());
    }

    public function changeStatus(Request $request)
    {
        return $this->institucionesRepo->changeStatus($request->all());

    }
}

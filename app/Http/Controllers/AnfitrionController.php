<?php

namespace App\Http\Controllers;

use App\Http\Repositories\AnfitrionRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AnfitrionController extends Controller
{
    /**
     * @var AnfitrionRepo
     */
    private $anfitrionRepo;

    public function __construct(AnfitrionRepo $anfitrionRepo)
    {
        $this->anfitrionRepo = $anfitrionRepo;
    }

    public function index()
    {
        return view('anfitrion.index');
    }

    public function getDataByAjax()
    {
        $anfitriones = $this->anfitrionRepo->all();
        return DataTables::of($anfitriones)->make(true);
    }

    public function save(Request $request)
    {
        return $this->anfitrionRepo->save($request->all());
    }

    public function update(Request $request)
    {
        return $this->anfitrionRepo->update($request->all());
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Repositories\EventoTipoRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TipoController extends Controller
{
    /**
     * @var EventoTipoRepo
     */
    private $tipoRepo;
    /**
     * @var EventoTipoRepo
     */
    private $eventoTipoRepo;

    public function __construct(EventoTipoRepo $eventoTipoRepo)
    {

        $this->eventoTipoRepo = $eventoTipoRepo;
    }

    public function index()
    {
        return view('tipo.index');
    }

    public function geTipoByAjax()
    {
        $tipos = $this->eventoTipoRepo->all();
        return DataTables::of($tipos)->make(true);
    }

    public function create(Request $request)
    {
        return $this->eventoTipoRepo->create($request->all());
    }

    public function edit(Request $request)
    {
        return $this->eventoTipoRepo->edit($request->all());
    }
}

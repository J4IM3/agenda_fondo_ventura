<?php

namespace App\Http\Controllers;

use App\Http\Entities\Actividades;
use App\Http\Entities\Evento_participantes;
use App\Http\Entities\Eventos;
use App\Http\Entities\Filoando;
use App\Http\Entities\FiloandoInvitados;
use App\Http\Entities\Invitado;
use App\Http\Entities\Itinerarios;
use App\Http\Entities\Moderadores;
use App\Http\Entities\Prensa;
use App\Http\Entities\Presentadores;
use App\Http\Entities\Social;
use App\Http\Repositories\InvitadosPrensaRepo;
use App\Http\Repositories\InvitadosRepo;
use App\Http\Requests\InvitadosRequest;
use Illuminate\Http\Request;
use Symfony\Component\VarDumper\Cloner\Data;
use Yajra\DataTables\DataTables;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Jenssegers\Date\Date;

use Illuminate\Support\Str as Str;
use Illuminate\Support\Facades\File;

class InvitadosController extends Controller
{
    /**
     * @var InvitadosRepo
     */
    private $invitadosRepo;
    private $invitadosPrensaRepo;

    public function __construct(InvitadosRepo $invitadosRepo,InvitadosPrensaRepo $invitadosPrensaRepo)
    {
        $this->invitadosRepo = $invitadosRepo;
        $this->invitadosPrensaRepo = $invitadosPrensaRepo;
    }

    public function index(){
        return view('invitados.index');
    }

    public function save(InvitadosRequest $request){
        $nameImage = " ";
        if ($request->file('avatar') != null)
        {
            $image = $request->file('avatar');
            $name = Str::slug($request->get('name'));
            $extension = $image->getClientOriginalExtension();
            $image->move('avatar', $name.".".$extension);
            $nameImage = $name.".".$extension;
        }
        return $this->invitadosRepo->save($request->all(),$nameImage);
    }

    public function getInvitadoByAjax(){
        $invitados = Invitado::with('itinerario','itinerario.aerolineas','itinerario.hotel','anfitrion')->get();
        return DataTables::of($invitados)->make(true);
    }

    public function getInvitadoItinerarioAjax(){

        $invitados = Invitado::join('itinerarios','invitado_id','invitados.id')
            ->with( 'itinerario2','itinerario2.aerolineas','itinerario2.aerolineasInter','itinerario2.hotel','presupuesto','anfitrion')
            ->get();
        /*
         *$invitados = Invitado::join('itinerario','invitado_id','invitados.id')
            ->with( 'itinerario2','itinerario.aerolineas','itinerario.aerolineasInter','itinerario.hotel','presupuesto','anfitrion')
            ->get();

        $invitado = Invitado::with('itinerario', 'itinerario.aerolineas', 'itinerario.hotel','anfitrion')->get();
         */

        return DataTables::of($invitados)->make(true);
    }

    public function update(Request $request){
        $nameImage = null;
        if ($request->file('avatar') != null)
        {
            $invitado = Invitado::where('id',$request->get('id'))->first();
            File::delete('avatar/'.$invitado->avatar);
            $image = $request->file('avatar');
            $nameImage = Str::slug($request->get('name')).date('his').".".$image->getClientOriginalExtension();
            $image->move('avatar',$nameImage);
        }
        return $this->invitadosRepo->update($request->all(),$nameImage);
    }

    public function getInvitadoPrensaAjax(){
        $invitados = Invitado::with('prensa')
            ->get();
        return DataTables::of($invitados)->make(true);
    }


    public function delete(Request $request)
    {
        return $this->invitadosRepo->deleteUser($request->get('id'));
    }

    public function download($id,Request $request)
    {
        Date::setLocale('es');
        $visibleSede = "";
        $prensa = "";

        $invitado =  Invitado::with(
            'itinerarioVuelo.aerolineas','itinerarioVuelo.aerolineasInter','itinerarioVuelo.aerolineasInterRetorno',
            'itinerarioVuelo.hoteles','invitadosNombre','anfitrion',
            'itinerarioVuelo.anfitrion')
            ->where('id',$id)
            ->first();

        /*$eventos = Evento_participantes::with(
            'detailEvents.sedesNombre','eventos.programacion',
            'eventos.programacionTipo','eventos.moderador.moderadoresNombre')
            ->where('invitado_id',$id)
            ->get();*/

        /**
         * Nuevo
         */
        $invitadosEventos = Invitado::with(
            'invitadoEventoParticipante.invitadoParticipanteEvento.invitadoParticipanteEventoDetalles.invitadoParticipanteEventoDetallesSedes',
            'invitadoPresentador.presentadorEvento.presentadorEventoDetalle.presentadorEventoDetallesSedes',
            'invitadoModerador.moderadorEventos.moderadorEventoDetalle.presentadorEventoDetallesSedes'
        )->where('id',$id)->get();


        foreach ($invitadosEventos as $value)
        {
            $actividadesEventos = $this->getEventos($value);
        }

        foreach ($invitadosEventos as $value)
        {
            $actividadesPresentador = $this->getEventosPresentador($value);
        }

        foreach ($invitadosEventos as $value)
        {
            $actividadesModerador = $this->getEventosModerador($value);
        }
        $actividadesEventosPresentadores = $actividadesEventos->concat($actividadesPresentador)->concat($actividadesModerador);
        $actividades = $actividadesEventosPresentadores->sortBy('hora')->sortBy('fecha');
        $visibleSede = $actividades->firstWhere('formato', '=','Presencial') !== null;
        $actividades->sortBy('fecha');
        /**
         * Termina nuevo
         */

        //$invitadoAnfitrion =  Itinerarios::with('itinerarioVuelo.anfitrion')->where('id',$id)->first();
        if (!empty($invitado->itinerarioVuelo[0]->anfitrion->nombre)){
            $anfitrion = $invitado->itinerarioVuelo[0]->anfitrion->nombre;
            $anfitrionTelefono = $invitado->itinerarioVuelo[0]->anfitrion->telefono;
        }
        else{
            $anfitrion="";
            $anfitrionTelefono ="";
        }

        /*foreach ($eventos as $evento)
        {
            foreach ($evento->detailEvents as $sedes)
            {
                $foros[] = $sedes->sedesNombre->nombre;
                $evento->nombreSedes = implode(", ", $foros);
            }
            unset($foros);
        }*/

        //$actividades = $this->saveEventos($eventos,$id,$anfitrion,$anfitrionTelefono);
        /*$filoando = FiloandoInvitados::with('eventosFiloando.instituciones')->where('invitado_id',$id)->get();
        if (!empty($filoando)) {
            $filoandos = $this->saveFiloando($filoando,$id,$anfitrion,$anfitrionTelefono);
        }*/

        $prensaEventos = Prensa::with('prensaInvitado','prensaMedios','prensaTipos')->where('invitado_id',$id)->get();
        if (count($prensaEventos) > 0 )
        {
            $prensa = $this->invitadosPrensaRepo->getPrensa($prensaEventos);
        }

        $social = Social::with('socialInvitado','socialHotel')->where('invitado_id',$id)->get();
        if (count($social) > 0 )
        {
            $social = $this->saveSocial($social,$anfitrion,$anfitrionTelefono);
        }

        //$actividades = Actividades::where('invitado_id',$id)->orderBy('fecha','ASC')->orderBy('hora','ASC')->get();

        $pdf = PDF::loadView('invitados.itinerarios', compact('invitado','prensa','actividades','visibleSede'));
        //$this->deleteActividades($id);
        $pdf->setPaper('A4', 'portrait');
        //return view('invitados.itinerarios',compact('invitado'));
        return $pdf->download($invitado->name.'.pdf');

    }

    private function getEventos($value)
    {
        $eventos = [];
        $nombreInvitado = [];

        foreach ($value->invitadoEventoParticipante as $key => $invitadoV)
        {
            $nombreModerador = [];
            $nombrePresentador = [];

            $participantes_id = Evento_participantes::where('evento_id',$invitadoV->evento_id)->get();
            foreach ($participantes_id as $participante_id)
            {
                $invitado = Invitado::where('id',$participante_id->invitado_id)->first();
                $nombreInvitado[] = $invitado->slug;
            }

            //moderadores
            $moderadores_id = Moderadores::where('evento_id',$invitadoV->evento_id)->get();
            foreach ($moderadores_id as $moderador_id)
            {
                $invitado = Invitado::where('id',$moderador_id->moderador_id)->first();
                $nombreModerador[] = $invitado->slug;
            }

            //Presentadores
            $presentadores_id = Presentadores::where('evento_id',$invitadoV->evento_id)->get();
            foreach ($presentadores_id as $presentador_id)
            {
                $invitado = Invitado::where('id',$presentador_id->presentador_id)->first();
                $nombrePresentador[] = $invitado->slug;
            }

            foreach ($invitadoV->invitadoParticipanteEvento->invitadoParticipanteEventoDetalles as $item)
            {
                $fecha = $item->fecha;
                $hora = $item->hora_inicio;
                $sede = $item->invitadoParticipanteEventoDetallesSedes->nombre;
                $formato = $invitadoV->invitadoParticipanteEvento->formato;
            }
            $eventos[$key]['fecha'] = $fecha;
            $eventos[$key]['hora'] = $hora;
            $eventos[$key]['sede'] = $sede;
            $eventos[$key]['formato'] = $formato;
            $eventos[$key]['participan'] = implode(',',$nombreInvitado);
            $eventos[$key]['moderan'] = implode(',',$nombreModerador);
            $eventos[$key]['presentador'] = implode(',',$nombrePresentador);
            $eventos[$key]['nombre_evento'] = $invitadoV->invitadoParticipanteEvento->nombre_evento;
            $eventos[$key]['descripcion'] =   $invitadoV->invitadoParticipanteEvento->descripcion;

            unset($fecha);
            unset($hora);
            unset($sede);
            unset($formato);
            unset($nombreInvitado);
            unset($nombrePresentador);
            unset($nombreModerador);
        }
        return $collection = collect($eventos);
        //return $collection->sortBy('fecha')->sortBy('hora') ;
    }

    public function getEventosPresentador($eventos)
    {
        $registros = [];


        foreach ($eventos->invitadoPresentador as $key => $evento)
        {
            $nombreModerador = [];
            $nombrePresentador = [];

            $participantes_id = Evento_participantes::where('evento_id',$evento->evento_id)->get();
            foreach ($participantes_id as $participante_id)
            {
                $invitado = Invitado::where('id',$participante_id->invitado_id)->first();
                $nombreInvitado[] = $invitado->slug;
            }

            //moderadores
            $moderadores_id = Moderadores::where('evento_id',$evento->evento_id)->get();
            foreach ($moderadores_id as $moderador_id)
            {
                $invitado = Invitado::where('id',$moderador_id->moderador_id)->first();
                $nombreModerador[] = $invitado->slug;
            }

            //Presentadores
            $presentadores_id = Presentadores::where('evento_id',$evento->evento_id)->get();
            foreach ($presentadores_id as $presentador_id)
            {
                $invitado = Invitado::where('id',$presentador_id->presentador_id)->first();
                $nombrePresentador[] = $invitado->slug;
            }

            foreach ($evento->presentadorEvento->presentadorEventoDetalle as $item)
            {
                $fecha = $item->fecha;
                $hora = $item->hora_inicio;
                $sede = $item->presentadorEventoDetallesSedes->nombre;
            }

            $registros[$key]['nombre_evento'] = $evento->presentadorEvento->nombre_evento;
            $registros[$key]['descripcion'] = $evento->presentadorEvento->descripcion;
            $registros[$key]['participan'] = implode(',',$nombreInvitado);
            $registros[$key]['modera'] = implode(',',$nombreModerador);
            $registros[$key]['presenta'] = implode(',',$nombrePresentador);
            $registros[$key]['fecha'] = $fecha;
            $registros[$key]['hora'] = $hora;
            $registros[$key]['sede'] = $sede;
            unset($fecha);
            unset($hora);
            unset($sede);
            unset($nombreInvitado);
            unset($nombrePresentador);
            unset($nombreModerador);
        }
        return $collection = collect($registros);
        //return $collection->sortBy('fecha')->sortBy('hora') ;
    }

    private function getEventosModerador($eventos)
    {
        $registros = [];
        $nombreModerador = [];
        $nombrePresentador = [];

        foreach ($eventos->invitadoModerador as $key => $evento)
        {
            $nombreModerador = [];
            $nombrePresentador = [];

            $participantes_id = Evento_participantes::where('evento_id',$evento->evento_id)->get();
            foreach ($participantes_id as $participante_id)
            {
                $invitado = Invitado::where('id',$participante_id->invitado_id)->first();
                $nombreInvitado[] = $invitado->slug;
            }

            //moderadores
            $moderadores_id = Moderadores::where('evento_id',$evento->evento_id)->get();

            foreach ($moderadores_id as $moderador_id)
            {
                $invitado = Invitado::where('id',$moderador_id->moderador_id)->first();
                $nombreModerador[] = $invitado->slug;
            }


            //Presentadores
            $presentadores_id = Presentadores::where('evento_id',$evento->evento_id)->get();
            foreach ($presentadores_id as $presentador_id)
            {
                $invitado = Invitado::where('id',$presentador_id->presentador_id)->first();
                $nombrePresentador[] = $invitado->slug;
            }

            foreach ($evento->moderadorEventos->moderadorEventoDetalle as $item)
            {
                $fecha = $item->fecha;
                $hora = $item->hora_inicio;
                $sede = $item->presentadorEventoDetallesSedes->nombre;
            }

            $registros[$key]['nombre_evento'] = $evento->moderadorEventos->nombre_evento;
            $registros[$key]['descripcion'] = $evento->moderadorEventos->descripcion;
            $registros[$key]['participan'] = implode(',',$nombreInvitado);
            $registros[$key]['modera'] = implode(',',$nombreModerador);
            $registros[$key]['presenta'] = implode(',',$nombrePresentador);
            $registros[$key]['fecha'] = $fecha;
            $registros[$key]['hora'] = $hora;
            $registros[$key]['sede'] = $sede;
            unset($fecha);
            unset($hora);
            unset($sede);
            unset($nombreInvitado);
            unset($nombrePresentador);
            unset($nombreModerador);
        }
        return $collection = collect($registros);
    }

    private function saveEventos($items,$id,$anfitrion,$anfitrionTelefono)
    {
        $participantes[]="";
        $moderador[]="";
        $presentador[]="";
        unset($participantes);
        unset($moderador);
        unset($presentador);
        $presenta_id = $id;
        $presentadores = Presentadores::with('presentadores')
            ->where('presentador_id',$id)
            ->get();

        if (isset($items))
        {
            foreach ($presentadores as $value)
            {
                $items = Evento_participantes::with('invitadosNombre')
                    ->where('evento_id',$value->evento_id)
                    ->get();
            }
        }
        foreach ($items as $item)
        {
            $invitados = Evento_participantes::with('invitadosNombre')->where('evento_id',$item->evento_id)->get();
            $moderadores = Moderadores::with('moderadoresNombre')
                ->where('evento_id',$item->evento_id)
                ->get();
            $presentadores = Presentadores::with('presentadores')
                ->where('evento_id',$item->evento_id)
                ->get();

            foreach ($invitados as $invitado)
            {
                $participantes[]= $invitado->invitadosNombre[0]->name;
            }

            if (!empty($moderadores))
            {
                foreach ($moderadores as $moderadore)
                {
                    $moderador[] = $moderadore->moderadoresNombre[0]->name;
                }
                if (!empty($moderador)){
                    $item->moderador = implode(", ",$moderador);
                }else{
                    $item->moderador = "";
                }
            }

            if(!empty($presentadores))
            {
                foreach ($presentadores as $presentadore)
                {
                    $presentador[] = $presentadore->presentadores[0]->name;

                }
                if (!empty($presentador)){
                    $item->presentador = implode(", ",$presentador);
                }else{
                    $item->presentador = "";
                }
            }

            Actividades::create([
                'fecha'=>$item->eventos[0]->fecha,
                'hora'=>$item->eventos[0]->hora_inicio,
                'hora_final'=>$item->eventos[0]->hora_fin,
                'actividad'=>$item->eventos[0]->nombre_evento,
                'lugar'=>$item->nombreSedes,
                'invitado_id'=>$id,
                'ciclo'=>$item->eventos[0]->ciclo,
                'descripcion'=>$item->eventos[0]->descripcion,
                'tipo_evento'=>$item->eventos[0]->programacion->nombre,
                'tipo_programa'=>$item->eventos[0]->programacionTipo->nombre,
                'invitados'=>implode(", ",$participantes),
                'moderadores'=>$item->moderador,
                'presentadores'=>$item->presentador,
                'anfitrion'=>$anfitrion,
                'telefono_anfitrion'=>$anfitrionTelefono
            ]);

            unset($participantes);
            unset($moderador);
            unset($presentador);
        }
    }

    private function saveFiloando($items,$id,$anfitrion,$anfitrionTelefono)
    {
        $participantes[]="";
        unset($participantes);
        foreach ($items as $item){

            $invitados = FiloandoInvitados::with('invitadosNombre')->where('evento_id',$item->evento_id)->get();

            foreach ($invitados as $invitado)
            {
                $participantes[]= $invitado->invitadosNombre[0]->name;;
            }

            Date::setLocale('es');
            $date = Date::parse($item->fecha)->format('l j F Y');
            Actividades::create([
                'fecha'=>$item->eventosFiloando[0]->fecha,
                'hora'=>$item->eventosFiloando[0]->hora_inicio_evento,
                'hora_final'=>$item->eventosFiloando[0]->hora_final_evento,
                'actividad'=>"Evento filoando",
                'lugar'=>$item->eventosFiloando[0]->instituciones->nombre,
                'invitado_id'=>$id,
                'ciclo'=>"",
                'descripcion'=>" ",
                'tipo_evento'=>" ",
                'tipo_programa'=>" ",
                'invitados'=>implode(", ",$participantes),
                'anfitrion'=>$anfitrion,
                'telefono_anfitrion'=>$anfitrionTelefono
            ]);
            unset($participantes);
        }
    }

    public function savePrensa($items,$anfitrion,$anfitrionTelefono)
    {
        foreach ($items as $item)
        {
            Date::setLocale('es');
            $date = Date::parse($item->fecha)->format('l j F Y');

            $prensa = Actividades::create([
                'fecha'=>$item->fecha,
                'hora'=>$item->hora_inicio,
                'hora_final'=>$item->hora_fin,
                'actividad'=>"Evento prensa: ".$item->prensaTipos->nombre,
                'lugar'=>$item->lugar,
                'invitado_id'=>$item->invitado_id,
                'fecha_mostrar'=>$date,
                'ciclo'=>"",
                'descripcion'=>"",
                'tipo_evento'=>"",
                'tipo_programa'=>"",
                'anfitrion'=>$anfitrion,
                'telefono_anfitrion'=>$anfitrionTelefono
            ]);
        }
        return $prensa;
    }

    public function deleteActividades($id){
        $activides = Actividades::where('invitado_id',$id)->get();
        foreach ($activides as $item)
        {
            $item->delete();
        }
    }

    public function saveSocial($items,$anfitrion,$anfitrionTelefono)
    {
        foreach ($items as $item) {

            $social = Actividades::create([
                'fecha'=>$item->fecha,
                'hora'=>$item->hora_inicio,
                'hora_final'=>$item->hora_fin,
                'actividad'=>$item->actividad,
                'lugar'=>$item->socialHotel->name,
                'invitado_id'=>$item->invitado_id,
                'ciclo'=>"",
                'descripcion'=>$item->actividad,
                'tipo_evento'=>"",
                'tipo_programa'=>"",
                'invitados'=>"",
                'anfitrion'=>$anfitrion,
                'telefono_anfitrion'=>$anfitrionTelefono
            ]);
        }
    }

    /**
     * Eliminar foto de invitado
     */

    public function deleteImage(Request $request)
    {
        return $this->invitadosRepo->deleteImage($request->get('id'));
    }

    public function findEmailOrPhone(Request $request)
    {
        return $this->invitadosRepo->findEmail($request->all());
    }





}

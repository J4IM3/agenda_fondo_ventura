<?php

namespace App\Http\Controllers;

use App\Http\Entities\Evento_participantes;
use App\Http\Entities\Eventos;
use App\Http\Entities\EventoTipo;
use App\Http\Entities\Foro;
use App\Http\Repositories\EventosRepo;
use App\Http\Repositories\EventoTipoRepo;
use App\Http\Repositories\EventRepo;
use App\Http\Repositories\ForoRepo;
use App\Http\Repositories\InvitadosRepo;
use App\Http\Requests\EventoCreate;
use App\Http\Requests\EventoEditarRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;
use DB;
class EventoController extends Controller
{
    /**
     * @var InvitadosRepo
     */
    private $invitadosRepo;
    /**
     * @var EventosRepo
     */
    private $eventosRepo;
    /**
     * @var EventoTipo
     */
    private $eventoTipo;
    /**
     * @var ForoRepo
     */
    private $foroRepo;
    /**
     * @var EventoTipoRepo
     */
    private $eventoTipoRepo;
    private $eventRepo;

    public function __construct(
        InvitadosRepo $invitadosRepo,
        EventosRepo $eventosRepo,
        ForoRepo $foroRepo,
        EventoTipoRepo $eventoTipoRepo, EventRepo $eventRepo)
    {
        $this->invitadosRepo = $invitadosRepo;
        $this->eventosRepo = $eventosRepo;
        $this->foroRepo = $foroRepo;
        $this->eventoTipoRepo = $eventoTipoRepo;
        $this->eventRepo = $eventRepo;
    }

    public function index()
    {
        $invitados = $this->invitadosRepo->all();
        $tipos = $this->eventoTipoRepo->tipoEvento("Tipo evento");
        $programaciones = $this->eventoTipoRepo->tipoEvento("Programacion");
        $foros = $this->foroRepo->getForos();

        return view('evento.index', compact('invitados', 'tipos', 'programaciones', 'foros'));
    }


    public function getDatabyAjax()
    {
        $eventos = Eventos::with('invitados.invitadosNombre',
            'foros',
            'programacion',
            'tipo',
            'presentador.presentadores',
            'moderador',
            'soundTest',
            'sedes.sedesNombre')
            ->orderBy('fecha','ASC')
            ->orderBy('hora_inicio','ASC')
            ->get();
        $participantes[] = "";
        $foros[] = "";
        unset($participantes);
        unset($foros);

        foreach ($eventos as $evento)
        {
            $evento->descripcion_alterna = Str::limit($evento->descripcion,50,'...');
            $invitados = Evento_participantes::with('invitadosNombre')
                ->where('evento_id', $evento->id)
                ->get();
            foreach ($invitados as $invitado) {
                $participantes[] = $invitado->invitadosNombre[0]->slug;
                $evento->participantesPrueba = implode(", ", $participantes);
            }

            foreach ($evento->sedes as $sedes)
            {
                $foros[] = $sedes->sedesNombre->nombre;
                $evento->nombreSedes = implode(", ", $foros);
            }
            unset($participantes);
            unset($foros);
        }
        return DataTables::of($eventos->sortBy('fecha'))->make(true);
    }


    public function save(EventoCreate $request)
    {
        return $this->eventosRepo->saveEvento($request->all());
        /*return $this->eventRepo->saveEvent($request->all());*/
    }
    /**
     *Retorna los datos de un evento al llamado en ajax que muestra la modal para visualizar y actualizar los datos.
     * @param Request $request
     * @return Eventos|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     * */
    public function editar(Request $request)
    {
        return $this->eventosRepo->getData($request->all());
    }

    public function update(EventoEditarRequest $request)
    {
        return $this->eventosRepo->update($request->all());
    }

    public function delete(Request $request)
    {
        return $this->eventosRepo->delete($request->all());
    }

    /**
     * >Crear una prueba de sonido
     */

    public function createSoundTest(Request $request)
    {
        return $this->eventosRepo->soundTest($request->all());
    }

    public function getSedeByType(Request $request)
    {
        if ($request->get('type') == "Hibrido")
        {
            return Foro::where('status',1)->get();
        }
        return Foro::where('type',$request->get('type'))->where('status',1)->get();
    }




}

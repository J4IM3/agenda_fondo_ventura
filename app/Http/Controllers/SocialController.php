<?php

namespace App\Http\Controllers;

use App\Http\Entities\Social;
use App\Http\Repositories\HotelesRepo;
use App\Http\Repositories\InvitadosRepo;
use App\Http\Repositories\SocialRepo;
use App\Http\Requests\SocialRequest;
use App\Http\Requests\SocialUpdateRequest;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SocialController extends Controller
{
    /**
     * @var SocialRepo
     */
    private $socialRepo;
    /**
     * @var InvitadosRepo
     */
    private $invitadosRepo;
    /**
     * @var HotelesRepo
     */
    private $hotelesRepo;

    public function __construct(SocialRepo $socialRepo,InvitadosRepo $invitadosRepo,HotelesRepo $hotelesRepo)
    {
        $this->socialRepo = $socialRepo;
        $this->invitadosRepo = $invitadosRepo;
        $this->hotelesRepo = $hotelesRepo;
    }

    public function index(){
        $invitados = $this->invitadosRepo->all();
        $restaurants  = $this->hotelesRepo->getRestaurant();
        return view('social.index',compact('invitados','restaurants'));
    }



    public function getSocialByAjax(){
        $socialEvent = Social::with('socialInvitado','socialHotel')->get();
        return DataTables::of($socialEvent)->make(true);
    }

    public function save(SocialRequest $request){
        return $this->socialRepo->save($request->all());
    }

    public function update(SocialUpdateRequest $request){
        return $this->socialRepo->update($request->all());
    }

    public function delete(Request $request){
        return $this->socialRepo->delete($request->get('id'));
    }
}

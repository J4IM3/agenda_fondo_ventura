<?php

namespace App\Http\Controllers;

use App\Http\Entities\Eventos;
use App\Http\Entities\Programa_temp;
use App\Http\Repositories\AerolineasRepo;
use App\Http\Repositories\AnfitrionRepo;
use App\Http\Repositories\HotelesRepo;
use App\Http\Repositories\InvitadosRepo;
use App\Http\Repositories\ItinerariosRepo;
use App\Http\Repositories\UsersRepo;
use App\Http\Entities\Invitado;
use App\Http\Entities\Itinerarios;
use App\Http\Requests\CreateItinerarioRequest;
use Illuminate\Http\Request;

class ItinerarioController extends Controller
{
    /**
     * @var UsersRepo
     */
    private $usersRepo;
    /**
     * @var HotelesRepo
     */
    private $hotelesRepo;
    /**
     * @var InvitadosRepo
     */
    private $invitadosRepo;
    /**
     * @var AerolineasRepo
     */
    private $aerolineasRepo;
    /**
     * @var ItinerariosRepo
     */
    private $itinerariosRepo;
    /**
     * @var AnfitrionRepo
     */
    private $anfitrionRepo;

    public function __construct(
        UsersRepo $usersRepo,
        HotelesRepo $hotelesRepo,
        InvitadosRepo $invitadosRepo,
        AerolineasRepo $aerolineasRepo,
        ItinerariosRepo $itinerariosRepo,
        AnfitrionRepo $anfitrionRepo)
    {
        $this->usersRepo = $usersRepo;
        $this->hotelesRepo = $hotelesRepo;
        $this->invitadosRepo = $invitadosRepo;
        $this->aerolineasRepo = $aerolineasRepo;
        $this->itinerariosRepo = $itinerariosRepo;
        $this->anfitrionRepo = $anfitrionRepo;
    }

    public function index(){
        //$invitados = $this->invitadosRepo->getInvitado();
        $invitados = $this->invitadosRepo->all();
        $anfitriones = $this->anfitrionRepo->getAnfitrionActives();
        $aerolineas = $this->aerolineasRepo->getAero();
        $hoteles = $this->hotelesRepo->getHotel();

        return view('itinerario.index',compact('invitados','aerolineas','hoteles','anfitriones'));
    }

    public function save(CreateItinerarioRequest $request){
        return $this->itinerariosRepo->save($request->all());
    }

    public function update(Request $request){
        return $this->itinerariosRepo->update($request->all());
    }

    public function addHonorarios(Request $request)
    {
        return $this->itinerariosRepo->addHonorarios($request->all());
    }

    public function generarPrograma()
    {

        $eventos = Eventos::with(array('programacion' => function($query){
            $query->where('tipo',"Tipo evento");
        },'invitados.invitadosNombre',
            'presentador.presentadores',
            'moderador.moderadoresNombre',
            'foros'))->get();

        $moderadores[]  = "";
        $presentadores[]  = "";
        unset($moderadores);
        unset($presentadores);

        foreach ($eventos as $actividades)
        {
            foreach ($actividades->invitados as $item)
            {
                $participantes[] = $item->invitadosNombre[0]->slug;;
            }
            $actividades->participantes = $participantes;

            foreach ($actividades->moderador as $itemsModeradores)
            {
                $moderadores[]  = $itemsModeradores->moderadoresNombre[0]->slug;
            }

            if (!empty($moderadores))
            {
                $actividades->moderador = $moderadores;
                $moderador = "Modera: ".implode(",",$actividades->moderador);
            }else{
                $moderador="";
            }


            foreach ($actividades->presentador as $itemsPresentadores)
            {
                $presentadores[]  = $itemsPresentadores->presentadores[0]->name;
            }

            if (!empty($presentadores))
            {
                $actividades->presentador = $presentadores;
                $presentador = " Presenta:  ".implode(",",$actividades->presentador);
            }else{
                $presentador="";
            }

            $programas = Programa_temp::create([
                'titulo'=>$actividades->nombre_evento,
                'participantes'=>implode(", ", $actividades->participantes),
                'lugar'=>$actividades->foros->nombre,
                'info'=>$actividades->descripcion,
                'modera'=>$moderador.$presentador,
                'presentador'=>$presentador,
                'categoria'=>$actividades->programacion->nombre,
                'fini'=>$actividades->fecha,
                'hora_ini'=>$actividades->hora_inicio,
                'ciclo'=>$actividades->ciclo,
            ]);
            unset($participantes);
            unset($moderadores);
            unset($presentadores);
        }

        return $programas;
    }



}

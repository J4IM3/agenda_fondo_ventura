<?php

namespace App\Http\Controllers;
use App\Http\Repositories\UsersRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{
    /**
     * @var UsersRepo
     */
    private $usersRepo;

    public function __construct(UsersRepo $usersRepo)
    {
        $this->usersRepo = $usersRepo;
    }

    public function index(){
        return view('users.index');
    }

    public function getUsersByAjax(){
        $users = $this->usersRepo->all();
        return DataTables::of($users)->make(true);
    }

    public function save(Request $request){
        return $this->usersRepo->save($request->all());
    }

    public function edit(Request $request){
        return $this->usersRepo->edit($request->all());
    }

    public function changeStatus(Request $request){
        return $this->usersRepo->changeStatus($request->all());
    }
}

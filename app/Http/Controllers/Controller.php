<?php

namespace App\Http\Controllers;

use App\Http\Entities\Eventos;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function prueba()
    {
        $evento = Eventos::where('id',1)->first();
        $horario_final_evento = strtotime ( $evento->hora_fin);
        $horario_final_limpieza = strtotime ( '+30 minute',$horario_final_evento) ;
        return $evento->hora_fin." - ".date("H:i:s",$horario_final_limpieza);
    }
}

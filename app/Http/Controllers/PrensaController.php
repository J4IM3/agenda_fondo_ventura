<?php

namespace App\Http\Controllers;

use App\Http\Entities\Prensa;
use App\Http\Repositories\InvitadosRepo;
use App\Http\Repositories\MediosRepo;
use App\Http\Repositories\PrensaRepo;
use App\Http\Repositories\PrensaTiposRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PrensaController extends Controller
{
    /**
     * @var InvitadosRepo
     */
    private $invitadosRepo;
    /**
     * @var PrensaRepo
     */
    private $prensaRepo;
    /**
     * @var PrensaTiposRepo
     */
    private $prensaTiposRepo;
    /**
     * @var MediosRepo
     */
    private $mediosRepo;

    /**
     * PrensaController constructor.
     * @param InvitadosRepo $invitadosRepo
     */
    public function __construct(InvitadosRepo $invitadosRepo,
                                PrensaRepo $prensaRepo,
                                PrensaTiposRepo $prensaTiposRepo,
                                MediosRepo $mediosRepo
                                )
    {
        $this->invitadosRepo = $invitadosRepo;
        $this->prensaRepo = $prensaRepo;
        $this->prensaTiposRepo = $prensaTiposRepo;
        $this->mediosRepo = $mediosRepo;
    }

    public function index(){
        $invitados = $this->invitadosRepo->all();
        $tipos = $this->prensaTiposRepo->getTiposActive();
        $medios = $this->mediosRepo->all();
        return view('prensa.index',compact('invitados','tipos','medios'));
    }

    public function getPrensaAjax(){
        $prensa = Prensa::with('prensa','medios','prensaTipos')->get();
        return DataTables::of($prensa)->make(true);
    }

    public function save(Request $request){
        return $this->prensaRepo->save($request->all());
    }

    public function update(Request $request){
        return $this->prensaRepo->update($request->all());
    }

    public function delete(Request $request){
        return $this->prensaRepo->delete($request->get('id'));
    }


}

<?php

namespace App\Http\Controllers;

use App\Http\Repositories\AerolineasRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AerolineasController extends Controller
{

    /**
     * @var AerolineasRepo
     */
    private $aerolineasRepo;

    public function __construct(AerolineasRepo $aerolineasRepo)
    {
        $this->aerolineasRepo = $aerolineasRepo;
    }

    public function index(){
        return view('aerolineas.index');
    }

    public function save(Request $request){
        return $this->aerolineasRepo->save($request->all());
    }

    public function getDataByAjax(){
        $aerolineas = $this->aerolineasRepo->all();
        return DataTables::of($aerolineas)->make(true);
    }

    public function changeStatus(Request $request){
        return $this->aerolineasRepo->changeStatus($request->get('id'));
    }

    public function update(Request $request){
        return $this->aerolineasRepo->update($request->all());
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Entities\PrensaTipos;
use App\Http\Repositories\PrensaTiposRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PrensaTiposController extends Controller
{
    /**
     * @var PrensaTiposRepo
     */
    private $prensaTiposRepo;

    public function __construct(PrensaTiposRepo $prensaTiposRepo)
    {
        $this->prensaTiposRepo = $prensaTiposRepo;
    }

    public function index()
    {
        return view('prensa-tipos.index');
    }

    public function getTiposPrensa()
    {
        $tipos = PrensaTipos::all();
        return DataTables::of($tipos)->make(true);
    }

    public function save(Request $request)
    {
        return $this->prensaTiposRepo->save($request->all());
    }

    public function editar(Request $request)
    {
        return $this->prensaTiposRepo->editar($request->all());
    }

    public function changeStatus(Request $request)
    {
        return $this->prensaTiposRepo->changeStatus($request->all());
    }

}

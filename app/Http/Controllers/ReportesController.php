<?php

namespace App\Http\Controllers;

use App\Exports\EventosExport;
use App\Http\Entities\Actividades;
use App\Http\Entities\Eventos;
use App\Http\Entities\Foro;
use App\Http\Entities\Invitado;
use App\Http\Entities\Itinerarios;
use App\Http\Entities\PruebaSonido;
use App\Http\Export\ExportarEventosRequerimientosFecha;
use App\Http\Export\ExportarPorForo;
use App\Http\Export\ExportarReporteForoRequerimientos;
use App\Http\Export\ExportForoFecha;
use App\Http\Export\ExportForos;
use App\Http\Export\ExportInvitadoProgram;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Export\ExportarInvitadosExcel;
use Illuminate\Support\Collection as Collection;
use Exception;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str as Str;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Return_;

class ReportesController extends Controller
{
    public function index()
    {
        return view('reportes.reporte_programacion');

    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * Nos retorna un reporte en excel con los invitaods asi como la informacion si dependiendo de la fecha
     * ingresada ya se despidieron de la filo o siguen en ella
     */
    public function reporteProgramacion(Request $request)
    {
        return Excel::download(new ExportarInvitadosExcel($request->get('fecha')), 'users.xlsx');
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * Nos retorna un documento de word en el cual se visualizan las semblanzas del invitado
     * asi como su fotografia si es que esta existiera
     */

    public function downloadSemblance($id)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $invitado = Invitado::where('id',$id)->first();
        $nameFile = Str::slug($invitado->name).'.'.'docx';
        $section = $phpWord->addSection();
        if (!empty($invitado->avatar))
        {
            $section->addImage(
                asset('/avatar/'.$invitado->avatar),
                array(
                    'width'         => 100,
                    'height'        => 100,
                    'marginTop'     => -1,
                    'marginLeft'    => 4,
                    'wrappingStyle' => 'behind'
                )
            );
        }

        $section->addText(
            $invitado->name,
            array('name' => 'Tahoma', 'size' => 15)
        );

        if (!empty($invitado->semblanza))
        {
            $section->addText($invitado->semblanza);
        }else{
            $section->addText("No existe semblanza");
        }

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        try {
            $objWriter->save(storage_path($nameFile));
        } catch (Exception $e) {
        }
        return response()->download(storage_path($nameFile));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * Nos devuelve un excel con los invitados y los programas en los que esta participando
     */
    public function invitadosProgramacion()
    {
        return Excel::download(new ExportInvitadoProgram(), 'invitados_programacion.xlsx');
    }

    /**
     * Nos retorna a la vista para seleccionar el reporte deseado
     * reporte por fecha : Nos muestra todos los foros y sus horarios de el dia seleccionado
     * reporte por foro: Nos muestra el foro seleccionado y sus horarios
     * reporte general , nos muestra todos los foros con sus horarios y eventos
     */

    public function getForos()
    {
        $foros = Foro::where('status',1)->get();
        return view('reportes.foros',compact('foros'));
    }

    /**
     * @param Request $request
     * @return Request
     * Retorna un documento de excel con un reporte de todos los foros en base a una fecha
     */
    public function reporteForoFecha(Request $request)
    {
        $fecha = $request->get('fecha');
        return Excel::download(new ExportForoFecha($fecha),'reporte-foro-fecha-'.$fecha.".xlsx");
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Retorna un documento en excel en el cual se exporta el reporte
     * por foro asi como sus horarios y eventos
     */

    public function reportePorForo(Request $request)
    {
        $foro_id = $request->get('foro_id');
        $foro = Foro::where('id',$foro_id)->first();
        return Excel::download(new ExportarPorForo($foro_id),'reporte-foro-'.$foro->nombre.".xlsx");
    }

    /**
     * retorna todos los foros con sus eventos ordenados por dia y hora
     */

    public function reporteForos()
    {
        return Excel::download(new ExportForos(),'reportes-foros.xlsx');
    }



    /**
     * Nos retorna a la vista para seleccionar el reporte
     * para las actividades con sus respectivos requerimientos
     */

    public function requerimientosActividad()
    {
        $foros = Foro::where('status',1)->get();
        return view('reportes.reporte-actividades-home',compact('foros'));
    }

    public function reporteActividadesPorFecha(Request $request){
        return Excel::download(new ExportarEventosRequerimientosFecha($request->get('fecha')),'reporte-eventos-dia'.$request->get('fecha').'.xlsx');
    }

    /**
     * Nos retorna un documento de excel con actividades , con horariosm y requerimientos
     */
    public function reportePorForoRequerimientos(Request $request)
    {
        $foro = Foro::where('id',$request->get('foro_id'))->first();
        return Excel::download(new ExportarReporteForoRequerimientos($request->get('foro_id')),'reporte-foro-'.$foro->name.'.xlsx');
    }

    public function reporteEventos()
    {
        return Excel::download(new EventosExport(),'eventos.xlsx');
    }

    public function librosPresentadores(Request $request)
    {

    }



}

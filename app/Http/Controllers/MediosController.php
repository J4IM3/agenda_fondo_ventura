<?php

namespace App\Http\Controllers;

use App\Http\Repositories\MediosRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
class MediosController extends Controller
{
    /**
     * @var MediosRepo
     */
    private $mediosRepo;

    public function __construct(MediosRepo $mediosRepo)
    {
        $this->mediosRepo = $mediosRepo;
    }

    public function index()
    {
        return view('prensa_medios.index');
    }

    public function getData()
    {
        $medios = $this->mediosRepo->all();
        return DataTables::of($medios)->make(true);
    }

    public function create(Request $request)
    {
        return $this->mediosRepo->create($request->all());
    }

    public function editar(Request $request)
    {
        return $this->mediosRepo->edit($request->all());
    }
}

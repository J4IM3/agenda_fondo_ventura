<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-03-25
 * Time: 00:58
 */

namespace App\Http\Export;
use App\Http\Entities\PruebaSonido;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Facades\DB;
class ExportarPorForo implements FromCollection, WithMapping, WithHeadings
{
    protected $foro_id;

    function __construct($foro_id)
    {
        $this->foro_id = $foro_id;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        $foro_id = $this->foro_id;
        $eventos = DB::select("select fo.nombre,ev.nombre_evento,de.fecha as fecha,de.hora_inicio as hora_inicio,de.hora_fin as hora_fin
        from foros fo
inner join details_events de on fo.id=de.sede_id
inner join eventos ev on ev.id=de.event_id
where de.sede_id= $foro_id
group by ev.nombre_evento,fo.nombre,de.fecha ,de.hora_inicio ,de.hora_fin");

        $soundTests = PruebaSonido::with('eventSoundTest','forumSoundTest')->where('foro_id', $foro_id)->get();
        foreach ($soundTests as $soundTest)
        {
            $soundTest->nombre = $soundTest->forumSoundTest->nombre;
            $soundTest->nombre_evento = "Prueba de sonido para: ".$soundTest->eventSoundTest->nombre_evento ;
            array_push($eventos, $soundTest);
        }
        return $collection = Collection::make($eventos);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Foro',
            'Fecha',
            'Hora inicio',
            'Hora fin',
            'Evento'
        ] ;
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($collection): array
    {
        return [
            $collection->nombre,
            $collection->fecha,
            $collection->hora_inicio,
            $collection->hora_fin,
            $collection->nombre_evento,
        ] ;
    }
}

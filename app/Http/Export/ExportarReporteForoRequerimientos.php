<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-03-26
 * Time: 23:43
 */

namespace App\Http\Export;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Illuminate\Support\Collection as Collection;


class ExportarReporteForoRequerimientos  implements FromCollection,WithHeadings,WithMapping
{
    protected $foro_id;

    function __construct($foro_id)
    {
        $this->foro_id = $foro_id;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        $foro_id = $this->foro_id;


        $eventos = DB::select("
            select ev.nombre_evento as nombre,fo.nombre as lugar,ev.fecha as fecha,ev.hora_inicio as inicio,ev.hora_fin as fin, ev.requirimientos as requerimientos from eventos ev
            inner join foros fo on ev.foro_id = fo.id
            where ev.foro_id = $foro_id
        ");

        return $row = Collection::make($eventos);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Nombre',
            'Lugar',
            'Fecha',
            'Hora inicio',
            'Hora fin',
            'Requerimientos'
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row->nombre,
            $row->lugar,
            $row->fecha,
            $row->inicio,
            $row->fin,
            $row->requerimientos,
        ];
    }
}
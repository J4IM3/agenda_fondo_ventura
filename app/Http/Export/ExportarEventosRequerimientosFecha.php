<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-03-26
 * Time: 18:50
 */

namespace App\Http\Export;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Illuminate\Support\Collection as Collection;

class ExportarEventosRequerimientosFecha implements FromCollection,WithHeadings,WithMapping
{

    protected $fecha;

    function __construct($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        $fecha = $this->fecha;
        $eventos = DB::select("
        select ev.nombre_evento as evento,fo.nombre as lugar,ev.fecha as fecha,ev.hora_inicio as inicio,ev.hora_fin as fin,ev.requirimientos as requerimientos from eventos ev
        inner join foros fo on fo.id = ev.foro_id
        where fecha = '$fecha'
        ");
        return $collection = Collection::make($eventos);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return[
            'evento',
            'Lugar',
            'Fecha',
            'Hora inicio',
            'Hora fin',
            'Requerimientos'
        ];

    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($collection): array
    {
        return [
            $collection->evento,
            $collection->lugar,
            $collection->fecha,
            $collection->inicio,
            $collection->fin,
            $collection->requerimientos

        ];
    }
}
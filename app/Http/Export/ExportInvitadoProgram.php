<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-03-24
 * Time: 18:32
 */

namespace App\Http\Export;
use App\Http\Entities\Invitado;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Illuminate\Support\Collection as Collection;


class ExportInvitadoProgram  implements FromCollection, WithMapping, WithHeadings
{

    /**
     * @return Collection
     */
    public function collection()
    {
        $invitados = Invitado::select()->with('ParticipanteEnEventos.eventos.programacionTipo')->get();
        $tipoProgramacion = [];
        foreach ($invitados as $invitado) {
            foreach ($invitado->ParticipanteEnEventos as $eventos)
            {
                foreach ($eventos->eventos   as $evento)
                {
                    $tipoProgramacion[] = $evento->programacionTipo->nombre;
                }
                $invitado->tipo = implode(", ",$tipoProgramacion) ;
            }

            unset($tipoProgramacion);
        }
        return $collectInvitados = Collection::make($invitados);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Nombre',
            'Programacion'
        ] ;

    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($collectInvitados): array
    {
        return [
            $collectInvitados->name,
            $collectInvitados->tipo,
        ] ;
    }
}
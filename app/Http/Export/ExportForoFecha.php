<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-03-25
 * Time: 00:34
 */

namespace App\Http\Export;
use App\Http\Entities\PruebaSonido;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Facades\DB;

class ExportForoFecha implements FromCollection, WithMapping, WithHeadings
{
    protected $fecha;

    function __construct($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return Collection
     */
    public function collection()
    {

        $fecha = $this->fecha;
        /*
        $eventos = DB::select("select nombre,fecha,hora_inicio,hora_fin,nombre_evento from foros fo
                                inner join eventos ev on fo.`id`=ev.foro_id
                                where ev.fecha = '$fecha'
                                order by fo.nombre,ev.fecha,ev.hora_inicio");
        */
        $eventos = DB::select("select nombre as nombre,dv.fecha as fecha,dv.hora_inicio as hora_inicio,
dv.hora_fin as hora_fin,nombre_evento from foros fo
inner join details_events dv on fo.`id`=dv.sede_id
inner join eventos ev on ev.id=dv.event_id
where dv.fecha = '$fecha'
order by fo.nombre,ev.fecha,ev.hora_inicio,nombre_evento");
        $soundTests = PruebaSonido::with('eventSoundTest','forumSoundTest')->where('fecha', $fecha)->get();
        foreach ($soundTests as $soundTest)
        {
            $soundTest->nombre = $soundTest->forumSoundTest->nombre;
            $soundTest->nombre_evento = "Prueba de sonido para: ".$soundTest->eventSoundTest->nombre_evento ;
            array_push($eventos, $soundTest);
        }
        return $collection = Collection::make($eventos);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Foro',
            'Fecha',
            'Hora inicio',
            'Hora fin',
            'Evento'
        ] ;
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($collection): array
    {
        return [
            $collection->nombre,
            $collection->fecha,
            $collection->hora_inicio,
            $collection->hora_fin,
            $collection->nombre_evento,
        ] ;
    }
}

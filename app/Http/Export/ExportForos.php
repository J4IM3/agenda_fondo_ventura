<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-03-25
 * Time: 01:15
 */

namespace App\Http\Export;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Facades\DB;
use App\Http\Entities\PruebaSonido;
class ExportForos implements FromCollection, WithMapping, WithHeadings
{

    /**
     * @return Collection
     */
    public function collection()
    {
        $eventos = DB::select("select fo.nombre,nombre_evento,et.nombre as tipo,etp.nombre as programacion,ev.fecha,ev.hora_inicio,ev.hora_fin,requirimientos from foros fo
inner join details_events de on fo.`id`=de.sede_id
inner join eventos ev on de.event_id=ev.id
inner join evento_tipos et on ev.tipo_id=et.id
inner join evento_tipos etp on ev.programacion_id=etp.id
order by ev.fecha,ev.hora_inicio,fo.nombre,requirimientos");

        $soundTests = PruebaSonido::with('eventSoundTest','forumSoundTest')->get();
        foreach ($soundTests as $soundTest)
        {
            $soundTest->nombre = $soundTest->forumSoundTest->nombre;
            $soundTest->nombre_evento = "Prueba de sonido para: ".$soundTest->eventSoundTest->nombre_evento ;
            array_push($eventos, $soundTest);
        }
        return $collection = Collection::make($eventos);

    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Sede',
            'Evento',
            'Tipo de evento',
            'Programación',
            'Fecha',
            'Hora inicio',
            'Hora fin',
            'Requerimientos'
        ] ;
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($collection): array
    {
        return [
            $collection->nombre,
            $collection->nombre_evento,
            $collection->tipo,
            $collection->programacion,
            $collection->fecha,
            $collection->hora_inicio,
            $collection->hora_fin,
            $collection->requirimientos,
        ] ;
    }
}

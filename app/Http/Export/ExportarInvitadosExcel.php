<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-03-23
 * Time: 12:49
 */

namespace App\Http\Export;
use App\Http\Entities\Invitado;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Illuminate\Support\Collection as Collection;

use App\Http\Entities\Itinerarios;
class  ExportarInvitadosExcel implements FromCollection, WithMapping, WithHeadings
{
    protected $fecha;

    function __construct($fecha) {
        $this->fecha = $fecha;
    }

    public function collection()
    {
        $invitado = Invitado::with('itinerarioVuelo')->get();
        $invitados =[];

        foreach ($invitado as $item)
        {
            if (!empty($item->itinerarioVuelo ))
            {
                $item->invitado_id = $item->id;
                $item->invitado_nombre = $item->name;

                if ($item->itinerarioVuelo->fecha_salida == $this->fecha){
                    $item->invitado_fecha_salida = "Sale hoy";
                }

                if ($item->itinerarioVuelo->fecha_salida > $this->fecha)
                {
                    $item->invitado_fecha_salida = $item->itinerarioVuelo->fecha_salida;
                }
                if ($item->itinerarioVuelo->fecha_salida < $this->fecha){
                    $item->invitado_fecha_salida  = "Salieron en dias anteriores";
                }

            }else{
                $item->invitado_id= $item->id;
                $item->invitado_nombre = $item->name;
                $item->invitado_fecha_salida  = "No cuenta con fecha de salida";
            }
            $invitados[]=$item;
        }


        return $collection = Collection::make($invitados);
    }

    public function map($collection) : array {
        return [
            $collection->invitado_id,
            $collection->invitado_nombre,
            $collection->invitado_fecha_salida
        ] ;


    }

    public function headings() : array {
        return [
            '#',
            'Nombre',
            'Fecha salida'
        ] ;
    }
}
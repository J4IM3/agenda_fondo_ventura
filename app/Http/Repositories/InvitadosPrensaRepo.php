<?php


namespace App\Http\Repositories;


class InvitadosPrensaRepo
{
    public function getPrensa($prensaEventos)
    {
        foreach ($prensaEventos as $evento)
        {
            $evento->nombre = $evento->prensaInvitado->name;
            $evento->tipo = $evento->prensaTipos->nombre;
            $evento->medio = $evento->prensaMedios->nombre;
        }
        return $prensaEventos;
    }
}

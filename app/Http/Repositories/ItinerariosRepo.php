<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\Honorarios;
use App\Http\Entities\Itinerarios;
use App\Http\Services\ChangeStatus;
use Illuminate\Http\Request;
use Carbon\Carbon;
class ItinerariosRepo extends BaseRepo {

    public function getModel()
    {
        return new Itinerarios();
    }

    public function save($data){
        $guestItinerary = Itinerarios::where('invitado_id',$data['invitado_id'])->first();
        if (!empty($guestItinerary))
        {
            return ['success' => 'error','msg' => 'Este invitado ya tiene un itinerario creado '];
        }

        $salida = Carbon::parse($data['salida_estancia']);
        $llegada = Carbon::parse($data['llegada_estancia']);
        $diasDiferencia = $salida->diffInDays($llegada);


        $itinerario = Itinerarios::create([
            'invitado_id'=>$data['invitado_id'],
            'origen_internacional'=>$data['origen_internacional'],
            'aerolinea_internacional_id'=>$data['aerolinea_internacional_id'],
            'num_vuelo_internacional'=>$data['num_vuelo_internacional'],
            'fecha_salida_internacional_origen'=>$data['fecha_salida_internacional_origen'],
            'hora_salida_internacional_origen'=>$data['hora_salida_internacional_origen'],
            'fecha_llegada_internacional_origen'=>$data['fecha_llegada_internacional_origen'],
            'hora_llegada_internacional_origen'=>$data['hora_llegada_internacional_origen'],
            'destino_internacional' => $data['destino_internacional'],
            'aerolinea_retorno_internacional_id' => $data['aerolinea_retorno_internacional_id'],
            'num_vuelo_retorno_internacional' => $data['num_vuelo_retorno_internacional'],
            'fecha_regreso'=>$data['fecha_regreso'],
            'hora_regreso'=>$data['hora_regreso'],

            'origen'=>$data['origen'],
            'aerolinea_id'=>$data['aerolinea_id'],
            'num_vuelo'=>$data['num_vuelo'],
            'fecha_origen_salida'=>$data['fecha_origen_salida'],
            'hora_origen_salida'=>$data['hora_origen_salida'],
            'fecha_llegada'=>$data['fecha_llegada'],
            'hora_llegada'=>$data['hora_llegada'],


            'origen_salida'=>$data['origen_salida'],
            'transporte_retorno'=>$data['transporte_retorno'],
            'fecha_salida'=>$data['fecha_salida'],
            'hora_salida'=>$data['hora_salida'],

            'hotel'=>$data['hotel'],
            'estancia'=>$diasDiferencia,
            'observaciones'=>$data['observaciones'],
            'anfitrion_id'=>$data['anfitrion_id'],
            'llegada_estancia'=>$data['llegada_estancia'],
            'salida_estancia'=>$data['salida_estancia'],

        ]);

        if (empty($itinerario)){
            return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte al administrador'];
        }
        return ['success'=>'success','msg'=>'Registro guardado'];
    }

    public function addHonorarios($data)
    {
        $honorarios = Honorarios::where('invitado_id',$data['invitado_id'])->first();
        if(empty($honorarios))
        {
            $honorarios = Honorarios::create([
                'invitado_id'=>$data['invitado_id'],
                'honorarios'=>$data['honorarios'],
                'monto_hotel'=>$data['monto_hotel'],
                'monto_alimentos'=>$data['monto_alimentos'],
                'monto_viaticos'=>$data['monto_viaticos'],
                'monto_transporte'=>$data['monto_transporte']
            ]);
            if (empty($honorarios)){
                return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte al administrador'];
            }
            return ['success'=>'success','msg'=>'Registro guardado'];
        }

        $honorarios->invitado_id = $data['invitado_id'];
        $honorarios->honorarios = $data['honorarios'];
        $honorarios->monto_hotel = $data['monto_hotel'];
        $honorarios->monto_alimentos = $data['monto_alimentos'];
        $honorarios->monto_viaticos = $data['monto_viaticos'];
        $honorarios->monto_transporte = $data['monto_transporte'];
        if (!$honorarios->save()){
            return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte al administrador'];
        }
        return ['success'=>'success','msg'=>'Registro actualizado'];
    }

    public function update($data)
    {
        $diff = strtotime($data['fecha_salida'])- strtotime($data['fecha_llegada']);
        $estancia = $diff/86400;
        $itinerario = $this->find($data['id']);
        $itinerario->invitado_id = $data['invitado_id'];

        $itinerario->origen_internacional = $data['origen_internacional'];
        $itinerario->aerolinea_internacional_id = $data['aerolinea_internacional_id'];
        $itinerario->num_vuelo_internacional = $data['num_vuelo_internacional'];
        $itinerario->fecha_salida_internacional_origen = $data['fecha_salida_internacional_origen'];
        $itinerario->hora_salida_internacional_origen = $data['hora_salida_internacional_origen'];
        $itinerario->fecha_llegada_internacional_origen = $data['fecha_llegada_internacional_origen'];
        $itinerario->hora_llegada_internacional_origen = $data['hora_llegada_internacional_origen'];
        $itinerario->destino_internacional = $data['destino_internacional'];
        $itinerario->aerolinea_retorno_internacional_id = $data['aerolinea_retorno_internacional_id'];
        $itinerario->num_vuelo_retorno_internacional = $data['num_vuelo_retorno_internacional'];
        $itinerario->fecha_regreso = $data['fecha_regreso'];
        $itinerario->hora_regreso = $data['hora_regreso'];
        $itinerario->origen = $data['origen'];
        $itinerario->aerolinea_id = $data['aerolinea_id'];
        $itinerario->num_vuelo = $data['num_vuelo'];
        $itinerario->fecha_origen_salida = $data['fecha_origen_salida'];
        $itinerario->hora_origen_salida = $data['hora_origen_salida'];
        $itinerario->fecha_llegada = $data['fecha_llegada'];
        $itinerario->hora_llegada = $data['hora_llegada'];
        $itinerario->origen_salida = $data['origen_salida'];
        $itinerario->transporte_retorno = $data['transporte_retorno'];
        $itinerario->fecha_salida = $data['fecha_salida'];
        $itinerario->hora_salida = $data['hora_salida'];
        $itinerario->hotel = $data['hotel'];
        $itinerario->estancia = $estancia;
        $itinerario->observaciones = $data['observaciones'];
        $itinerario->anfitrion_id = $data['anfitrion_id'];
        $itinerario->llegada_estancia = $data['llegada_estancia'];
        $itinerario->salida_estancia = $data['salida_estancia'];
        if (!$itinerario->save())
        {
            return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte al administrador'];
        }
        return ['success'=>'success','msg'=>'Registro actualizado'];

    }



}

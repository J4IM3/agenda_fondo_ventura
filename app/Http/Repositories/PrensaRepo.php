<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\Evento_participantes;
use App\Http\Entities\FiloandoInvitados;
use App\Http\Entities\Invitado;
use App\Http\Entities\Prensa;
use App\Http\Entities\Social;
use App\Http\Services\ChangeStatus;
use App\Http\Services\ComparaEventos;
use App\Http\Services\ComparaFiloando;
use App\Http\Services\CompararPrensa;
use App\Http\Services\CompararService;
use App\Http\Services\CompararSocial;
use Illuminate\Http\Request;

class PrensaRepo extends BaseRepo {

    /**
     * @var InvitadosRepo
     */
    private $invitadosRepo;
    /**
     * @var EventosRepo
     */
    private $eventosRepo;
    /**
     * @var CompararService
     */
    private $compararService;
    /**
     * @var ComparaEventos
     */
    private $comparaEventos;
    /**
     * @var ComparaFiloando
     */
    private $comparaFiloando;
    /**
     * @var CompararPrensa
     */
    private $compararPrensa;
    /**
     * @var CompararSocial
     */
    private $compararSocial;

    public function getModel()
    {
        return new Prensa();
    }
    public function __construct(InvitadosRepo $invitadosRepo,
                                CompararService $compararService,
                                ComparaEventos $comparaEventos,
                                ComparaFiloando $comparaFiloando,
                                CompararPrensa $compararPrensa,
                                CompararSocial $compararSocial)
    {
        $this->invitadosRepo = $invitadosRepo;
        $this->compararService = $compararService;
        $this->comparaEventos = $comparaEventos;
        $this->comparaFiloando = $comparaFiloando;
        $this->compararPrensa = $compararPrensa;
        $this->compararSocial = $compararSocial;
    }

    public function save($data)
    {
        if ($data['hora_fin'] <= $data['hora_inicio'])
        {
            return ['success' => 'error','msg' => 'La hora final no debe ser menor o igual a la hora de inicio'];
        }

        //Compara eventos filo
        $eventos = $this->comparaEventos->compararEvento($data['invitado_id'],$data['fecha'],$data['hora_inicio'],$data['hora_fin'],'0');
        if ($eventos != "false")
        {
            return $eventos;
        }

        //Compara eventos de filoando
        $eventosFiloando = $this->comparaFiloando->comparaFiloando($data['invitado_id'],$data['fecha'],$data['hora_inicio'],$data['hora_fin'],'0');
        if ($eventosFiloando != "false")
        {
            return $eventosFiloando;
        }
        //Compara eventos de prensa
        $eventosPrensa = $this->compararPrensa->compararPrensa($data['invitado_id'],$data['fecha'],$data['hora_inicio'],$data['hora_fin'],'0');
        if ($eventosPrensa != "false")
        {
            return $eventosPrensa;
        }

        $eventoSocial = $this->compararSocial->compararSocial($data['invitado_id'],$data['fecha'],$data['hora_inicio'],$data['hora_fin'],'0');
        if ($eventoSocial != "false")
        {
            return $eventoSocial;
        }


        foreach ($data['invitado_id'] as $guest)
        {
            Prensa::create([
                'invitado_id'=>$guest,
                'participante'=>$data['participante'],
                'medio_id'=>$data['medio_id'],
                'entrevistador'=>$data['entrevistador'],
                'fecha'=>$data['fecha'],
                'hora_inicio'=>$data['hora_inicio'],
                'hora_fin'=>$data['hora_fin'],
                'lugar'=>$data['lugar'],
                'tipo_evento_id'=>$data['tipo_evento_id']

            ]);
        }

        return ['success'=>'success','msg'=>'Registro guardado'];


    }

    public function update($data)
    {
        if ($data['hora_fin'] <= $data['hora_inicio'])
        {
            return ['success' => 'error','msg' => 'La hora final no debe ser menor o igual a la hora de inicio'];
        }

        //Compara eventos filo
        $eventos = $this->comparaEventos->compararEvento($data['invitado_id'],$data['fecha'],$data['hora_inicio'],$data['hora_fin'],'0');
        if ($eventos != "false")
        {
            return $eventos;
        }

        //Compara eventos de filoando
        $eventosFiloando = $this->comparaFiloando->comparaFiloando($data['invitado_id'],$data['fecha'],$data['hora_inicio'],$data['hora_fin'],"0");
        if ($eventosFiloando != "false")
        {
            return $eventosFiloando;
        }
        //Compara eventos de prensa
        $eventosPrensa = $this->compararPrensa->compararPrensa($data['invitado_id'],$data['fecha'],$data['hora_inicio'],$data['hora_fin'],$data['id']);
        if ($eventosPrensa != "false")
        {
            return $eventosPrensa;
        }

        $eventoSocial = $this->compararSocial->compararSocial($data['invitado_id'],$data['fecha'],$data['hora_inicio'],$data['hora_fin'],'0');
        if ($eventoSocial != "false")
        {
            return $eventoSocial;
        }

        $event = Prensa::where('id',$data['id'])->first();
        foreach ($data['invitado_id'] as $guest)
        {
            $event->invitado_id = $guest;
            $event->participante = $data['participante'];
            $event->medio_id = $data['medio_id'];
            $event->entrevistador  = $data['entrevistador'];
            $event->fecha = $data['fecha'];
            $event->hora_inicio = $data['hora_inicio'];
            $event->hora_fin = $data['hora_fin'];
            $event->lugar = $data['lugar'];
            $event->tipo_evento_id = $data['tipo_evento_id'];
            $event->save();
        }

        return ['success' => 'success','msg'=> 'Evento actualizado'];


    }



    //$fecha;$hora_inicio,$hora_fin,$data
    private function comparar($typo,$dataEvent, $data){

        foreach ($dataEvent as $evento){
            $comparar = $this->compararService->comparar($typo,$evento->fecha,$evento->hora_inicio,$evento->hora_fin,$data);
            if ($comparar != "true"){
                return $comparar;
            }
        }
        return $comparar;
    }

    public function message($fecha,$hora_inicio,$hora_fin,$tipo){
        return ['success'=>'error','msg'=>'Exite un evento de '.$tipo." el dia ".$fecha." de las ".$hora_inicio. " a ".$hora_fin];
    }

    public function delete($id){
        $prensa  = $this->find($id);
        if ($prensa->delete()){
            return ['success'=>'success','msg'=>'Registro eliminado'];
        }
    }

    /*
    public function update($data){
        $inicio = $this->compararService->convertir($data['hora_inicio']);
        $fin = $this->compararService->convertir($data['hora_fin']);


        $compararHoras = $this->compararService->compararHoras($data['hora_inicio'],$data['hora_fin']);
        if ($compararHoras != "true"){
            return $compararHoras;
        }

        //inicia comparaciones
        $evento = Evento_participantes::with('eventos','invitadosNombre')->where('invitado_id',$data['invitado_id'])->get();

        if (!empty($evento)){
            foreach ($evento as $value)
            {
                $compararEvento = $this->compararService->comparar($value['eventos'],$inicio,$fin,$data['fecha'],"la filo");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," la filo de ".$value['invitadosNombre'][0]->name);
                }
            }
        }

        //comparar Filoando

        $filoando = FiloandoInvitados::with('eventosFiloando','invitadosNombre')
            ->where('invitado_id',$data['invitado_id'])
            ->get();

        if (!empty($filoando)){
            foreach ($filoando as $value)
            {
                $compararEvento = $this->compararService->compararFiloando($value->eventosFiloando,$inicio,$fin,$data['fecha'],"la filo");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_traslado_ida,$compararEvento->hora_traslado_regreso," tipo FILOANDO de ".$value['invitadosNombre'][0]->name);
                }
            }
        }

        $prensa = Prensa::with('prensa')
            ->where('invitado_id',$data['invitado_id'])
            ->where('id','=!',$data['id'])
            ->get();

        if (!empty($prensa))
        {
            $compararEvento = $this->compararService->comparar($prensa,$inicio,$fin,$data['fecha'],"prensa");
            if ($compararEvento != "true" ){
                return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," prensa  de  ".$compararEvento->prensa[0]->name);
            }
        }

        $social = Social::with('socialInvitado')
            ->where('invitado_id',$data['invitado_id'])
            ->get();
        if (!empty($social))
        {
            $compararEvento = $this->compararService->comparar($social,$inicio,$fin,$data['fecha'],"social");
            if ($compararEvento != "true" ){
                return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," tipo social de ".$compararEvento->socialInvitado[0]->name);
            }
        }


        $prensa = $this->find($data['id']);
        $prensa->invitado_id = $data['invitado_id'];
        $prensa->participante = $data['participante'];
        $prensa->medio_id = $data['medio_id'];
        $prensa->entrevistador = $data['entrevistador'];
        $prensa->fecha = $data['fecha'];
        $prensa->hora_inicio = $data['hora_inicio'];
        $prensa->hora_fin = $data['hora_fin'];
        $prensa->lugar = $data['lugar'];
        $prensa->save();


        /*
        $prensa = Prensa::create([
            'invitado_id'=>$data['invitado_id'],
            'participante' => $data['participante'],
            'medio' => $data['medio'],
            'entrevistador' =>$data['entrevistador'],
            'fecha' => $data['fecha'],
            'hora_inicio' => $data['hora_inicio'],
            'hora_fin' => $data['hora_fin'],
            'lugar' => $data['lugar'],
            'tipo_evento_id'=>$data['tipo_evento_id']
        ]);


        if ($prensa){
            return ['success'=>'success','msg'=>'Registro actualizado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error, consulta a tu administrador'];

    }
    */


    /*
     public function save($data){
        $inicio = $this->compararService->convertir($data['hora_inicio']);
        $fin = $this->compararService->convertir($data['hora_fin']);


        $compararHoras = $this->compararService->compararHoras($data['hora_inicio'],$data['hora_fin']);
        if ($compararHoras != "true"){
            return $compararHoras;
        }

        //inicia comparaciones
            $evento = Evento_participantes::with('eventos','invitadosNombre')->where('invitado_id',$data['invitado_id'])->get();

            if (!empty($evento)){
                foreach ($evento as $value)
                {
                    $compararEvento = $this->compararService->comparar($value['eventos'],$inicio,$fin,$data['fecha'],"la filo");
                    if ($compararEvento != "true" ){
                        return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," la filo de ".$value['invitadosNombre'][0]->name);
                    }
                }
            }

            //comparar Filoando

            $filoando = FiloandoInvitados::with('eventosFiloando','invitadosNombre')
                ->where('invitado_id',$data['invitado_id'])
                ->get();

            if (!empty($filoando)){
                foreach ($filoando as $value)
                {
                    $compararEvento = $this->compararService->compararFiloando($value->eventosFiloando,$inicio,$fin,$data['fecha'],"la filo");
                    if ($compararEvento != "true" ){
                        return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_traslado_ida,$compararEvento->hora_traslado_regreso," tipo FILOANDO de ".$value['invitadosNombre'][0]->name);
                    }
                }
            }

            $prensa = Prensa::with('prensa')
                ->where('invitado_id',$data['invitado_id'])
                ->get();

            if (!empty($prensa))
            {
                $compararEvento = $this->compararService->comparar($prensa,$inicio,$fin,$data['fecha'],"prensa");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," prensa  de  ".$compararEvento->prensa[0]->name);
                }
            }

            $social = Social::with('socialInvitado')
                ->where('invitado_id',$data['invitado_id'])
                ->get();
            if (!empty($social))
            {
                $compararEvento = $this->compararService->comparar($social,$inicio,$fin,$data['fecha'],"social");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," tipo social de ".$compararEvento->socialInvitado[0]->name);
                }
            }

            Prensa::create([
                'invitado_id'=>$data['invitado_id'],
                'participante'=>$data['participante'],
                'medio_id'=>$data['medio_id'],
                'entrevistador'=>$data['entrevistador'],
                'fecha'=>$data['fecha'],
                'hora_inicio'=>$data['hora_inicio'],
                'hora_fin'=>$data['hora_fin'],
                'lugar'=>$data['lugar'],
                'tipo_evento_id'=>$data['tipo_evento_id']

            ]);
            return ['success'=>'success','msg'=>'Registro guardado con exito'];

    }
    */

}

<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;

use App\Http\Entities\DetailEvents;
use App\Http\Entities\EventBooks;
use App\Http\Entities\Evento_moderadores;
use App\Http\Entities\Eventos;
use App\Http\Entities\Evento_participantes;

use App\Http\Entities\EventoTipo;
use App\Http\Entities\Filoando;
use App\Http\Entities\FiloandoInvitados;
use App\Http\Entities\Invitado;
use App\Http\Entities\Moderadores;
use App\Http\Entities\Prensa;
use App\Http\Entities\Presentadores;
use App\Http\Entities\PruebaSonido;
use App\Http\Entities\Social;
use App\Http\Services\ChangeStatus;
use App\Http\Services\ComparaEventos;
use App\Http\Services\ComparaFiloando;
use App\Http\Services\ComparaForos;
use App\Http\Services\ComparaPruebaSonido;
use App\Http\Services\CompararPrensa;
use App\Http\Services\CompararService;
use App\Http\Services\CompararSocial;
use App\Http\Services\EventoParticipantesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventosRepo extends BaseRepo
{

    /**
     * @var PrensaRepo
     */
    private $prensaRepo;
    /**
     * @var CompararService
     */
    private $compararService;
    /**
     * @var EventoParticipantesService
     */
    private $eventoParticipantesService;
    /**
     * @var ComparaEventos
     */
    private $comparaEventos;
    /**
     * @var ComparaFiloando
     */
    private $comparaFiloando;
    /**
     * @var CompararPrensa
     */
    private $compararPrensa;

    private $compararSocial;
    /**
     * @var ComparaForos
     */
    private $comparaForos;
    /**
     * @var ComparaPruebaSonido
     */
    private $comparaPruebaSonido;

    public function getModel()
    {
        return new Eventos();
    }

    public function __construct(PrensaRepo                 $prensaRepo,
                                CompararService            $compararService,
                                EventoParticipantesService $eventoParticipantesService,
                                ComparaEventos             $comparaEventos,
                                ComparaFiloando            $comparaFiloando,
                                CompararPrensa             $compararPrensa,
                                CompararSocial             $compararSocial,
                                ComparaForos               $comparaForos,
                                ComparaPruebaSonido        $comparaPruebaSonido)
    {
        $this->prensaRepo = $prensaRepo;
        $this->compararService = $compararService;
        $this->eventoParticipantesService = $eventoParticipantesService;
        $this->comparaEventos = $comparaEventos;
        $this->comparaFiloando = $comparaFiloando;
        $this->compararPrensa = $compararPrensa;
        $this->compararSocial = $compararSocial;
        $this->comparaForos = $comparaForos;
        $this->comparaPruebaSonido = $comparaPruebaSonido;
    }

    public function saveEvento($data)
    {
        $invitados = [];
        //Compara la hora final con la hora inicial de un evento, la hora final no puede ser menor a la hora inicial
        if ($this->compararService->convertir($data['hora_fin']) <= $this->compararService->convertir($data['hora_inicio'])) {
            return ['success' => 'error', 'msg' => 'La hora final no puede ser menor o igual a la hora de inicio'];
        }

        //guarda en la variable invitados los id's de estos para ser utilizados al momento de crear el
        foreach ($data['invitado_id'] as $item) {
            $invitados[] = $item;
        }

        //Inserta los ids que viene en el input moderador al array $data['invitado_id']
        if (!empty($data['moderador_id'])) {
            foreach ($data['moderador_id'] as $datum) {
                if (in_array($datum, $data['invitado_id'])) {
                    return ['success' => 'error', 'msg' => 'El invitado al evento y el moderador no pueden ser la misma persona.'];
                } else {
                    array_push($data['invitado_id'], $datum);
                }
            }
        }

        //Inserta los ids que viene en el input presentador al array $data['invitado_id']
        if (!empty($data['presentador_id'])) {
            foreach ($data['presentador_id'] as $datum) {
                if (in_array($datum, $data['invitado_id'])) {
                    return ['success' => 'error', 'msg' => 'El invitado al evento y el presentador no pueden ser la misma persona.'];
                } else {
                    array_push($data['invitado_id'], $datum);
                }
            }
        }

        //Checa si el foro esta libre para un evento
        $foros = $this->comparaForos->compararEvento($data['foro_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($foros != "false") {
            return $foros;
        }

        //Compara si el invitado tiene un evento en el dia y hora dada.
        $eventos = $this->comparaEventos->compararEvento($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($eventos != "false") {
            return $eventos;
        }

        if (!isset($data['evento_id'])) {
            $data['evento_id'] = 0;
        }

        //revisa foros de prueba de sonido
        $pruebaSonido = $this->comparaPruebaSonido->compararEvento($data['foro_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], $data['evento_id']);
        if ($pruebaSonido != "false") {
            return $pruebaSonido;
        }

        /**
         * Compara que los invitados no tengan pruebas de sonido
         */
        $pruebaSonido = $this->comparaPruebaSonido->compararEventoInvitado($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], 0);
        if ($pruebaSonido != "false") {
            return $pruebaSonido;
        }

        //Compara eventos de filoando
        $eventosFiloando = $this->comparaFiloando->comparaFiloando($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($eventosFiloando != "false") {
            return $eventosFiloando;
        }
        //Compara eventos de prensa
        $eventosPrensa = $this->compararPrensa->compararPrensa($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($eventosPrensa != "false") {
            return $eventosPrensa;
        }

        //Compara eventos Sociales
        $eventoSocial = $this->compararSocial->compararSocial($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($eventoSocial != "false") {
            return $eventoSocial;
        }

        $evento = $this->crearEvento($data);

        //Guarda la posicion del index donde va el libro seleccionado para el promotor

        $positionBookForPresenter = !empty($data['title_for_promotor']) ? $this->getPositionBookForPresenter($data['title_for_promotor']) : null;

        //Guarda los libros  para el evento
        $this->saveBooksForEvent($data, $positionBookForPresenter, $evento->id);


        //guardar participantes
        $this->guardarParticipantes($invitados, $evento->id);
        //guardar si es que existen, moderadores
        if (!empty($data['moderador_id'])) {
            $this->guardarModeradores($data['moderador_id'], $evento->id);
        }

        //guardar si es que existen, presentadores
        if (!empty($data['presentador_id'])) {
            $this->guardarPresentadores($data['presentador_id'], $evento->id);
        }

        return ['success' => 'success', 'msg' => 'Evento generado con exito.'];
    }



    /**
     * Inicio
     */


    /**
     * Fin
     */


    public function crearEvento($data)
    {
        $evento = Eventos::create([
            'nombre_evento' => $data['nombre_evento'],
            'descripcion' => $data['descripcion'],
            'tipo_id' => $data['tipo_id'],
            'programacion_id' => $data['programacion_id'],
            'ciclo' => $data['ciclo'],
            'requirimientos' => $data['requirimientos'],
            'status' => 1,
            'colaborador' => $data['colaborador'],
            'formato' => $data['formato'],
            'fecha' => $data['fecha'],
            'hora_inicio' => $data['hora_inicio'],
            'hora_fin' => $data['hora_fin'],
            'user_id' => Auth::id(),
        ]);

        if (!empty($evento)) {
            foreach ($data['foro_id'] as $sede)
                DetailEvents::create([
                    'fecha' => $data['fecha'],
                    'hora_inicio' => $data['hora_inicio'],
                    'hora_fin' => $data['hora_fin'],
                    'sede_id' => $sede,
                    'event_id' => $evento->id
                ]);
            return $evento;
        }
    }

    public function guardarParticipantes($invitados, $evento_id)
    {
        foreach ($invitados as $invitado) {
            Evento_participantes::create([
                'invitado_id' => $invitado,
                'evento_id' => $evento_id
            ]);
        }
    }

    /**
     * >Guardar moderadores
     */

    public function guardarModeradores($moderadores, $evento_id)
    {
        foreach ($moderadores as $moderador) {
            Moderadores::create([
                'moderador_id' => $moderador,
                'evento_id' => $evento_id
            ]);
        }

    }

    /**
     * Guarda presentadores
     */

    public function guardarPresentadores($presentadores, $evento_id)
    {
        foreach ($presentadores as $presentador) {
            Presentadores::create([
                'presentador_id' => $presentador,
                'evento_id' => $evento_id
            ]);
        }
    }

    public function getData($data)
    {
        return Eventos::with('invitados.invitadosNombre',
            'invitados.invitadosNombre',
            'foros',
            'sedes',
            'programacion',
            'presentador.presentadores',
            'moderador',
            'eventBooks')
            ->where('id', $data['id'])
            ->first();
    }

    public function update($data)
    {
        $invitados = [];
        //Compara la hora final con la hora inicial de un evento, la hora final no puede ser menor a la hora inicial
        if ($this->compararService->convertir($data['hora_fin']) <= $this->compararService->convertir($data['hora_inicio'])) {
            return ['success' => 'error', 'msg' => 'La hora final no puede ser menor o igual a la hora de inicio'];
        }

        //guarda en la variable invitados los id's de estos para ser utilizados al momento de crear el
        // evento
        foreach ($data['invitado_id'] as $item) {
            $invitados[] = $item;
        }
        //Inserta los ids que viene en el input moderador al array $data['invitado_id']
        if (!empty($data['moderador_id'])) {
            foreach ($data['moderador_id'] as $datum) {
                if (in_array($datum, $data['invitado_id'])) {
                    return ['success' => 'error', 'msg' => 'El invitado al evento y el moderador no pueden ser la misma persona.'];
                } else {
                    array_push($data['invitado_id'], $datum);
                }

            }
        }
        //Inserta los ids que viene en el input presentador al array $data['invitado_id']
        if (!empty($data['presentador_id'])) {
            foreach ($data['presentador_id'] as $datum) {
                if (in_array($datum, $data['invitado_id'])) {
                    return ['success' => 'error', 'msg' => 'El invitado al evento y el presentador no pueden ser la misma persona.'];
                } else {
                    array_push($data['invitado_id'], $datum);
                }
            }
        }

        //Compara foros
        $foros = $this->comparaForos->compararEvento($data['foro_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], $data['id']);
        if ($foros != "false") {
            return $foros;
        }

        //Compara foros de prueba de sonido
        $pruebaSonido = $this->comparaPruebaSonido->compararEvento($data['foro_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], 0);
        if ($pruebaSonido != "false") {
            return $pruebaSonido;
        }

        /**
         * Compara que los invitados no tengan pruebas de sonido
         */
        $pruebaSonidoInvitados = $this->comparaPruebaSonido->compararEventoInvitado($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], 0);
        if ($pruebaSonidoInvitados != "false") {
            return $pruebaSonidoInvitados;
        }


        //Compara eventos filo
        $eventos = $this->comparaEventos->compararEvento($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], $data['id']);
        if ($eventos != "false") {
            return $eventos;
        }

        //Compara eventos de filoando
        $eventosFiloando = $this->comparaFiloando->comparaFiloando($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], "0");
        if ($eventosFiloando != "false") {
            return $eventosFiloando;
        }
        //Compara eventos de prensa
        $eventosPrensa = $this->compararPrensa->compararPrensa($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($eventosPrensa != "false") {
            return $eventosPrensa;
        }

        $eventoSocial = $this->compararSocial->compararSocial($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($eventoSocial != "false") {
            return $eventoSocial;
        }


        $participantes = Evento_participantes::where('evento_id', $data['id'])->get();

        foreach ($participantes as $item) {
            $item->delete();
        }

        foreach ($invitados as $invitado) {
            Evento_participantes::create([
                'evento_id' => $data['id'],
                'invitado_id' => $invitado
            ]);
        }

        $evento = Eventos::where('id', $data['id'])->first();

        if ($evento->user_id != Auth::id()) {
            return ['success' => 'error','msg' => 'No puedes editar un evento que no es tuyo.'];
        }
        $evento->fecha = $data['fecha'];
        $evento->nombre_evento = $data['nombre_evento'];
        $evento->descripcion = $data['descripcion'];
        $evento->tipo_id = $data['tipo_id'];
        $evento->programacion_id = $data['programacion_id'];
        $evento->ciclo = $data['ciclo'];
        $evento->requirimientos = $data['requirimientos'];
        $evento->status = 1;
        $evento->colaborador = $data['colaborador'];
        $evento->formato = $data['formato'];
        $evento->visible = 1;
        $evento->hora_inicio = $data['hora_inicio'];
        $evento->hora_fin = $data['hora_fin'];
        $evento->save();

        $detalle = DetailEvents::where('event_id', $data['id'])->get();
        foreach ($detalle as $item) {
            $item->delete();
        }
        foreach ($data['foro_id'] as $datum) {
            DetailEvents::create([
                'hora_inicio' => $data['hora_inicio'],
                'hora_fin' => $data['hora_fin'],
                'fecha' => $data['fecha'],
                'sede_id' => $datum,
                'event_id' => $evento->id

            ]);
        }


        $books = EventBooks::where('event_id',$data['id'])->get();
        foreach ($books as $book) {
            $book->delete();
        }

        //Guarda la posicion del index donde va el libro seleccionado para el promotor

        $positionBookForPresenter = !empty($data['title_for_promotor']) ? $this->getPositionBookForPresenter($data['title_for_promotor']) : null;

        //Guarda los libros  para el evento
        $this->saveBooksForEvent($data, $positionBookForPresenter, $evento->id);



        $moderadores = Moderadores::where('evento_id', $data['id'])->get();
        $presentadores = Presentadores::where('evento_id', $data['id'])->get();
        if (count($moderadores) > 0) {
            foreach ($moderadores as $item) {
                $moderador = Moderadores::where('moderador_id', $item->moderador_id)->first();
                $moderador->delete();

            }
        }
        if (!empty($data['moderador_id'])) {
            $this->guardarModeradores($data['moderador_id'], $evento->id);
        }

        if (count($presentadores) > 0) {
            foreach ($presentadores as $item) {
                $presentador = Presentadores::where('presentador_id', $item->presentador_id)->first();
                $presentador->delete();
            }
        }

        if (!empty($data['presentador_id'])) {
            $this->guardarPresentadores($data['presentador_id'], $evento->id);
        }

        return ['success' => 'success', 'msg' => 'Evento actualizado'];
    }

    public function getPositionBookForPresenter($titles)
    {
        $x = [];
        foreach ($titles as $key => $promotor)
        {
            if (!empty($promotor))
            {
                $x[] =  $key;
            }
        }
        return $x;
    }

    public function saveBooksForEvent($data, $positionBookForPresenter, $eventId)
    {
        foreach ($data['title'] as $key => $item) {
            if (!empty($item)) {
                if (in_array($key,$positionBookForPresenter)) {
                    $isPresenter = 1;
                    $pos = $key;
                    $quantity = $data['quantity'][$pos] != null ? $data['quantity'][$pos] : 0;
                } else {
                    $isPresenter = 0;
                    $pos = 0;
                    $quantity = 0;
                }

                EventBooks::create([
                    'titulo' => $item,
                    'is_presenter' => $isPresenter,
                    'event_id' => $eventId,
                    'quantity_for_presenter' => $quantity,
                ]);
            }
        }
    }

    /**
     * @param $id
     * @param $invitados
     * @return arrayGuarda en
     * Guarda en la tabla participantes de un evento a los invitados que participaran en este
     */
    public function createInvitedEvent($id, $invitados)
    {
        foreach ($invitados as $invitado) {
            $participantesEvento[] = Evento_participantes::create([
                'invitado_id' => $invitado,
                'evento_id' => $id
            ]);
        }
        return $participantesEvento;
    }

    /**
     * @param $data
     * @return arrayE
     *
     * Elimina un evento
     */
    public function delete($data)
    {
        $moderadores = Moderadores::where('evento_id', $data)->get();
        $presentadores = Presentadores::where('evento_id', $data)->get();
        $pruebaSonido = PruebaSonido::where('evento_id', $data)->first();
        $sedes = DetailEvents::where('event_id', $data)->first();
        if (!empty($pruebaSonido)) {
            $pruebaSonido->delete();

        }

        if (!empty($sedes)) {
            $sedes->delete();
        }

        if (!empty($moderadores)) {
            foreach ($moderadores as $moderador) {
                $moderador->delete();
            }
        }

        if (!empty($presentadores)) {
            foreach ($presentadores as $presentador) {
                $presentador->delete();
            }
        }

        $detalles = Evento_participantes::where('evento_id', $data['id'])->get();
        foreach ($detalles as $detalle) {
            $detalle->delete();
        }

        $evento = $this->find($data['id']);


        if ($evento->delete()) {
            return ['success' => 'success', 'msg' => 'El evento ah sido eliminado'];
        }
        return ['success' => 'error', 'msg' => 'Ah ocurrido un error, consulte a su administrador'];
    }

    /**
     * Crea nuevo registro de los moderadores de un evento
     */
    public function createModera($id, $moderadores)
    {
        foreach ($moderadores as $item) {
            Moderadores::create([
                'moderador_id' => $item,
                'evento_id' => $id
            ]);
        }

    }

    /**
     * Crea nuevo registro de los presentadores de un evento
     */
    public function createPresentadores($id, $presentadores)
    {
        foreach ($presentadores as $item) {
            Presentadores::create([
                'presentador_id' => $item,
                'evento_id' => $id
            ]);

        }
    }

    /**
     * Elimina items relacionados al evento
     */
    public function deleteItemsEvent($detalles)
    {
        foreach ($detalles as $detalle) {
            $detalle->delete();
        }
    }

    //Crear una prueba de sonido para un evento
    public function soundTest($data)
    {
        $id = $data['evento_id'];
        //Compara la hora final con la hora inicial de un evento, la hora final no puede ser menor a la hora inicial
        if ($this->compararService->convertir($data['hora_fin']) <= $this->compararService->convertir($data['hora_inicio'])) {
            return ['success' => 'error', 'msg' => 'La hora final no puede ser menor o igual a la hora de inicio'];
        }
        $data['invitado_id'] = [];
        $evento_id = "";
        $participantes = Evento_participantes::where('evento_id', $data['evento_id'])->get();
        $moderadores = Moderadores::where('evento_id', $data['evento_id'])->get();
        $presentadores = Presentadores::where('evento_id', $data['evento_id'])->get();


        if (count($participantes) > 0) {
            foreach ($participantes as $participante) {
                array_push($data['invitado_id'], $participante->invitado_id);
            }

        }
        if (count($moderadores) > 0) {
            foreach ($moderadores as $moderador) {
                array_push($data['invitado_id'], $moderador->moderador_id);
            }
        }

        if (count($presentadores) > 0) {
            foreach ($presentadores as $presentador) {
                array_push($data['invitado_id'], $presentador->presentador_id);
            }
        }

        //Compara foros
        $foros = $this->comparaForos->compararEvento($data['foro_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($foros != "false") {
            return $foros;
        }

        //Compara pruebas de sonido
        $pruebaSonidoId = PruebaSonido::where('evento_id', $data['evento_id'])->first();
        if (empty($pruebaSonidoId)) {
            $data['evento_id'] = 0;
        }

        $pruebaSonido = $this->comparaPruebaSonido->compararEvento($data['foro_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], $data['evento_id']);

        if ($pruebaSonido != "false") {
            return $pruebaSonido;
        }

        /**
         * Compara que los invitados no tengan pruebas de sonido
         */
        $pruebaSonido = $this->comparaPruebaSonido->compararEventoInvitado($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], $data['evento_id']);
        if ($pruebaSonido != "false") {
            return $pruebaSonido;
        }

        //Compara eventos filo
        $eventos = $this->comparaEventos->compararEvento($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($eventos != "false") {
            return $eventos;
        }

        //Compara eventos de filoando
        $eventosFiloando = $this->comparaFiloando->comparaFiloando($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], "0");
        if ($eventosFiloando != "false") {
            return $eventosFiloando;
        }
        //Compara eventos de prensa
        $eventosPrensa = $this->compararPrensa->compararPrensa($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($eventosPrensa != "false") {
            return $eventosPrensa;
        }

        $eventoSocial = $this->compararSocial->compararSocial($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($eventoSocial != "false") {
            return $eventoSocial;
        }

        if (!empty($pruebaSonidoId)) {
            return $this->updateSoundTest($data['fecha'], $data['hora_inicio'], $data['hora_fin'], $data['foro_id'], $pruebaSonidoId->evento_id);
        }

        PruebaSonido::create([
            'fecha' => $data['fecha'],
            'hora_inicio' => $data['hora_inicio'],
            'hora_fin' => $data['hora_fin'],
            'foro_id' => $data['foro_id'],
            'evento_id' => $id,
        ]);

        return ['success' => 'success', 'msg' => 'Se agrego la prueba de sonido'];
    }

    public function updateSoundTest($fecha, $hora_inicio, $hora_fin, $foro_id, $evento_id)
    {
        $pruebaSonido = PruebaSonido::where('evento_id', $evento_id)->first();
        $pruebaSonido->fecha = $fecha;
        $pruebaSonido->hora_inicio = $hora_inicio;
        $pruebaSonido->hora_fin = $hora_fin;
        $pruebaSonido->foro_id = $foro_id;
        if ($pruebaSonido->save()) {
            return ['success' => 'success', 'msg' => 'Prueba de sonido actualizada'];
        }
    }
}

/**
 * @param $data
 * @return array|bool
 * Este es el original
 *
 * public function save($data){
 *
 * $inicio = $this->compararService->convertir($data['hora_inicio']);
 * $fin = $this->compararService->convertir($data['hora_fin']);
 *
 *
 * $compararHoras = $this->compararService->compararHoras($data['hora_inicio'],$data['hora_fin']);
 * if ($compararHoras != "true"){
 * return $compararHoras;
 * }
 *
 * //inicia comparaciones
 * foreach ($data['invitado_id'] as $item) {
 *
 * $evento = Evento_participantes::with('eventos', 'invitadosNombre')->where('invitado_id', $item)->get();
 *
 * if (!empty($evento)) {
 * foreach ($evento as $value) {
 * $compararEvento = $this->compararService->comparar($value['eventos'], $inicio, $fin, $data['fecha'], "la filo");
 * if ($compararEvento != "true") {
 * return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " la filo de " . $value['invitadosNombre'][0]->name);
 * }
 * }
 * }
 *
 * //comparar Filoando
 *
 * $filoando = FiloandoInvitados::with('eventosFiloando', 'invitadosNombre')
 * ->where('invitado_id', $item)
 * ->get();
 *
 * if (!empty($filoando)) {
 * foreach ($filoando as $value) {
 * $compararEvento = $this->compararService->compararFiloando($value->eventosFiloando, $inicio, $fin, $data['fecha'], "la filo");
 * if ($compararEvento != "true") {
 * return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_traslado_ida, $compararEvento->hora_traslado_regreso, " tipo FILOANDO de " . $value['invitadosNombre'][0]->name);
 * }
 * }
 * }
 *
 * $prensa = Prensa::with('prensa')
 * ->where('invitado_id', $item)
 * ->get();
 *
 * if (!empty($prensa)) {
 * $compararEvento = $this->compararService->comparar($prensa, $inicio, $fin, $data['fecha'], "prensa");
 * if ($compararEvento != "true") {
 * return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " prensa  de  " . $compararEvento->prensa[0]->name);
 * }
 * }
 *
 * $social = Social::with('socialInvitado')
 * ->where('invitado_id', $item)
 * ->get();
 * if (!empty($social))
 * {
 * $compararEvento = $this->compararService->comparar($social, $inicio, $fin, $data['fecha'], "social");
 * if ($compararEvento != "true") {
 * return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " tipo social de " . $compararEvento->socialInvitado[0]->name);
 * }
 * }
 *
 * /**
 * Comparar presentadores
 *
 *
 * if (!empty($data['presentador_id'])) {
 *
 * foreach ($data['presentador_id'] as $item) {
 * $evento = Presentadores::with('presentadores_eventos', 'presentadores')
 * ->where('presentador_id', $item)
 * ->get();
 *
 * if (!empty($evento)) {
 * foreach ($evento as $value) {
 * $compararEvento = $this->compararService->comparar($value['presentadores_eventos'], $inicio, $fin, $data['fecha']);
 * if ($compararEvento != "true") {
 * return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " presentador  " . $value['presentadores'][0]->name);
 * }
 * }
 * }
 * }
 * }
 *
 * /**
 * Compara moderadores
 *
 *
 * if (!empty($data['moderador_id'])) {
 * foreach ($data['moderador_id'] as $item) {
 * $evento = Moderadores::with('moderadores_eventos', 'moderadoresNombre')
 * ->where('moderador_id', $item)
 * ->get();
 *
 * if (!empty($evento)) {
 * foreach ($evento as $value) {
 * $compararEvento = $this->compararService->comparar($value['moderadores_eventos'], $inicio, $fin, $data['fecha']);
 * if ($compararEvento != "true") {
 * return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " moderador  " . $value['moderadoresNombre'][0]->name);
 * }
 * }
 * }
 * }
 * }
 * }
 *
 * $evento = Eventos::create([
 * 'fecha'=>$data['fecha'],
 * 'hora_inicio'=>$data['hora_inicio'],
 * 'hora_fin'=>$data['hora_fin'],
 * 'nombre_evento'=>$data['nombre_evento'],
 * 'descripcion'=>$data['descripcion'],
 * 'tipo_id'=>$data['tipo_id'],
 * 'ciclo'=>$data['ciclo'],
 * 'programacion_id'=>$data['programacion_id'],
 * 'foro_id'=>$data['foro_id'],
 * 'requirimientos'=>$data['requirimientos'],
 * 'status'=>0,
 * 'colaborador'=>$data['colaborador']
 *
 * ]);
 * if (!empty($evento)){
 *
 * $participantesEvento = $this->createInvitedEvent($evento->id,$data['invitado_id']);
 * if (!empty($data['moderador_id']))
 * {
 * $moderadoresEvento = $this->createModera($evento->id,$data['moderador_id']);
 * }
 *
 * if (!empty($data['presentador_id'])) {
 * $presentadoreEvento = $this->createPresentadores($evento->id, $data['presentador_id']);
 * }
 *
 * if (!empty($participantesEvento)){
 * return ['success'=>'success','msg'=>'Registro guardado con exito'];
 * }
 * }
 * return ['success'=>'error','msg'=>'Ah ocurrido un error , consulte a su administrador'];
 * }
 */


/*
    public function update2($data)
    {

        $inicio = $this->compararService->convertir($data['hora_inicio']);
        $fin = $this->compararService->convertir($data['hora_fin']);
        $detalles = Evento_participantes::where('evento_id',$data['id'])->get();
        $moderadores = Moderadores::where('evento_id',$data['id'])->get();
        $presentadores = Presentadores::where('evento_id',$data['id'])->get();

        /** Limpiar la tabla moderadores en caso que venga vacia
        if (empty($data['moderador_id']))
        {
            $moderadores = Moderadores::where('evento_id', $data['id'])->get();
            if (count($moderadores ) > 0)
            {
                foreach ($moderadores as $item)
                {
                    $item->delete();
                }
            }
        }

        if (empty($data['presentador_id']))
        {
            $presentadores = Presentadores::where('evento_id',$data['id'])->get();
            if (count($presentadores ) > 0)
            {
                foreach ($presentadores as $item)
                {
                    $item->delete();
                }
            }
        }



        $compararHoras = $this->compararService->compararHoras($data['hora_inicio'], $data['hora_fin']);
        if ($compararHoras != "true") {
            return $compararHoras;
        }

        //Envia detalles a eliminar
        //$this->deleteItemsEvent($detalles);
        /**
         *

        $evento = Evento_participantes::with('eventos', 'invitadosNombre')
            ->where('evento_id','<>',$data['id'])
            ->where('invitado_id', $data['invitado_id'])
            ->get();

        foreach ($evento as $item) {

            $compararEvento = $this->compararService->comparar($item->eventos, $inicio, $fin, $data['fecha'], "la filo");
            if ($compararEvento != "true") {
                return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " la filo de " . $item->invitadosNombre[0]->name);
            }
        }

        /**
         * COmprobar filoando


        foreach ($data['invitado_id'] as $item)
        {
            $filoando = FiloandoInvitados::with('eventosFiloando', 'invitadosNombre')
                ->where('invitado_id', $item)
                ->get();

            if (!empty($filoando)) {
                foreach ($filoando as $value) {
                    $compararEvento = $this->compararService->compararFiloando($value->eventosFiloando, $inicio, $fin, $data['fecha'], "la filo");
                    if ($compararEvento != "true") {
                        return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_traslado_ida, $compararEvento->hora_traslado_regreso, " tipo FILOANDO de " . $value['invitadosNombre'][0]->name);
                    }
                }
            }

        }


        /**
         * Comprobar prensa


        foreach ($data['invitado_id'] as $item) {
            $eventosPrensa = Prensa::with('prensa')
                ->where('invitado_id', $item)
                ->get();

            $compararPrensa = $this->compararService->comparar($eventosPrensa, $inicio, $fin, $data['fecha'], "prensa");
            if ($compararPrensa != "true") {
                return $this->compararService->message($compararPrensa->fecha, $compararPrensa->hora_inicio, $compararPrensa->hora_fin, "prensa de " . $compararPrensa->prensa[0]->name);
            }


            /**
             * Comprobar social


            $social = Social::with('socialInvitado')
                ->where('invitado_id', $item)
                ->get();
            if (!empty($social)) {
                $compararEvento = $this->compararService->comparar($social, $inicio, $fin, $data['fecha'], "social");
                if ($compararEvento != "true") {
                    return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " tipo social de " . $compararEvento->socialInvitado[0]->name);
                }
            }
        }




        $evento = $this->find($data['id']);
        $invitados = Evento_participantes::where('evento_id',$evento->id)->get();


        $evento->fecha = $data['fecha'];
        $evento->hora_inicio = $data['hora_inicio'];
        $evento->hora_fin = $data['hora_fin'];
        $evento->nombre_evento = $data['nombre_evento'];
        $evento->descripcion = $data['descripcion'];
        $evento->tipo_id= $data['tipo_id'];
        $evento->ciclo = $data['ciclo'];
        $evento->programacion_id = $data['programacion_id'];
        $evento->foro_id = $data['foro_id'];
        $evento->requirimientos = $data['requirimientos'];
        $evento->colaborador = $data['colaborador'];
        $evento->status = 0;


        if ($evento->save())
        {
            foreach ($invitados as $invitado){
                $invitado->delete();
            }
            $participantesEvento = $this->createInvitedEvent($evento->id,$data['invitado_id']);

            if (!empty($data['presentador_id'])) {
                /**
                 * Aqui voy a hacer la comprobacion de los presentadores

                foreach($data['presentador_id'] as $item)
                {
                    $evento = Evento_participantes::with('eventos', 'invitadosNombre')->where('invitado_id', $item)->get();

                    if (!empty($evento)) {
                        foreach ($evento as $value) {
                            $compararEvento = $this->compararService->comparar($value['eventos'], $inicio, $fin, $data['fecha'], "la filo");
                            if ($compararEvento != "true") {
                                return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " la filo de " . $value['invitadosNombre'][0]->name);
                            }
                        }
                    }

                    $filoando = FiloandoInvitados::with('eventosFiloando', 'invitadosNombre')
                        ->where('invitado_id', $item)
                        ->get();

                    if (!empty($filoando)) {
                        foreach ($filoando as $value) {
                            $compararEvento = $this->compararService->compararFiloando($value->eventosFiloando, $inicio, $fin, $data['fecha'], "la filo");
                            if ($compararEvento != "true") {
                                return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_traslado_ida, $compararEvento->hora_traslado_regreso, " tipo FILOANDO de " . $value['invitadosNombre'][0]->name);
                            }
                        }
                    }

                    $eventosPrensa = Prensa::with('prensa')
                            ->where('invitado_id', $item)
                            ->get();
                    $compararPrensa = $this->compararService->comparar($eventosPrensa, $inicio, $fin, $data['fecha'], "prensa");
                    if ($compararPrensa != "true") {
                        return $this->compararService->message($compararPrensa->fecha, $compararPrensa->hora_inicio, $compararPrensa->hora_fin, "prensa de " . $compararPrensa->prensa[0]->name);
                    }

                    /**
                     * Comprobar social

                    $social = Social::with('socialInvitado')
                        ->where('invitado_id', $item)
                        ->get();
                    if (!empty($social)) {
                        $compararEvento = $this->compararService->comparar($social, $inicio, $fin, $data['fecha'], "social");
                        if ($compararEvento != "true") {
                            return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " tipo social de " . $compararEvento->socialInvitado[0]->name);
                        }
                    }
                }

                $presentadores = Presentadores::where('evento_id',$data['id'])->get();
                foreach ($presentadores as $presentador)
                {
                    $presentador->delete();
                }
                $presentadores = $this->createPresentadores($data['id'],$data['presentador_id']);

            }


            /**
             *Comparacion moderadores


            if (!empty($data['moderador_id']))
            {
                //$evento = Evento_participantes::with('eventos', 'invitadosNombre')->where('invitado_id', $item)->get();
                $evento = Evento_participantes::with('eventos', 'invitadosNombre')
                ->where('evento_id','<>',$data['id'])
                ->where('invitado_id', $data['invitado_id'])
                ->get();


                if (!empty($evento)) {
                    foreach ($evento as $value) {
                        $compararEvento = $this->compararService->comparar($value['eventos'], $inicio, $fin, $data['fecha'], "la filo");
                        if ($compararEvento != "true") {
                            return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " la filo de " . $value['invitadosNombre'][0]->name);
                        }
                    }
                }

                $filoando = FiloandoInvitados::with('eventosFiloando', 'invitadosNombre')
                    ->where('invitado_id', $item)
                    ->get();

                if (!empty($filoando)) {
                    foreach ($filoando as $value) {
                        $compararEvento = $this->compararService->compararFiloando($value->eventosFiloando, $inicio, $fin, $data['fecha'], "la filo");
                        if ($compararEvento != "true") {
                            return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_traslado_ida, $compararEvento->hora_traslado_regreso, " tipo FILOANDO de " . $value['invitadosNombre'][0]->name);
                        }
                    }
                }

                $eventosPrensa = Prensa::with('prensa')
                        ->where('invitado_id', $item)
                        ->get();
                $compararPrensa = $this->compararService->comparar($eventosPrensa, $inicio, $fin, $data['fecha'], "prensa");
                if ($compararPrensa != "true") {
                    return $this->compararService->message($compararPrensa->fecha, $compararPrensa->hora_inicio, $compararPrensa->hora_fin, "prensa de " . $compararPrensa->prensa[0]->name);
                }

                /**
                 * Comprobar social


                $social = Social::with('socialInvitado')
                    ->where('invitado_id', $item)
                    ->get();
                if (!empty($social)) {
                    $compararEvento = $this->compararService->comparar($social, $inicio, $fin, $data['fecha'], "social");
                    if ($compararEvento != "true") {
                        return $this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " tipo social de " . $compararEvento->socialInvitado[0]->name);
                    }
                }
                $moderadores = Moderadores::where('evento_id',$data['id'])->get();
                foreach ($moderadores as $moderador)
                {
                    $moderador->delete();
                }
                $moderador = $this->createModera($data['id'],$data['moderador_id']);
            }
            }


        if (!empty($participantesEvento)){
            return ['success'=>'success','msg'=>'Registro actualizado con exito'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error , consulte a su administrador'];
    }
    */

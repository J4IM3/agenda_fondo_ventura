<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\Anfitrion;
use App\Http\Entities\Foro;
use App\Http\Entities\Instituciones;
use App\Http\Entities\Invitado;
use App\Http\Entities\EventoTipo;
use App\Http\Services\ChangeStatus;
use Illuminate\Http\Request;

class InstitucionesRepo extends BaseRepo
{

    /**
     * @var ChangeStatus
     */
    private $changeStatus;

    public function getModel()
    {
        return new Instituciones();
    }
    public function __construct(ChangeStatus $changeStatus)
    {
        $this->changeStatus = $changeStatus;
    }

    public function getInstituciones()
    {
        return Instituciones::where('status',1)
            ->get();
    }

    public function create($data)
    {
        $institucion = Instituciones::create([
            'nombre'=>$data['nombre'],
            'status'=>1
        ]);
        if(!empty($institucion))
        {
            return ['success'=>'success','msg'=>'Registro creado correctamente'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte a su administrador'];
    }

    public function update($data)
    {
        $institucion = $this->find($data['id']);
        $institucion->nombre = $data['nombre'];
        if ($institucion->save())
        {
            return ['success'=>'success','msg'=>'Registro actualizado correctamente'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte a su administrador'];
    }

    public function changeStatus($id)
    {
        $institucion = Instituciones::where('id',$id)->first();
        $status = $this->changeStatus->changeStatus($institucion->status);
        $institucion->status = $status;
        if ($institucion->save())
        {
            return ['success'=>'success','msg'=>'Estatus actualizado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte a su administrador'];

    }
}

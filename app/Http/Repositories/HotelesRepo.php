<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\Hotels;
use App\Http\Entities\Invitado;
use App\Http\Services\ChangeStatus;
use Illuminate\Http\Request;

class HotelesRepo extends BaseRepo {

    public function getModel()
    {
        return new Hotels();
    }

    public function getHotel(){
        return Hotels::where('status',1)
                     ->where('tipo','hotel')
                     ->get();
    }

    public function getRestaurant()
    {
        return Hotels::where('status',1)
            ->where('tipo','restaurant')
            ->get();
    }

    public function save($data)
    {
        $aliados = Hotels::create([
            'name'=>$data['name'],
            'direccion'=>$data['direccion'],
            'telefono'=>$data['telefono'],
            'contacto'=>$data['contacto'],
            'status'=>1,
            'tipo'=>$data['tipo']
        ]);
        if (!empty($aliados)){
            return ["success"=>'success','msg'=>'Registro agregado'];
        }
        return ["success"=>'error','msg'=>'Ah ocurrido un error'];
    }

    public function edit($data)
    {
        $aliado = $this->find($data['id']);
        $aliado->name  = $data['name'];
        $aliado->direccion = $data['direccion'];
        $aliado->telefono = $data['telefono'];
        $aliado->contacto = $data['contacto'];
        $aliado->tipo = $data['tipo'];
        if(!$aliado->save())
        {
            return ['success'=>'error','msg'=>'Ah ocurrido un error'];
        }
        return ['success'=>'success','msg'=>'Registro actualizado'];

    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Services\ChangeStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UsersRepo extends BaseRepo {

    /**
     * @var ChangeStatus
     */
    private $changeStatus;

    public function getModel()
    {
        return new User();
    }

    public function __construct(ChangeStatus $changeStatus)
    {
        $this->changeStatus = $changeStatus;
    }

    public function save($data){
        $user  = User::create([
            'name'=>$data['name'],
            'email'=>$data['email'],
            'status'=>1,
            'password' => bcrypt($data['password'])
        ]);
        if (!$user){
            return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte a su administrador'];
        }
        return ['success'=>'success','msg'=>'Usuario agregado'];
    }

    public function edit($data){
        $user = $this->find($data['id']);
        $user->name=$data['name'];
        $user->email=$data['email'];
        if (!$user->save()){
            return ['success'=>'error','msg'=>'Ah ocurrido un error'];
        }
        return ['success'=>'success','msg'=>'Usuario actualizado'];
    }

    public function changeStatus($data){
        $user = $this->find($data['id']);
        $status = $this->changeStatus->changeStatus($user->status);
        $user->status = $status;
        if(!$user->save()){
            return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte a su administrador'];
        }
        return ['success'=>'success','msg'=>'Estatus actualizado con exito'];
    }
}

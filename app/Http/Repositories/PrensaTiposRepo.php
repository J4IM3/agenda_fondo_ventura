<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\Anfitrion;
use App\Http\Entities\Foro;
use App\Http\Entities\Invitado;
use App\Http\Entities\EventoTipo;
use App\Http\Entities\PrensaTipos;
use App\Http\Services\ChangeStatus;
use Illuminate\Http\Request;

class PrensaTiposRepo extends BaseRepo
{

    /**
     * @var ChangeStatus
     */
    private $changeStatus;

    public function getModel()
    {
        return new PrensaTipos();
    }

    public function getTiposActive()
    {
        return PrensaTipos::where('status',1)->get();
    }

    public function __construct(ChangeStatus $changeStatus)
    {
        $this->changeStatus = $changeStatus;
    }

    public function save($data)
    {
        $tipos = PrensaTipos::create([
           'nombre'=>$data['nombre'],
           'status'=>1
        ]);
        if(!empty($tipos))
        {
            return ['success'=>'success','msg'=>'Registro creado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error'];
    }

    public function editar($data)
    {
        $tipo = $this->find($data['id']);
        $tipo->nombre = $data['nombre'];
        if ($tipo->save())
        {
            return ['success'=>'success','msg'=>'Registro actualizado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error'];
    }

    public function changeStatus($data)
    {
        $tipo = $this->find($data['id']);
        $status = $this->changeStatus->changeStatus($tipo->status);

        $tipo->status = $status;
        if ($tipo->save())
        {
            return ['success'=>'success','msg'=>'Status actualizado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error'];
    }

}

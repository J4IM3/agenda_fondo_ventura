<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\Evento_moderadores;
use App\Http\Entities\Eventos;
use App\Http\Entities\Evento_participantes;

use App\Http\Entities\Filoando;
use App\Http\Entities\FiloandoInvitados;
use App\Http\Entities\Invitado;
use App\Http\Entities\Moderadores;
use App\Http\Entities\Prensa;
use App\Http\Entities\Presentadores;
use App\Http\Entities\Social;
use App\Http\Services\ChangeStatus;
use App\Http\Services\ComparaEventos;
use App\Http\Services\ComparaFiloando;
use App\Http\Services\CompararPrensa;
use App\Http\Services\CompararService;
use App\Http\Services\CompararSocial;
use Illuminate\Http\Request;

class FiloandoRepo extends BaseRepo {

    /**
     * @var CompararService
     */
    private $compararService;
    /**
     * @var ComparaEventos
     */
    private $comparaEventos;
    /**
     * @var ComparaFiloando
     */
    private $comparaFiloando;
    /**
     * @var CompararPrensa
     */
    private $compararPrensa;
    /**
     * @var CompararSocial
     */
    private $compararSocial;

    public function getModel()
    {
        return new Filoando();
    }

    public function __construct(CompararService $compararService,
                                ComparaEventos $comparaEventos,
                                ComparaFiloando $comparaFiloando,
                                CompararPrensa $compararPrensa,
                                CompararSocial $compararSocial)
    {
        $this->compararService = $compararService;
        $this->comparaEventos = $comparaEventos;
        $this->comparaFiloando = $comparaFiloando;
        $this->compararPrensa = $compararPrensa;
        $this->compararSocial = $compararSocial;
    }

    public function create($data)
    {
        $comparaHorasIniciales = $this->compararService->comparaMultiplesHoras(
            $this->compararService->convertir($data['hora_traslado_ida']),
            $this->compararService->convertir($data['hora_inicio_evento']),
            $this->compararService->convertir($data['hora_final_evento']),
            $this->compararService->convertir($data['hora_traslado_regreso']));
        if ($comparaHorasIniciales != "true")
        {
            return $comparaHorasIniciales;
        }
        //Compara eventos filo
        $eventos = $this->comparaEventos->compararEvento($data['invitado_id'],$data['fecha'],$data['hora_traslado_ida'],$data['hora_traslado_regreso'],'0');
        if ($eventos != "false")
        {
            return $eventos;
        }

        //Compara eventos de filoando
        $eventosFiloando = $this->comparaFiloando->comparaFiloando($data['invitado_id'],$data['fecha'],$data['hora_traslado_ida'],$data['hora_traslado_regreso'],'0');
        if ($eventosFiloando != "false")
        {
            return $eventosFiloando;
        }
        //Compara eventos de prensa
        $eventosPrensa = $this->compararPrensa->compararPrensa($data['invitado_id'],$data['fecha'],$data['hora_traslado_ida'],$data['hora_traslado_regreso'],'0');
        if ($eventosPrensa != "false")
        {
            return $eventosPrensa;
        }

        $eventoSocial = $this->compararSocial->compararSocial($data['invitado_id'],$data['fecha'],$data['hora_traslado_ida'],$data['hora_traslado_regreso'],'0');
        if ($eventoSocial != "false")
        {
            return $eventoSocial;
        }

        $filoando = Filoando::create([
            'institucion_id'=>$data['institucion_id'],
            'direccion'=>$data['direccion'],
            'nivel'=>$data['nivel'],
            'fecha'=>$data['fecha'],
            'contacto'=>$data['contacto'],
            'telefono_contacto'=>$data['telefono_contacto'],
            'tipo_transporte'=>$data['tipo_transporte'],
            'requerimientos'=>$data['requerimientos'],
            'ubicacion_inicial'=>$data['ubicacion_inicial'],
            'ubicacion_final'=>$data['ubicacion_final'],
            'hora_inicio_evento'=>$data['hora_inicio_evento'],
            'hora_final_evento'=>$data['hora_final_evento'],
            'hora_traslado_regreso'=>$data['hora_traslado_regreso'],
            'hora_traslado_ida'=>$data['hora_traslado_ida'],
        ]);

        if (!empty($filoando)) {

            $participantesEvento = $this->createInvitedEvent($filoando->id, $data['invitado_id']);
        }
        return ['success'=>'success','msg'=>'Registro ingresado'];
    }

    public function createInvitedEvent($id,$invitados){
        foreach ($invitados as $invitado) {
            $participantesEvento[] = FiloandoInvitados::create([
                'invitado_id'=>$invitado,
                'evento_id'=>$id
            ]);
        }
        return $participantesEvento;
    }

    public function update($data)
    {

        $comparaHorasIniciales = $this->compararService->comparaMultiplesHoras(
            $this->compararService->convertir($data['hora_traslado_ida']),
            $this->compararService->convertir($data['hora_inicio_evento']),
            $this->compararService->convertir($data['hora_final_evento']),
            $this->compararService->convertir($data['hora_traslado_regreso']));
        if ($comparaHorasIniciales != "true")
        {
            return $comparaHorasIniciales;
        }

        //Compara eventos filo
        $eventos = $this->comparaEventos->compararEvento($data['invitado_id'],$data['fecha'],$data['hora_traslado_ida'],$data['hora_traslado_regreso'],'0');
        if ($eventos != "false")
        {
            return $eventos;
        }

        //Compara eventos de filoando
        $eventosFiloando = $this->comparaFiloando->comparaFiloando($data['invitado_id'],$data['fecha'],$data['hora_traslado_ida'],$data['hora_traslado_regreso'],$data['id']);
        if ($eventosFiloando != "false")
        {
            return $eventosFiloando;
        }
        //Compara eventos de prensa
        $eventosPrensa = $this->compararPrensa->compararPrensa($data['invitado_id'],$data['fecha'],$data['hora_traslado_ida'],$data['hora_traslado_regreso'],'0');
        if ($eventosPrensa != "false")
        {
            return $eventosPrensa;
        }

        $eventoSocial = $this->compararSocial->compararSocial($data['invitado_id'],$data['fecha'],$data['hora_traslado_ida'],$data['hora_traslado_regreso'],'0');
        if ($eventoSocial != "false")
        {
            return $eventoSocial;
        }

        $filoando = $this->find($data['id']);
        $filoando->institucion_id =$data['institucion_id'];
        $filoando->direccion =$data['direccion'];
        $filoando->nivel = $data['nivel'];
        $filoando->fecha = $data['fecha'];
        $filoando->contacto = $data['contacto'];
        $filoando->telefono_contacto = $data['telefono_contacto'];
        $filoando->tipo_transporte = $data['tipo_transporte'];
        $filoando->requerimientos = $data['requerimientos'];
        $filoando->ubicacion_inicial = $data['ubicacion_inicial'];
        $filoando->ubicacion_final = $data['ubicacion_final'];
        $filoando->hora_inicio_evento = $data['hora_inicio_evento'];
        $filoando->hora_final_evento = $data['hora_final_evento'];
        $filoando->hora_traslado_regreso = $data['hora_traslado_regreso'];
        $filoando->hora_traslado_ida = $data['hora_traslado_ida'];

        if ($filoando->save())
        {
            if (!empty($filoando)) {
                $this->deleteRelaciones($data['id']);
                $participantesEvento = $this->createInvitedEvent($filoando->id, $data['invitado_id']);
            }
            return ['success'=>'success','msg'=>'Registro actualizado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error'];


    }

    private function deleteRelaciones($id)
    {
        $invitadosFiloando = FiloandoInvitados::where('evento_id',$id)->get();

        foreach ($invitadosFiloando as $item)
        {
            $item->delete();
        }
    }


    public function delete($id)
    {
        $evento = Filoando::where('id',$id)->first();
        $this->deleteGuestFiloando($evento->id);
        if ($evento->delete())
        {
            return ['success' => 'success','msg' => 'Evento eliminado con exito'];
        }
        return ['success' => 'error','msg' => 'Ah ocurrido un error al eliminar el evento'];


    }

    public function deleteGuestFiloando($eventoId)
    {
        $guests = FiloandoInvitados::where('evento_id',$eventoId)->get();

        foreach ($guests as $guest )
        {
            $guest->delete();
        }
    }
    /*
    public function update(){

        $inicio = $this->compararService->convertir($data['hora_traslado_ida']);
        $fin = $this->compararService->convertir($data['hora_traslado_regreso']);


        $compararHoras = $this->compararService->compararHoras($data['hora_traslado_ida'],$data['hora_traslado_regreso']);
        if ($compararHoras != "true"){
            return $compararHoras;
        }

        //inicia comparaciones
        foreach ($data['invitado_id'] as $item){
            $evento = Evento_participantes::with('eventos','invitadosNombre')->where('invitado_id',$item)->get();

            if (!empty($evento)){
                foreach ($evento as $value)
                {
                    $compararEvento = $this->compararService->comparar($value['eventos'],$inicio,$fin,$data['fecha'],"la filo");
                    if ($compararEvento != "true" ){
                        return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," la filo de ".$value['invitadosNombre'][0]->name);
                    }
                }
            }

            //comparar Filoando

            $filoando = FiloandoInvitados::with('eventosFiloando','invitadosNombre')
                ->where('invitado_id',$item)
                ->where('evento_id','!=',$data['id'])
                ->get();

            if (!empty($filoando)){
                foreach ($filoando as $value)
                {
                    $compararEvento = $this->compararService->compararFiloando($value->eventosFiloando,$inicio,$fin,$data['fecha'],"tipo FILOANDO de ");
                    if ($compararEvento != "true" ){
                        return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_traslado_ida,$compararEvento->hora_traslado_regreso,"tipo FILOANDO de ".$value['invitadosNombre'][0]->name);
                    }
                }
            }

            $prensa = Prensa::with('prensa')
                ->where('invitado_id',$item)
                ->get();

            if (!empty($prensa))
            {
                $compararEvento = $this->compararService->comparar($prensa,$inicio,$fin,$data['fecha'],"prensa");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," la filo de ".$compararEvento->prensa[0]->name);
                }
            }

            $social = Social::with('socialInvitado')
                ->where('invitado_id',$item)
                ->get();
            if (!empty($social))
            {
                $compararEvento = $this->compararService->comparar($social,$inicio,$fin,$data['fecha'],"social");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," tipo social de ".$compararEvento->socialInvitado[0]->name);
                }
            }


        }


        $filoando = $this->find($data['id']);
        $filoando->institucion_id =$data['institucion_id'];
        $filoando->direccion =$data['direccion'];
        $filoando->nivel = $data['nivel'];
        $filoando->fecha = $data['fecha'];
        $filoando->contacto = $data['contacto'];
        $filoando->telefono_contacto = $data['telefono_contacto'];
        $filoando->tipo_transporte = $data['tipo_transporte'];
        $filoando->requerimientos = $data['requerimientos'];
        $filoando->ubicacion_inicial = $data['ubicacion_inicial'];
        $filoando->ubicacion_final = $data['ubicacion_final'];
        $filoando->hora_inicio_evento = $data['hora_inicio_evento'];
        $filoando->hora_final_evento = $data['hora_final_evento'];
        $filoando->hora_traslado_regreso = $data['hora_traslado_regreso'];
        $filoando->hora_traslado_ida = $data['hora_traslado_ida'];

        if ($filoando->save())
        {
            if (!empty($filoando)) {
                $this->deleteRelaciones($data['id']);
                $participantesEvento = $this->createInvitedEvent($filoando->id, $data['invitado_id']);
            }
            return ['success'=>'success','msg'=>'Registro actualizado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error'];

    }

    /*

    public function nuevo($data)
    {
        $inicio = $this->compararService->convertir($data['hora_traslado_ida']);
        $fin = $this->compararService->convertir($data['hora_traslado_regreso']);


        $compararHoras = $this->compararService->compararHoras($data['hora_traslado_ida'],$data['hora_traslado_regreso']);
        if ($compararHoras != "true"){
            return $compararHoras;
        }
        //inicia comparaciones
        foreach ($data['invitado_id'] as $item){
            $evento = Evento_participantes::with('eventos','invitadosNombre')->where('invitado_id',$item)->get();

            if (!empty($evento)){
                foreach ($evento as $value)
                {
                    $compararEvento = $this->compararService->comparar($value['eventos'],$inicio,$fin,$data['fecha'],"la filo");
                    if ($compararEvento != "true" ){
                        return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," la filo de ".$value['invitadosNombre'][0]->name);
                    }
                }
            }

            //comparar Filoando

            $filoando = FiloandoInvitados::with('eventosFiloando','invitadosNombre')
                ->where('invitado_id',$item)
                ->get();

            if (!empty($filoando)){
                foreach ($filoando as $value)
                {
                    $compararEvento = $this->compararService->compararFiloando($value->eventosFiloando,$inicio,$fin,$data['fecha'],"la filo");
                    if ($compararEvento != "true" ){
                        return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_traslado_ida,$compararEvento->hora_traslado_regreso," tipo FILOANDO de ".$value['invitadosNombre'][0]->name);
                    }
                }
            }

            $prensa = Prensa::with('prensa')
                ->where('invitado_id',$item)
                ->get();

            if (!empty($prensa))
            {
                $compararEvento = $this->compararService->comparar($prensa,$inicio,$fin,$data['fecha'],"prensa");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," prensa  de  ".$compararEvento->prensa[0]->name);
                }
            }

            $social = Social::with('socialInvitado')
                ->where('invitado_id',$item)
                ->get();
            if (!empty($social))
            {
                $compararEvento = $this->compararService->comparar($social,$inicio,$fin,$data['fecha'],"social");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," tipo social de ".$compararEvento->socialInvitado[0]->name);
                }
            }

        }
        $filoando = Filoando::create([
            'institucion_id'=>$data['institucion_id'],
            'direccion'=>$data['direccion'],
            'nivel'=>$data['nivel'],
            'fecha'=>$data['fecha'],
            'contacto'=>$data['contacto'],
            'telefono_contacto'=>$data['telefono_contacto'],
            'tipo_transporte'=>$data['tipo_transporte'],
            'requerimientos'=>$data['requerimientos'],
            'ubicacion_inicial'=>$data['ubicacion_inicial'],
            'ubicacion_final'=>$data['ubicacion_final'],
            'hora_inicio_evento'=>$data['hora_inicio_evento'],
            'hora_final_evento'=>$data['hora_final_evento'],
            'hora_traslado_regreso'=>$data['hora_traslado_regreso'],
            'hora_traslado_ida'=>$data['hora_traslado_ida'],
        ]);

        if (!empty($filoando)) {

            $participantesEvento = $this->createInvitedEvent($filoando->id, $data['invitado_id']);
        }
        return ['success'=>'success','msg'=>'Registro ingresado'];

    }
    */

}

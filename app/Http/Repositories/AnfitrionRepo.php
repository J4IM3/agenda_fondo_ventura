<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\Anfitrion;
use App\Http\Entities\Invitado;
use App\Http\Services\ChangeStatus;
use Illuminate\Http\Request;

class AnfitrionRepo extends BaseRepo {

    public function getModel()
    {
        return new Anfitrion();
    }

    public function getAnfitrionActives()
    {
        return Anfitrion::where('status',1)->get();
    }

    public function save($data)
    {
        $anfitrion = Anfitrion::create([
            'nombre'=>$data['nombre'],
            'telefono'=>$data['telefono'],
            'status'=>1

        ]);
        if (!empty($anfitrion))
        {
            return ['success'=>'success','msg'=>'Regitro guardado'];
        }
        return ['success'=>'Error','msg'=>'Ah ocurrido un error, consulte con su administrador'];
    }

    public function update($data)
    {
        $anfitrion = $this->find($data['id']);
        $anfitrion->nombre = $data['nombre'];
        $anfitrion->telefono = $data['telefono'];
        if ($anfitrion->save())
        {
            return ['success'=>'success','msg'=>'Regitro actualizado'];
        }
        return ['success'=>'Error','msg'=>'Ah ocurrido un error, consulte con su administrador'];
    }
}

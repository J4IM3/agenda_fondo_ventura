<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\Anfitrion;
use App\Http\Entities\Foro;
use App\Http\Entities\Invitado;
use App\Http\Entities\EventoTipo;
use App\Http\Services\ChangeStatus;
use Illuminate\Http\Request;

class ForoRepo extends BaseRepo
{
    private $changeStatus;
    public function __construct(ChangeStatus $changeStatus)
    {
        $this->changeStatus = $changeStatus;
    }

    public function getModel()
    {
        return new Foro();
    }

    public function create($data)
    {
        $foro = Foro::create([
            'nombre' => $data['nombre'],
            'sede_id' => $data['sede_id'],
            'type' => $data['type'],
            'status' => 1
        ]);

        if (!empty($foro))
        {
            return ['success'=>'success','msg'=>'Registro agregado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte a tu administrador'];
    }

    public function edit($data)
    {
        $foro = $this->find($data['id']);
        $foro->nombre = $data['nombre'];
        $foro->type = $data['type'];
        $foro->sede_id = $data['sede_id'];
        if ($foro->save())
        {
            return ['success'=>'success','msg'=>'Registro actualizado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte a tu administrador'];
    }

    public function getForos()
    {
        return Foro::where('status',1)->get();
    }

    public function changeStatus($id)
    {
        $foro = Foro::where('id',$id)->first();
        if (!empty($foro))
        {
            $foro->status = $this->changeStatus->changeStatus($foro->status);
            if ($foro->save())
            {
                return ['success'=>'success','msg'=>'Registro actualizado'];
            }
            return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte a tu administrador'];
        }
    }

}

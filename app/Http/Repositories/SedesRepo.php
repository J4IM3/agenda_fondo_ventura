<?php

namespace App\Http\Repositories;

use App\Http\Entities\Sedes;
use App\Http\Services\ChangeStatus;

class SedesRepo
{
    private $changeStatus;
    public function __construct(ChangeStatus $changeStatus)
    {
        $this->changeStatus = $changeStatus;
    }
    public function add($data)
    {
        $data['status'] = "1";
        $sede = Sedes::create($data);
        if (!empty($sede))
        {
            return ['success' => 'success','msg' => "Registro agregado"];
        }
        return ['success' => 'error','msg' => "Ocurrio un error"];
    }

    public function edit($data)
    {
        $sede = Sedes::find($data['id']);
        if (!empty($sede))
        {
            $sede->name = $data['name'];
            $sede->direction = $data['direction'];
            if ($sede->save())
            {
                return ['success' => 'success','msg' => "Registro actualizado"];
            }
            return ['success' => 'error','msg' => "Ocurrio un error al actualizar el registro"];
        }
        return ['success' => 'error','msg' => "No se encontro el registro a actualizar"];
    }

    public function changeStatus($id)
    {
        $sede = Sedes::where('id',$id)->first();
        if (!empty($sede))
        {
            $sede->status = $this->changeStatus->changeStatus($sede->status);
            if ($sede->save())
            {
                return ['success' => 'success','msg' => "Registro actualizado"];
            }
            return ['success' => 'success','msg' => "Ocurrio un error al actualizar el registro"];
        }
        return ['success' => 'success','msg' => "No se encontro el registro"];
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\Invitado;
use App\Http\Entities\Itinerarios;
use App\Http\Services\ChangeStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rules\In;


class InvitadosRepo extends BaseRepo {

    public function getModel()
    {
        return new Invitado();
    }

    public function getInvitado()
    {
        return  DB::table('invitados')
            ->select('invitados.id','invitados.name')
            ->leftJoin('itinerarios','invitados.id','=','itinerarios.invitado_id')
            ->whereNull('itinerarios.invitado_id')
            ->get();


    }
    public function save($data,$imageName){
        $data['avatar'] = $imageName;
        $invitado = Invitado::create($data);
        /*$invitado = Invitado::create([
            'name'=>$data['name'],
            'slug'=>$data['slug'],
            'email'=>$data['email'],
            'phone'=>$data['phone'],
            'residencia'=>$data['residencia'],
            'semblanza'=>$data['semblanza'],
            'avatar' =>,
            'sexo' =>$data['sexo']
        ]);*/
        if (!isset($invitado)){
            return ['success'=>'error','msg'=>'Ah ocurrido un error'];
        }
        return ['success'=>'success','msg'=>'Registro agregado','data'=>$invitado];
    }

    public function update($data,$nameImage){
        $invitado = $this->find($data['id']);
        if ($nameImage == null)
        {
            $nameImage = $invitado->avatar;
        }

        $invitado->name = $data['name'];
        $invitado->slug = $data['slug'];
        $invitado->semblanza = $data['semblanza'];
        $invitado->residencia = $data['residencia'];
        $invitado->phone = $data['phone'];
        $invitado->email= $data['email'];
        $invitado->sexo= $data['sexo'];
        $invitado->avatar= $nameImage;

        if (!$invitado->save()){
            return ['success'=>'error','msg'=>'Ah ocurrido un error'];
        }
        return ['success'=>'success','msg'=>'Registro actualizado'];
    }

    public function deleteUser($id)
    {
        $guest =  $this->find($id);

        try{
            if($guest->delete()){
                return ["success"=>'success','msg'=>'Registro eliminado'];
            }
                }catch(Exception $e){
                    return ["success"=>'deleteError','msg'=>'Registro utilizado en algun elemento en el sistema, contacta a tu administrador'];
                }
    }

    public function deleteImage($id)
    {
        $invitado = Invitado::where('id',$id)->first();
        File::delete('avatar/'.$invitado->avatar);
        $invitado->avatar = null;
        if ($invitado->save())
        {
            return ["success"=>'success','msg'=>'Foto eliminada'];
        }
        return ["success"=>'error','msg'=>'Ocurrio un error, consulte a su administrador'];
    }

    public function findEmail($data)
    {
        if (!empty($data['value']))
        {
            $guestEmail = Invitado::where($data['field'],$data['value'])-> first();
            if (!empty($guestEmail))
            {
                return ['success' =>'error','msg'=> ' Este registro ya existe'];
            }
            return ['success' =>'success','msg'=> ''];
        }
    }
}

<?php namespace App\Http\Repositories;
use DB;

abstract class BaseRepo {

    protected $model;

    public function __construct(){
        $this->model = $this->getModel();
    }



    abstract public function getModel();

    public function all(){
        return $this->getModel()->get();
    }

    public function find($id){
        return $this->getModel()->find($id);
    }

    public function lists($table,$campos = array('nombre','id')){
        return DB::table($table)->lists($campos[0],$campos[1]);
    }

    public function delete($id){
        return $this->getModel()->destroy($id);
    }

    public function getPermiso($user_id, $permiso_id){
        return DB::table('permiso_user')
            ->where('user_id','=',$user_id)
            ->where('permiso_id','=',$permiso_id,'AND')
            ->get();
    }

    public static function getEnumValues($table, $column)
    {
        $type = DB::select( DB::raw("SHOW COLUMNS FROM $table WHERE Field = '$column'") )[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $vd = $v;

            $enum = array_add($enum, $v, $vd);
        }

        return $enum;
    }
}

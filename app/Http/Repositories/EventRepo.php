<?php

namespace App\Http\Repositories;

use App\Http\Entities\EventBooks;
use App\Http\Entities\DetailEvents;
use App\Http\Entities\Evento_participantes;
use App\Http\Entities\Eventos;
use App\Http\Entities\Moderadores;
use App\Http\Entities\Presentadores;
use App\Http\Services\ComparaEventos;
use App\Http\Services\ComparaFiloando;
use App\Http\Services\ComparaForos;
use App\Http\Services\ComparaPruebaSonido;
use App\Http\Services\CompararPrensa;
use App\Http\Services\CompararService;
use App\Http\Services\CompararSocial;

class EventRepo
{

    private $compararService;
    private $comparaForos;
    private $comparaPruebaSonido;
    private $comparaFiloando;
    private $compararPrensa;
    private $compararSocial;

    public function __construct(
        CompararService $compararService,
        ComparaForos $comparaForos,
        ComparaPruebaSonido $comparaPruebaSonido,
        ComparaFiloando $comparaFiloando,
        CompararPrensa $compararPrensa,
        CompararSocial $compararSocial
    )
    {
        $this->compararService = $compararService;
        $this->comparaForos = $comparaForos;
        $this->comparaPruebaSonido = $comparaPruebaSonido;
        $this->comparaFiloando = $comparaFiloando;
        $this->compararPrensa = $compararPrensa;
        $this->compararSocial = $compararSocial;
    }

    public function saveEvent($data)
    {
        $invitados = [];
        $isPresenter = '0';
        $positionBookForPresenter = null;

        $moderador_id = isset($data['moderador_id']) ? $data['moderador_id'] : "";
        $presentador_id = isset($data['presentador_id']) ? $data['presentador_id'] : "";

        //Compara la hora final con la hora inicial de un evento, la hora final no puede ser menor a la hora inicial
        if ($this->compararService->convertir($data['hora_fin']) <= $this->compararService->convertir($data['hora_inicio'])) {
            return ['success' => 'error', 'msg' => 'La hora final no puede ser menor o igual a la hora de inicio'];
        }

        //Hace la validacion donde checa si el foro seleccionado esta disponible

        $validaForos = $this->validaForos($data['foro_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($validaForos != "true") {
            return $validaForos;
        }
        //Valida que los invitados no tengan alguna participacion a la misma hora en otros eventos, filoando, social,
        //encuentros filo, etc
        $validaInvitados = $this->validaInvitadosFilo($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin']);
        if ($validaInvitados = !"true") {
            return $validaInvitados;
        }

        if (!empty($data['moderador_id'])) {
            foreach ($data['moderador_id'] as $datum) {
                if (in_array($datum, $data['invitado_id'])) {
                    return ['success' => 'error', 'msg' => 'El invitado al evento y el moderador no pueden ser la misma persona.'];
                } else {
                    array_push($data['invitado_id'], $datum);
                }
            }
        }

        //Inserta los ids que viene en el input presentador al array $data['invitado_id']
        if (!empty($data['presentador_id'])) {
            foreach ($data['presentador_id'] as $datum) {
                if (in_array($datum, $data['invitado_id'])) {
                    return ['success' => 'error', 'msg' => 'El invitado al evento y el presentador no pueden ser la misma persona.'];
                } else {
                    array_push($data['invitado_id'], $datum);
                }
            }
        }


        //Guarda en la variable invitados los id's de estos para ser utilizados al momento de crear el
        foreach ($data['invitado_id'] as $item) {
            $invitados[] = $item;
        }


        //Crea un evento
        $evento = $this->crearEvento($data);


        //Guarda la posicion del index donde va el libro seleccionado para el promotor

        $positionBookForPresenter = !empty($data['title_for_promotor']) ? $this->getPositionBookForPresenter($data['title_for_promotor']) : null;

        //Guarda los libros  para el evento
        $this->saveBooksForEvent($data, $positionBookForPresenter, $evento->id);

        /**
         * Guardar participantes
         */
        $this->guardarParticipantes($invitados, $evento->id);

        /**
         * Guardar si es que existen, moderadores
         */

        if (!empty($data['moderador_id'])) {
            $this->guardarModeradores($data['moderador_id'], $evento->id);
        }

        /**
         * Guardar si es que existen, presentadores
         */
        if (!empty($data['presentador_id'])) {
            $this->guardarPresentadores($data['presentador_id'], $evento->id);
        }

        return ['success' => 'success', 'msg' => 'Evento generado con exito.'];
    }

    public function getPositionBookForPresenter($titles)
    {
        $x = [];
        foreach ($titles as $key => $promotor)
        {
            if (!empty($promotor))
            {
                $x[] =  $key;
            }
        }
        return $x;
    }

    public function saveBooksForEvent($data, $positionBookForPresenter, $eventId)
    {
        foreach ($data['title'] as $key => $item) {
            if (!empty($item)) {
                if (in_array($key,$positionBookForPresenter)) {
                    $isPresenter = 1;
                    $pos = $key;
                    $quantity = $data['quantity'][$pos] != null ? $data['quantity'][$pos] : 0;
                } else {
                    $isPresenter = 0;
                    $pos = 0;
                    $quantity = 0;
                }

                EventBooks::create([
                    'titulo' => $item,
                    'is_presenter' => $isPresenter,
                    'event_id' => $eventId,
                    'quantity_for_presenter' => $quantity,
                ]);
            }
        }
    }


    public function validaForos($foroId,$fecha,$horaInicio,$horaFin)
    {
        //Compara si el foro seleccionado tiene un evento a la hora seleccionada
        $foros = $this->comparaForos->compararEvento($foroId, $fecha, $horaInicio, $horaFin, '0');
        if ($foros != "false") {
            return $foros;
        }

        //Compara foros de prueba de sonido
        if (!isset($data['evento_id'])) {
            $data['evento_id'] = 0;
        }

        $pruebaSonido = $this->comparaPruebaSonido->compararEvento($foroId, $fecha, $horaInicio, $horaFin, $data['evento_id']);
        if ($pruebaSonido != "false") {
            return $pruebaSonido;
        }
        return "true";

    }

    public function validaInvitadosFilo($invitadosId,$fecha,$horaInicio,$horaFin)
    {
        /**
         * Compara que los invitados no tengan pruebas de sonido
         */
        $pruebaSonido = $this->comparaPruebaSonido->compararEventoInvitado($invitadosId, $fecha, $horaInicio, $horaFin, 0);
        if ($pruebaSonido != "false") {
            return $pruebaSonido;
        }
        //Compara eventos de filoando
        $eventosFiloando = $this->comparaFiloando->comparaFiloando($invitadosId, $fecha, $horaInicio, $horaFin, '0');
        if ($eventosFiloando != "false") {
            return $eventosFiloando;
        }
        //Compara eventos de prensa
        $eventosPrensa = $this->compararPrensa->compararPrensa($invitadosId, $fecha, $horaInicio, $horaFin, '0');
        if ($eventosPrensa != "false") {
            return $eventosPrensa;
        }

        //Compara eventos Sociales
        $eventoSocial = $this->compararSocial->compararSocial($invitadosId, $fecha, $horaInicio, $horaFin, '0');
        if ($eventoSocial != "false") {
            return $eventoSocial;
        }
    }


    public function crearEvento($data)
    {
        $evento = Eventos::create([
            'nombre_evento' => $data['nombre_evento'],
            'descripcion' => $data['descripcion'],
            'tipo_id' => $data['tipo_id'],
            'programacion_id' => $data['programacion_id'],
            'ciclo' => $data['ciclo'],
            'requirimientos' => $data['requirimientos'],
            'status' => 1,
            'colaborador' => $data['colaborador'],
            'formato' => $data['formato'],
            'fecha' => $data['fecha'],
            'hora_inicio' => $data['hora_inicio'],
            'hora_fin' => $data['hora_fin'],
        ]);

        if (!empty($evento)) {
            foreach ($data['foro_id'] as $sede)
                DetailEvents::create([
                    'fecha' => $data['fecha'],
                    'hora_inicio' => $data['hora_inicio'],
                    'hora_fin' => $data['hora_fin'],
                    'sede_id' => $sede,
                    'event_id' => $evento->id
                ]);
            return $evento;
        }
    }

    public function guardarParticipantes($invitados, $evento_id)
    {
        foreach ($invitados as $invitado) {
            Evento_participantes::create([
                'invitado_id' => $invitado,
                'evento_id' => $evento_id
            ]);
        }
    }

    public function guardarModeradores($moderadores, $evento_id)
    {
        foreach ($moderadores as $moderador) {
            Moderadores::create([
                'moderador_id' => $moderador,
                'evento_id' => $evento_id
            ]);
        }

    }

    /**
     * Guarda presentadores
     */

    public function guardarPresentadores($presentadores, $evento_id)
    {
        foreach ($presentadores as $presentador) {
            Presentadores::create([
                'presentador_id' => $presentador,
                'evento_id' => $evento_id
            ]);
        }
    }


}

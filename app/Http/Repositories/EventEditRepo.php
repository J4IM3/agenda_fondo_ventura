<?php

namespace App\Http\Repositories;

use App\Http\Entities\DetailEvents;
use App\Http\Entities\Evento_participantes;
use App\Http\Entities\Eventos;
use App\Http\Entities\Moderadores;
use App\Http\Entities\Presentadores;

class EventEditRepo
{
    public function edit($data)
    {
        $invitados = [];
        //Compara la hora final con la hora inicial de un evento, la hora final no puede ser menor a la hora inicial
        if ($this->compararService->convertir($data['hora_fin']) <= $this->compararService->convertir($data['hora_inicio'])) {
            return ['success' => 'error', 'msg' => 'La hora final no puede ser menor o igual a la hora de inicio'];
        }

        //guarda en la variable invitados los id's de estos para ser utilizados al momento de crear el evento
        foreach ($data['invitado_id'] as $item) {
            $invitados[] = $item;
        }


        //Inserta los ids que viene en el input moderador al array $data['invitado_id']
        if (!empty($data['moderador_id'])) {
            foreach ($data['moderador_id'] as $datum) {
                if (in_array($datum, $data['invitado_id'])) {
                    return ['success' => 'error', 'msg' => 'El invitado al evento y el moderador no pueden ser la misma persona.'];
                } else {
                    array_push($data['invitado_id'], $datum);
                }
            }
        }

        //Inserta los ids que viene en el input presentador al array $data['invitado_id']
        if (!empty($data['presentador_id'])) {
            foreach ($data['presentador_id'] as $datum) {
                if (in_array($datum, $data['invitado_id'])) {
                    return ['success' => 'error', 'msg' => 'El invitado al evento y el presentador no pueden ser la misma persona.'];
                } else {
                    array_push($data['invitado_id'], $datum);
                }
            }
        }

        //Compara foros
        $foros = $this->comparaForos->compararEvento($data['foro_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], $data['id']);
        if ($foros != "false") {
            return $foros;
        }

        //Compara foros de prueba de sonido
        $pruebaSonido = $this->comparaPruebaSonido->compararEvento($data['foro_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], 0);
        if ($pruebaSonido != "false") {
            return $pruebaSonido;
        }

        /**
         * Compara que los invitados no tengan pruebas de sonido
         */
        $pruebaSonidoInvitados = $this->comparaPruebaSonido->compararEventoInvitado($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], 0);
        if ($pruebaSonidoInvitados != "false") {
            return $pruebaSonidoInvitados;
        }


        //Compara eventos filo
        $eventos = $this->comparaEventos->compararEvento($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], $data['id']);
        if ($eventos != "false") {
            return $eventos;
        }

        //Compara eventos de filoando
        $eventosFiloando = $this->comparaFiloando->comparaFiloando($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], "0");
        if ($eventosFiloando != "false") {
            return $eventosFiloando;
        }

        //Compara eventos de prensa
        $eventosPrensa = $this->compararPrensa->compararPrensa($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($eventosPrensa != "false") {
            return $eventosPrensa;
        }

        //Verifica que el invitado no tenga una prueba de sonido a la misma hr
        $eventoSocial = $this->compararSocial->compararSocial($data['invitado_id'], $data['fecha'], $data['hora_inicio'], $data['hora_fin'], '0');
        if ($eventoSocial != "false") {
            return $eventoSocial;
        }

        $participantes = Evento_participantes::where('evento_id', $data['id'])->get();

        foreach ($participantes as $item) {
            $item->delete();
        }

        foreach ($invitados as $invitado) {
            Evento_participantes::create([
                'evento_id' => $data['id'],
                'invitado_id' => $invitado
            ]);
        }

        $evento = Eventos::where('id', $data['id'])->first();

        $evento->fecha = $data['fecha'];
        $evento->nombre_evento = $data['nombre_evento'];
        $evento->descripcion = $data['descripcion'];
        $evento->tipo_id = $data['tipo_id'];
        $evento->programacion_id = $data['programacion_id'];
        $evento->ciclo = $data['ciclo'];
        $evento->requirimientos = $data['requirimientos'];
        $evento->status = 1;
        $evento->colaborador = $data['colaborador'];
        $evento->formato = $data['formato'];
        $evento->visible = 1;
        $evento->hora_inicio = $data['hora_inicio'];
        $evento->hora_fin = $data['hora_fin'];
        $evento->save();

        $detalle = DetailEvents::where('event_id', $data['id'])->get();
        foreach ($detalle as $item) {
            $item->delete();
        }
        foreach ($data['foro_id'] as $datum) {
            DetailEvents::create([
                'hora_inicio' => $data['hora_inicio'],
                'hora_fin' => $data['hora_fin'],
                'fecha' => $data['fecha'],
                'sede_id' => $datum,
                'event_id' => $evento->id

            ]);
        }
        $moderadores = Moderadores::where('evento_id', $data['id'])->get();
        $presentadores = Presentadores::where('evento_id', $data['id'])->get();
        if (count($moderadores) > 0) {
            foreach ($moderadores as $item) {
                $moderador = Moderadores::where('moderador_id', $item->moderador_id)->first();
                $moderador->delete();

            }
        }
        if (!empty($data['moderador_id'])) {
            $this->guardarModeradores($data['moderador_id'], $evento->id);
        }

        if (count($presentadores) > 0) {
            foreach ($presentadores as $item) {
                $presentador = Presentadores::where('presentador_id', $item->presentador_id)->first();
                $presentador->delete();
            }
        }
        if (!empty($data['presentador_id'])) {
            $this->guardarPresentadores($data['presentador_id'], $evento->id);
        }

        return ['success' => 'success', 'msg' => 'Evento actualizado'];
    }
}

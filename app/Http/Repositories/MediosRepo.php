<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 05/08/2019
 * Time: 06:37 PM
 */

namespace App\Http\Repositories;


use App\Http\Entities\Medios;

class MediosRepo extends BaseRepo
{
    public function getModel()
    {
        return new Medios();
    }

    public function create($data)
    {
        $medios = Medios::create([
            'nombre'=>$data['nombre'],
            'status'=>1
        ]);

        if (!empty($medios))
        {
            return ['success'=>'success','msg'=>'Registro guardado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error'];
    }


    public function edit($data)
    {
        $medio = $this->find($data['id']);
        $medio->nombre = $data['nombre'];
        if ($medio->save())
        {
            return ['success'=>'success','msg'=>'Registro guardado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error'];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\Anfitrion;
use App\Http\Entities\Invitado;
use App\Http\Entities\EventoTipo;
use App\Http\Services\ChangeStatus;
use Illuminate\Http\Request;

class EventoTipoRepo extends BaseRepo
{

    public function getModel()
    {
        return new EventoTipo();
    }

    public function create($data)
    {
        $tipo = EventoTipo::create([
            'nombre' => $data['nombre'],
            'tipo' => $data['tipo'],
            'status' => 1
        ]);

        if (empty($tipo)) {
            return ['success' => 'success', 'msg' => 'Ah ocurrido un error, consulte con su administrador'];
        }
        return ['success' => 'success', 'msg' => 'Registro agregado'];

    }

    public function edit($data)
    {
        $tipo = $this->find($data['id']);
        $tipo->nombre = $data['nombre'];
        $tipo->tipo = $data['tipo'];
        if ($tipo->save()) {
            return ['success' => 'success', 'msg' => 'Registro actualizado'];
        }
        return ['success' => 'success', 'msg' => 'Ah ocurrido un error, consulte con su administrador'];
    }

    public function tipoEvento($tipo)
    {
        return EventoTipo::where('tipo',$tipo)
            ->where('status',1)
            ->get();
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\Aerolineas;
use App\Http\Entities\Invitado;
use App\Http\Services\ChangeStatus;
use Illuminate\Http\Request;

class AerolineasRepo extends BaseRepo {

    /**
     * @var ChangeStatus
     */
    private $changeStatus;

    public function getModel()
    {
        return new Aerolineas();
    }

    public function __construct(ChangeStatus $changeStatus)
    {
        $this->changeStatus = $changeStatus;
    }

    public function save($data){
        $aero = Aerolineas::create([
            'name'=>$data['name']
        ]);

        if (!$aero){
            return ['success'=>'error','msg'=>'Error al ingresar el registro, consulte a su administador'];
        }
        return ['success'=>'success','msg'=>'Registro guardado.'];
    }

    public function changeStatus($id){
        $aero = $this->find($id);
        $status = $this->changeStatus->changeStatus($aero->status);
        $aero->status = $status;
        if(!$aero->save()){
            return ['success'=>'error','msg'=>'Error al ingresar el registro, consulte a su administador'];
        }
        return ['success'=>'success','msg'=>'Registro actualizado.'];
    }

    public function update($data){
        $aero = $this->find($data['id']);
        $aero->name= $data['name'];
        if (!$aero->save()){
            return ['success'=>'error','msg'=>'Error al ingresar el registro, consulte a su administador'];
        }
        return ['success'=>'success','msg'=>'Registro actualizado.'];
    }

    public function getAero(){
        return Aerolineas::where('status','1')->get();
    }
}

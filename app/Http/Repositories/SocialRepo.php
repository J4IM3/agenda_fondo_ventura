<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 05:10 PM
 */

namespace App\Http\Repositories;
use App\Http\Entities\FiloandoInvitados;
use App\Http\Entities\Social;
use App\Http\Entities\Evento_participantes;
use App\Http\Entities\Prensa;

use App\Http\Services\ComparaEventos;
use App\Http\Services\ComparaFiloando;
use App\Http\Services\CompararPrensa;
use App\Http\Services\CompararService;
use App\Http\Services\CompararSocial;
use Illuminate\Http\Request;

class SocialRepo extends BaseRepo {

    /**
     * @var PrensaRepo
     */
    private $prensaRepo;
    /**
     * @var CompararService
     */
    private $compararService;
    /**
     * @var InvitadosRepo
     */
    private $invitadosRepo;
    /**
     * @var ComparaEventos
     */
    private $comparaEventos;
    /**
     * @var CompararPrensa
     */
    private $compararPrensa;
    /**
     * @var ComparaFiloando
     */
    private $comparaFiloando;
    /**
     * @var CompararSocial
     */
    private $compararSocial;

    public function getModel()
    {
        return new Social();
    }

    public function __construct(CompararService $compararService,
                                InvitadosRepo $invitadosRepo,
                                ComparaEventos $comparaEventos,
                                ComparaFiloando $comparaFiloando,
                                CompararPrensa $compararPrensa,CompararSocial $compararSocial)
    {
        $this->compararService = $compararService;
        $this->invitadosRepo = $invitadosRepo;
        $this->comparaEventos = $comparaEventos;
        $this->compararPrensa = $compararPrensa;
        $this->comparaFiloando = $comparaFiloando;
        $this->compararSocial = $compararSocial;
    }

    public function save($data)
    {
        $comparaHorasIniciales = $this->compararService->comparaMultiplesHoras(
            $this->compararService->convertir($data['traslado_ida']),
            $this->compararService->convertir($data['hora_inicio']),
            $this->compararService->convertir($data['hora_fin']),
            $this->compararService->convertir($data['traslado_regreso']));
        if ($comparaHorasIniciales != "true")
        {
            return $comparaHorasIniciales;
        }
        //Compara eventos filo
        $eventos = $this->comparaEventos->compararEvento($data['invitado_id'],$data['fecha'],$data['traslado_ida'],$data['traslado_regreso'],'0');
        if ($eventos != "false")
        {
            return $eventos;
        }

        //Compara eventos de filoando
        $eventosFiloando = $this->comparaFiloando->comparaFiloando($data['invitado_id'],$data['fecha'],$data['traslado_ida'],$data['traslado_regreso'],'0');
        if ($eventosFiloando != "false")
        {
            return $eventosFiloando;
        }
        //Compara eventos de prensa
        $eventosPrensa = $this->compararPrensa->compararPrensa($data['invitado_id'],$data['fecha'],$data['traslado_ida'],$data['traslado_regreso'],'0');
        if ($eventosPrensa != "false")
        {
            return $eventosPrensa;
        }

        $eventoSocial = $this->compararSocial->compararSocial($data['invitado_id'],$data['fecha'],$data['traslado_ida'],$data['traslado_regreso'],'0');
        if ($eventoSocial != "false")
        {
            return $eventoSocial;
        }

        foreach ($data['invitado_id'] as $guest)
        {
            $guest_id  = $guest;
        }
        Social::create([
            'invitado_id' => $guest_id,
            'lugar_id' => $data['lugar_id'],
            'fecha' => $data['fecha'],
            'actividad' => $data['actividad'],
            'traslado_ida' => $data['traslado_ida'],
            'traslado_regreso' => $data['traslado_regreso'],
            'hora_inicio' => $data['hora_inicio'],
            'hora_fin' => $data['hora_fin'],
        ]);
        return ['success' => 'success','msg' => 'Registro guardado'];
    }

    public function update($data)
    {
        $comparaHorasIniciales = $this->compararService->comparaMultiplesHoras(
            $this->compararService->convertir($data['traslado_ida']),
            $this->compararService->convertir($data['hora_inicio']),
            $this->compararService->convertir($data['hora_fin']),
            $this->compararService->convertir($data['traslado_regreso']));
        if ($comparaHorasIniciales != "true")
        {
            return $comparaHorasIniciales;
        }
        //Compara eventos filo
        $eventos = $this->comparaEventos->compararEvento($data['invitado_id'],$data['fecha'],$data['traslado_ida'],$data['traslado_regreso'],$data['id']);
        if ($eventos != "false")
        {
            return $eventos;
        }

        //Compara eventos de filoando
        $eventosFiloando = $this->comparaFiloando->comparaFiloando($data['invitado_id'],$data['fecha'],$data['traslado_ida'],$data['traslado_regreso'],"0");
        if ($eventosFiloando != "false")
        {
            return $eventosFiloando;
        }
        //Compara eventos de prensa
        $eventosPrensa = $this->compararPrensa->compararPrensa($data['invitado_id'],$data['fecha'],$data['traslado_ida'],$data['traslado_regreso'],'0');
        if ($eventosPrensa != "false")
        {
            return $eventosPrensa;
        }

        $eventoSocial = $this->compararSocial->compararSocial($data['invitado_id'],$data['fecha'],$data['traslado_ida'],$data['traslado_regreso'],$data['id']);
        if ($eventoSocial != "false")
        {
            return $eventoSocial;
        }

        foreach ($data['invitado_id'] as $guest)
        {
            $guestId = $guest;
        }

        $socialEvent = Social::where('id',$data['id'])->first();
        $socialEvent->invitado_id = $guestId;
        $socialEvent->lugar_id = $data['lugar_id'];
        $socialEvent->actividad =  $data['actividad'];
        $socialEvent->fecha =  $data['fecha'];
        $socialEvent->hora_inicio = $data['hora_inicio'];
        $socialEvent->hora_fin = $data['hora_fin'];
        $socialEvent->traslado_ida = $data['traslado_ida'];
        $socialEvent->traslado_regreso = $data['traslado_regreso'];
        if ($socialEvent->save()){
            return ['success'=>'success','msg'=>'Registro actualizado'];
        }


    }

    /*
    public function update($data){
        $inicio = $this->compararService->convertir($data['hora_inicio']);
        $fin = $this->compararService->convertir($data['hora_fin']);


        $compararHoras = $this->compararService->compararHoras($data['hora_inicio'],$data['hora_fin']);
        if ($compararHoras != "true"){
            return $compararHoras;
        }

        //inicia comparaciones
        $evento = Evento_participantes::with('eventos','invitadosNombre')->where('invitado_id',$data['invitado_id'])->get();

        if (!empty($evento)){
            foreach ($evento as $value)
            {
                $compararEvento = $this->compararService->comparar($value['eventos'],$inicio,$fin,$data['fecha'],"la filo");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," la filo de ".$value['invitadosNombre'][0]->name);
                }
            }
        }

        //comparar Filoando

        $filoando = FiloandoInvitados::with('eventosFiloando','invitadosNombre')
            ->where('invitado_id',$data['invitado_id'])
            ->get();

        if (!empty($filoando)){
            foreach ($filoando as $value)
            {
                $compararEvento = $this->compararService->compararFiloando($value->eventosFiloando,$inicio,$fin,$data['fecha'],"la filo");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_traslado_ida,$compararEvento->hora_traslado_regreso," tipo FILOANDO de ".$value['invitadosNombre'][0]->name);
                }
            }
        }

        $prensa = Prensa::with('prensa')
            ->where('invitado_id',$data['invitado_id'])
            ->get();

        if (!empty($prensa))
        {
            $compararEvento = $this->compararService->comparar($prensa,$inicio,$fin,$data['fecha'],"prensa");
            if ($compararEvento != "true" ){
                return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," prensa  de  ".$compararEvento->prensa[0]->name);
            }
        }

        $social = Social::with('socialInvitado')
            ->where('invitado_id',$data['invitado_id'])
            ->where('id','=!',$data['id'])
            ->get();
        if (!empty($social))
        {
            $compararEvento = $this->compararService->comparar($social,$inicio,$fin,$data['fecha'],"social");
            if ($compararEvento != "true" ){
                return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," tipo social de ".$compararEvento->socialInvitado[0]->name);
            }
        }


        $social = $this->find($data['id']);
        $social->invitado_id = $data['invitado_id'];
        $social->lugar_id = $data['lugar_id'];
        $social->actividad =  $data['actividad'];
        $social->fecha =  $data['fecha'];
        $social->hora_inicio = $data['hora_inicio'];
        $social->hora_fin = $data['hora_fin'];
        $social->traslado_ida = $data['traslado_ida'];
        $social->traslado_regreso = $data['traslado_regreso'];
        if ($social->save()){
            return ['success'=>'success','msg'=>'Registro actualizado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error, consulta a tu administrador'];
    }
    */

    public function delete($id){
        $social = $this->find($id);
        if ($social->delete()){
            return ['success'=>'success','msg'=>'Registro eliminado'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error ,consulta con tu adminstrador'];
    }

    /*
     public function save($data){
        $inicio = $this->compararService->convertir($data['hora_inicio']);
        $fin = $this->compararService->convertir($data['hora_fin']);


        $compararHoras = $this->compararService->compararHoras($data['hora_inicio'],$data['hora_fin']);
        if ($compararHoras != "true"){
            return $compararHoras;
        }

        //inicia comparaciones
        $evento = Evento_participantes::with('eventos','invitadosNombre')->where('invitado_id',$data['invitado_id'])->get();

        if (!empty($evento)){
            foreach ($evento as $value)
            {
                $compararEvento = $this->compararService->comparar($value['eventos'],$inicio,$fin,$data['fecha'],"la filo");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," la filo de ".$value['invitadosNombre'][0]->name);
                }
            }
        }

        //comparar Filoando

        $filoando = FiloandoInvitados::with('eventosFiloando','invitadosNombre')
            ->where('invitado_id',$data['invitado_id'])
            ->get();

        if (!empty($filoando)){
            foreach ($filoando as $value)
            {
                $compararEvento = $this->compararService->compararFiloando($value->eventosFiloando,$inicio,$fin,$data['fecha'],"la filo");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_traslado_ida,$compararEvento->hora_traslado_regreso," tipo FILOANDO de ".$value['invitadosNombre'][0]->name);
                }
            }
        }

        $prensa = Prensa::with('prensa')
            ->where('invitado_id',$data['invitado_id'])
            ->get();

        if (!empty($prensa))
        {
            $compararEvento = $this->compararService->comparar($prensa,$inicio,$fin,$data['fecha'],"prensa");
            if ($compararEvento != "true" ){
                return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," prensa  de  ".$compararEvento->prensa[0]->name);
            }
        }

        $social = Social::with('socialInvitado')
            ->where('invitado_id',$data['invitado_id'])
            ->get();
        if (!empty($social))
        {
            $compararEvento = $this->compararService->comparar($social,$inicio,$fin,$data['fecha'],"social");
            if ($compararEvento != "true" ){
                return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," tipo social de ".$compararEvento->socialInvitado[0]->name);
            }
        }

        $social = Social::create([
            'invitado_id'=>$data['invitado_id'],
            'lugar_id'=>$data['lugar_id'],
            'actividad'=> $data['actividad'],
            'fecha'=> $data['fecha'],
            'hora_inicio'=>$data['hora_inicio'],
            'hora_fin'=>$data['hora_fin'],
            'traslado_ida'=>$data['traslado_ida'],
            'traslado_regreso'=>$data['traslado_regreso'],
        ]);

        if (!empty($social)){
            return ['success'=>'success','msg'=>'Se ah creado nuevo evento '];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error, consulte con su administrador'];
    }
     */
}

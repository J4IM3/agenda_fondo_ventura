<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 04/06/2019
 * Time: 02:11 PM
 */
namespace App\Http\Services;

class ChangeStatus{

    public function changeStatus($status){
        if($status == 0){
            return 1;
        }
        return 0;
    }
}
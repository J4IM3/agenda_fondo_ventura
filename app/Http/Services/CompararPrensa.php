<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-04-09
 * Time: 01:39
 */

namespace App\Http\Services;
use App\Http\Entities\Prensa;
use App\Http\Entities\Invitado;

class CompararPrensa
{
    /**
     * @var CompararService
     */
    private $compararService;

    public function __construct(CompararService $compararService)
    {
        $this->compararService = $compararService;
    }

    public function compararPrensa($id_invitados,$fecha,$hora_inicio,$hora_fin,$evento_id)
    {
        foreach ($id_invitados as $id)
        {
                $eventosPrensa = Prensa::where('invitado_id', $id)
                    ->where('id','<>',$evento_id)
                    ->get();
                foreach ($eventosPrensa as $evento)
                {
                    if ($fecha == $evento->fecha)
                    {
                        $comparar = $this->compararService->compararHorario(
                            $this->compararService->convertir($hora_inicio),
                            $this->compararService->convertir($hora_fin),
                            $this->compararService->convertir($evento->hora_inicio),
                            $this->compararService->convertir($evento->hora_fin));

                        if ($comparar == "true") {
                            $invitado = Invitado::where('id', $id)->first();
                            return ['success' => 'error', 'msg' => $invitado->name . " tiene un evento de prensa el día " . $evento->fecha . " de " . $evento->hora_inicio . " a " . $evento->hora_fin];
                        }
                    }
                }
            }
        return "false";
    }

}
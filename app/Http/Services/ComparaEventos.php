<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-04-08
 * Time: 17:53
 */

namespace App\Http\Services;
use App\Http\Entities\Evento_participantes;
use App\Http\Entities\Eventos;
use App\Http\Entities\Invitado;

class ComparaEventos
{

    /**
     * @var CompararService
     */
    private $compararService;

    public function __construct(CompararService $compararService)
    {
        $this->compararService = $compararService;
    }


    /**
     * Metodo de prueba
     * Se encarga de verificar si el invitsdo tiene un evento en el dia y hora dada.
     */

    public function compararEvento($id_invitados,$fecha,$hora_inicio,$hora_fin,$evento_id)
    {
        foreach ($id_invitados as $id)
        {
            $eventosPartipantes = Evento_participantes::with('eventos')
                ->where('invitado_id',$id)
                ->where('evento_id','<>',$evento_id)
                ->get();
            foreach ($eventosPartipantes as $item)
            {
                $evento = Eventos::where('id',$item->evento_id)->first();
                if ($fecha  == $evento->fecha)
                {
                    $comparar = $this->compararService->compararHorario(
                        $this->compararService->convertir($hora_inicio),
                        $this->compararService->convertir($hora_fin),
                        $this->compararService->convertir($evento->hora_inicio),
                        $this->compararService->convertir($evento->hora_fin));

                    if ($comparar == "true")
                    {
                        $invitado = Invitado::where('id',$id)->first();
                        return ['success' => 'error','msg'=> $invitado->name." tiene el evento '".$evento->nombre_evento."'' el día ".$evento->fecha." de ".$evento->hora_inicio." a ".$evento->hora_fin];
                    }
                }
            }
        }
        return "false";
    }
}
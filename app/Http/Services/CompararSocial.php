<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-04-09
 * Time: 02:29
 */

namespace App\Http\Services;


use App\Http\Entities\Social;
use App\Http\Entities\Invitado;
class CompararSocial
{
    /**
     * @var CompararService
     */
    private $compararService;

    public function __construct(CompararService $compararService)
    {
        $this->compararService = $compararService;
    }

    public function compararSocial($id_invitados,$fecha,$hora_inicio,$hora_fin,$evento_id)
    {
        foreach ($id_invitados as $id)
        {
            $eventosSocial= Social::where('invitado_id', $id)
                ->where('id','<>',$evento_id)
                ->get();
            foreach ($eventosSocial as $evento)
            {
                if ($fecha == $evento->fecha)
                {
                    $comparar = $this->compararService->compararHorario(
                        $this->compararService->convertir($hora_inicio),
                        $this->compararService->convertir($hora_fin),
                        $this->compararService->convertir($evento->traslado_ida),
                        $this->compararService->convertir($evento->traslado_regreso));

                    if ($comparar == "true") {
                        $invitado = Invitado::where('id', $id)->first();
                        return ['success' => 'error', 'msg' => $invitado->name . " tiene un evento social el día " . $evento->fecha . " de " . $evento->traslado_ida. " a " . $evento->traslado_regreso];
                    }
                }
            }
        }
        return "false";
    }
}
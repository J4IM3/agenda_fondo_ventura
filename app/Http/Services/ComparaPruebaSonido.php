<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-04-14
 * Time: 18:13
 */

namespace App\Http\Services;

use App\Http\Entities\Evento_participantes;
use App\Http\Entities\Eventos;
use App\Http\Entities\Invitado;
use App\Http\Entities\PruebaSonido;


class ComparaPruebaSonido
{
    /**
     * @var CompararService
     */
    private $compararService;

    public function __construct(CompararService $compararService)
    {
        $this->compararService = $compararService;
    }

    public function compararEvento($foro_id,$fecha,$hora_inicio,$hora_fin,$value)
    {
        $pruebaSonido = PruebaSonido::where('foro_id',$foro_id)
            ->where('fecha',$fecha)
            ->where('evento_id','<>',$value)
            ->get();

        foreach ($pruebaSonido as $pruebas)
        {
            $comparar = $this->compararService->compararHorario2(
                $this->compararService->convertir($hora_inicio),
                $this->compararService->convertir($hora_fin),
                $this->compararService->convertir($pruebas->hora_inicio),
                $this->compararService->convertir($pruebas->hora_fin));

            if ($comparar == "true")
            {
                $evento = Eventos::where('id',$pruebas->evento_id)->first();
                return ['success' => 'error','msg'=> "Este foro cuenta con una prueba de sonido del evento ' ".$evento->nombre_evento." ' de ".$pruebas->hora_inicio. " a ".$pruebas->hora_fin];
            }
        }
        return "false";


    }

    public function compararEventoInvitado($invitados,$fecha,$hora_inicio,$hora_fin)
    {
        foreach ($invitados as $invitado)
        {
            $eventoParticipante = Evento_participantes::with('eventos.soundTest')
                ->where('invitado_id',$invitado)
                ->get();
            foreach ($eventoParticipante as $evento)
            {
                foreach ($evento->eventos as $eventos )
                {
                    if (!empty($eventos->soundTest))
                    {
                        if ($eventos->soundTest['fecha'] == $fecha)
                        {
                            $comparar = $this->compararService->compararHorario2(
                                $this->compararService->convertir($hora_inicio),
                                $this->compararService->convertir($hora_fin),
                                $this->compararService->convertir($eventos->soundTest['hora_inicio']),
                                $this->compararService->convertir($eventos->soundTest['hora_fin']));
                            if ($comparar == "true") {
                                $invitado = Invitado::where('id',$invitado)->first();
                                return ['success' => 'error', 'msg' => $invitado->name.
                                    " cuenta con una prueba de sonido relacionado al evento: ".
                                    $eventos->nombre_evento." ".
                                    $eventos->soundTest['fecha']." de ".
                                    $eventos->soundTest['hora_inicio']."-".
                                    $eventos->soundTest['hora_fin']];
                            }
                        }
                    }
                }
            }
        }
        return "false";
    }



}
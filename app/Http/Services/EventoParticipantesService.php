<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 05/08/2019
 * Time: 05:56 PM
 */

namespace App\Http\Services;


use App\Http\Entities\Evento_participantes;

class EventoParticipantesService
{
    /**
     * @var CompararService
     */
    private $compararService;

    public function __construct(CompararService $compararService)
    {
        $this->compararService = $compararService;
    }

    public function comparaEventos($data,$inicio,$fin)
    {
        foreach ($data['invitado_id'] as $item) {
            $evento = Evento_participantes::with('eventos', 'invitadosNombre')->where('invitado_id', $item)->get();

            if (!empty($evento)) {
                foreach ($evento as $value) {
                    $compararEvento = $this->compararService->comparar($value['eventos'], $inicio, $fin, $data['fecha']);
                    if ($compararEvento != "true") {
                        return Response::json($this->compararService->message($compararEvento->fecha, $compararEvento->hora_inicio, $compararEvento->hora_fin, " la filo de " , $value['invitadosNombre'][0]->name));
                    }
                }
            }

        }
        return true;
    }

}
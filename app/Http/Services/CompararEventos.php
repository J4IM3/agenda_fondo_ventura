<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 04/06/2019
 * Time: 02:11 PM
 */
namespace App\Http\Services;

use App\Http\Entities\Evento_participantes;

class CompararEventos{

    /**
     * @var CompararService
     */
    private $compararService;

    public function __construct(CompararService $compararService)
    {
        $this->compararService = $compararService;
    }

    public function compararEventos($data,$inicio,$fin)
    {
        foreach ($data['invitado_id'] as $item){
            $evento = Evento_participantes::with('eventos','invitadosNombre')->where('invitado_id',$item)->first();
            if (!empty($evento)){
                $compararEvento = $this->compararService->comparar($evento['eventos'],$inicio,$fin,$data['fecha'],"la filo");
                if ($compararEvento != "true" ){
                    return $this->compararService->message($compararEvento->fecha,$compararEvento->hora_inicio,$compararEvento->hora_fin," la filo de ".$evento['invitadosNombre'][0]->name);
                }
            }
            return true;
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-04-09
 * Time: 00:32
 */

namespace App\Http\Services;


use App\Http\Entities\Filoando;
use App\Http\Entities\FiloandoInvitados;
use App\Http\Entities\Invitado;

class ComparaFiloando
{
    /**
     * @var CompararService
     */
    private $compararService;

    public  function __construct(CompararService $compararService)
    {
        $this->compararService = $compararService;
    }

    public function comparaFiloando($id_invitados,$fecha,$hora_inicio,$hora_fin,$evento_id)
    {
        foreach ($id_invitados as $id)
        {
            $filoandoPartipantes = FiloandoInvitados::where('invitado_id', $id)
                ->where('evento_id','<>',$evento_id)
                ->get();
            foreach ($filoandoPartipantes as $item) {
                $filoando = Filoando::with('instituciones')
                    ->where('id', $item->evento_id)
                    ->first();
                if ($fecha == $filoando->fecha)
                {
                    $comparar = $this->compararService->compararHorario(
                                $this->compararService->convertir($hora_inicio),
                                $this->compararService->convertir($hora_fin),
                                $this->compararService->convertir($filoando->hora_traslado_ida),
                                $this->compararService->convertir($filoando->hora_traslado_regreso));

                    if ($comparar == "true") {
                        $invitado = Invitado::where('id', $id)->first();
                        return ['success' => 'error', 'msg' => $invitado->name . " tiene un evento en la escuela " . $filoando->instituciones->nombre. " el día " . $filoando->fecha . " de " . $filoando->hora_traslado_ida . " a " . $filoando->hora_traslado_regreso];
                    }
                }
            }
        }
        return "false";
    }
}
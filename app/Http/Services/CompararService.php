<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 04/06/2019
 * Time: 02:11 PM
 */
namespace App\Http\Services;

class CompararService{


    public function comparar($items,$inicio,$fin,$fecha)
    {
        foreach ($items as $item){

            if ($fecha == $item->fecha){
                if(in_array($this->convertir($item->hora_inicio), range($inicio,$fin))){
                    return $item;
                }
                if(in_array($inicio, range($this->convertir($item->hora_inicio), $this->convertir($item->hora_fin)))
                    || in_array($fin, range($this->convertir($item->hora_inicio), $this->convertir($item->hora_fin)))){
                    return $item;
                }
            }
        }
        return "true";
    }

    public function compararFiloando($items,$inicio,$fin,$fecha,$tipo)
    {
        foreach ($items as $item){
            if ($fecha == $item->fecha){
                if(in_array($this->convertir($item->hora_traslado_ida), range($inicio,$fin))){
                    return $item;
                }
                if(in_array($inicio, range($this->convertir($item->hora_traslado_ida), $this->convertir($item->hora_traslado_regreso)))
                    || in_array($fin, range($this->convertir($item->hora_traslado_ida), $this->convertir($item->hora_traslado_regreso)))){
                    return $item;
                }
            }

        }
        return "true";
    }

    public function compararHoras($hora_inicio,$hora_fin){
        if ($this->convertir($hora_fin)<= $this->convertir($hora_inicio)){
            return ['success'=>'error','msg'=>'La hora final no puede ser mayor,igual a la hora inicial'];
        }
        return true;
    }


    public function convertirSumaMin($data,$trayecyo){
        $hora_inicio_evento = strtotime($data);
        $hora_inicio_trayecto = strtotime('+'. $trayecyo .'minutes');
        return $hora_inicio_trayecto + $hora_inicio_evento;
    }


    public function message($fecha,$hora_inicio,$hora_fin,$tipo){
        return ['success'=>'error','msg'=>'Existe un evento de '.$tipo." el dia ".$fecha." de las ".$hora_inicio. " a ".$hora_fin];
    }

    //todo



    public function convertir($data){
        return strtotime($data);
    }

    public function convertirSumarTiempo($data)
    {
        return strtotime ( '+45 minute',$data);
    }


    /**
     * Compara los horarios de un evento dado con eventos existentes
     */
    public function compararHorario($horaEventoInicial,$horaEventoFinal,$horarInicioComparar,$horaFinComparar)
    {
        //return $horarInicioComparar;
        if($horaEventoInicial>= $horarInicioComparar && $horaEventoInicial <=$horaFinComparar)
        {
            return "true";
        } elseif($horaEventoFinal >= $horarInicioComparar && $horaEventoFinal <=$horaFinComparar)
        {
            return "true";
        }
        if ($horaEventoInicial <= $horarInicioComparar and $horaEventoFinal >= $horarInicioComparar)
        {
            if ($horaEventoFinal >= $horarInicioComparar || $horaEventoFinal <= $horaFinComparar)
            {
                return "true";
            }

        }
        return "false";

    }

    public function compararHorario2($horaEventoInicial,$horaEventoFinal,$horarInicioComparar,$horaFinComparar)
    {
        //return $horarInicioComparar;
        if($horaEventoInicial>= $horarInicioComparar && $horaEventoInicial <$horaFinComparar)
        {
            return "true";
        } elseif($horaEventoFinal > $horarInicioComparar && $horaEventoFinal <$horaFinComparar)
        {
            return "true";
        }
        if ($horaEventoInicial < $horarInicioComparar and $horaEventoFinal > $horarInicioComparar)
        {
            if ($horaEventoFinal > $horarInicioComparar || $horaEventoFinal < $horaFinComparar)
            {
                return "true";
            }

        }
        return "false";
    }

    public function comparaMultiplesHoras($hora_traslado_ida,$hora_inicio_evento,$hora_fin_evento,$hora_traslado_regreso)
    {
        if ($hora_traslado_ida >= $hora_inicio_evento) {
            return ['success' => 'error','msg' => "La hora inicial del evento no puede ser menor o igual a la hora inical de traslado"];
        }elseif($hora_inicio_evento <= $hora_traslado_ida  ){
            return ['success' => 'error','msg' => "La hora inicial del evento no puede ser menor  o igual a la hora inical de traslado"];
        }

        if ($hora_traslado_ida >= $hora_fin_evento ) {
            return ['success' => 'error', 'msg' => 'La hora de traslado ida no puede ser mayor o igual a la hora final del evento'];
        }elseif ($hora_fin_evento <= $hora_traslado_ida)
        {
            return ['success' => 'error', 'msg' => 'La hora final del evento no puede ser menor o igual a la hora inicial de traslado'];
        }

        if ( $hora_traslado_ida >= $hora_traslado_regreso) {
            return ['success' => 'error', 'msg' => 'La hora de traslado ida no puede ser mayor a la hora traslado regreso'];
        }elseif ($hora_traslado_regreso <= $hora_traslado_ida)
        {
            return ['success' => 'error', 'msg' => 'La hora de traslado regreso no puede ser menor a la hora traslado ida'];
        }

        if ($hora_inicio_evento >=$hora_fin_evento){
            return ['success'=>'error', 'msg' => 'La hora inicial del evento no puede ser mayor a la hora final de este.'];
        }
        elseif ($hora_fin_evento <= $hora_inicio_evento)
        {
            return ['success'=>'error', 'msg' => 'La hora final del evento no puede ser menor o igual a la hora inicial de este'];
        }

        if ($hora_inicio_evento >= $hora_traslado_regreso) {
            return ['success'=>'error', 'msg' => 'La hora inicial del evento no puede ser mayor o igual a la hora del traslado regresos'];
        }elseif ($hora_traslado_regreso <= $hora_inicio_evento){
            return ['success'=>'error', 'msg' => 'La hora del traslado de regreso no puede ser menor o igual a la hora del inicio del evento'];
        }

        if ($hora_fin_evento >= $hora_traslado_regreso)
        {
            return ['success'=>'error', 'msg' => 'La hora del fin del evento no puede ser mayor o igual a la hora del traslado de regreso'];
        }elseif ($hora_traslado_regreso <= $hora_fin_evento)
        {
            return ['success'=>'error', 'msg' => 'La hora del traslado de regreso no puede ser menor o igual a la hora en la que finaliza el evento'];
        }
        return "true";

    }



}

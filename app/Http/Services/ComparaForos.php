<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-04-13
 * Time: 15:38
 */

namespace App\Http\Services;
use App\Http\Entities\DetailEvents;
use App\Http\Entities\Eventos;
use App\Http\Entities\Foro;


class ComparaForos
{
    /**
     * @var CompararService
     */
    private $compararService;

    public function __construct(CompararService $compararService)
    {
        $this->compararService = $compararService;
    }

    public function compararEvento($foro_id,$fecha,$hora_inicio,$hora_fin,$value)
    {
        /*$eventosForos = Eventos::where('foro_id',$foro_id)
            ->where('fecha',$fecha)
            ->where('id','<>',$value)
            ->get();*/
        $eventosForos = DetailEvents::whereIn('sede_id',$foro_id)
            ->where('fecha',$fecha)
            ->where('event_id','<>',$value)
            ->get();

        $foro = Foro::where('id',$foro_id)->first();

        if (!empty($eventosForos))
        {
            foreach ($eventosForos as $eventos)
            {
                $comparar = $this->compararService->compararHorario2(
                    $this->compararService->convertir($hora_inicio),
                    $this->compararService->convertir($hora_fin),
                    $this->compararService->convertir($eventos->hora_inicio),
                    $this->compararService->convertir($eventos->hora_fin));

                if ($comparar == "true")
                {
                    $evento = Eventos::where('id',$eventos->event_id)->first();
                    return ['success' => 'error','msg'=> "Este foro cuenta con el evento ' ".$evento->nombre_evento." ' de ".$evento->hora_inicio. " a ".$evento->hora_fin];
                }

                $horario_final_evento = strtotime ( $eventos->hora_fin);
                $horario_final_limpieza = strtotime ( '+45 minute',$horario_final_evento) ;
                /*$eventos->hora_fin." - ".date("H:i",$horario_final_limpieza);
                $hora_inicio." - ".$hora_fin;*/

                if($foro->type == 'Presencial')
                {
                    /*$horaFinalEvento = strtotime($hora_fin);
                $horaFinalEventoNuevo = strtotime ( '+30 minute',$horaFinalEvento) ;*/
                    $disponiblidadEscenario = $this->compararService->compararHorario2(
                        $this->compararService->convertir( $eventos->hora_fin),
                        $horario_final_limpieza,
                        $this->compararService->convertir($hora_inicio),
                        $this->compararService->convertir($hora_fin));
                    if ($disponiblidadEscenario == "true")
                    {
                        return ['success' => 'error','msg'=> "Tiempo para desalojar asistentes del evento: ' ".$eventos->nombre_evento." ' de ".$eventos->hora_inicio. " a ".$eventos->hora_fin. ". Disponible a partir de las ".date("H:i",$horario_final_limpieza)];
                    }


                    $time = strtotime($hora_fin);
                    $horarioFinEventoNuevo = strtotime('+45 minutes', $time);
                    //return date("H:i",$horarioFinEventoNuevo);
                    //return $startTime = date("H:i", strtotime('+30 minutes', $time));


                    $disponiblidadEscenario2 = $this->compararService->compararHorario2(
                        $this->compararService->convertir( $eventos->hora_inicio),
                        $horario_final_limpieza,
                        $this->compararService->convertir($hora_inicio),
                        $horarioFinEventoNuevo);

                    if ($disponiblidadEscenario2 == "true")
                    {
                        return ['success' => 'error','msg'=> "Este evento necesita 30 minutos posteriores para desalojar asistentes en el foro ".$foro->nombre.", el siguiente evento inicia a las:  ".$eventos->hora_inicio];
                    }
                }
            }
        }
        return "false";
    }

}

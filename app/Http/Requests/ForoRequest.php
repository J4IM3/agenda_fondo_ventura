<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ForoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:100',
            'tipo' => 'required|max:11',
            'sede_id' => 'required|max:3',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Este campo es obligatorio',
            'nombre.max' => 'Se aceptan maximo 100 caracteres',
            'tipo.required' =>  'Este campo es obligatorio',
            'tipo.max' => 'Se aceptan maximo 100 caracteres',
            'sede_id.required' =>  'Este campo es obligatorio',
            'sede_id.max' => 'Se aceptan maximo 3 caracteres',
        ];
    }
}

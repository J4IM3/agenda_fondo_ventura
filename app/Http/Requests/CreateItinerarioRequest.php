<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateItinerarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invitado_id' => 'required',
            'origen' => 'required',
            'fecha_llegada' => 'required',
            'fecha_salida' => 'required',
            'hotel' => 'required',
            'llegada_estancia' => 'required',
            'salida_estancia' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'invitado_id.required' => 'El campo invitado no puede estar vacio',
            'origen.required' => 'Este campo no puede estar vacío',
            'fecha_llegada.required' => 'El campo fecha llegada no puede estar vacio',
            'fecha_salida.required' => 'El campo fecha salida no puede estar vacio',
            'hotel.required' => 'El campo hotel no puede estar vacio',
            'llegada_estancia.required' => 'Este campo no puede estar vacío',
            'salida_estancia.required' => 'Este campo no puede estar vacío',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventoEditarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fecha'=> 'required',
            'hora_inicio'=>'required',
            'hora_fin'=>'required',
            'nombre_evento'=>'required',
            'descripcion'=>'required|max:180',
            'tipo_id'=>'required',
            'programacion_id'=>'required',
            'foro_id'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'tipo_id.required' => "El campo tipo es obligatorio.",
            'programacion_id.required' => "El campo programación es obligatorio.",
            'foro_id.required' => "El campo foro es obligatorio.",
            'descripcion.required' => "El campo descripción es obligatorio.",
            'descripcion.max' => "La descripción debe ser igual o menor a 180 caracteres"
        ];
    }
}

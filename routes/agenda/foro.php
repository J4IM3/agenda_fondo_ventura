<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 24/07/2019
 * Time: 11:49 AM
 */
Route::get('foro','ForoController@index')->name('foro');
Route::get('get-data-foros-ajax','ForoController@getDataByAjax')->name('get-data-foros-ajax');
Route::post('create-foro','ForoController@create')->name('create-foro');
Route::post('edit-foro','ForoController@edit')->name('edit-foro');
Route::post('update-status','ForoController@changeStatus')->name('update-status');

<?php
/**
 * Created by PhpStorm.
 * User: jsanchez
 * Date: 8/1/19
 * Time: 2:05 AM
 */
Route::get('evento_prensa','PrensaTiposController@index')->name('evento_prensa');
Route::get('get-data-tipos-prensa-ajax','PrensaTiposController@getTiposPrensa')->name('get-data-tipos-prensa-ajax');
Route::post('crear-tipo','PrensaTiposController@save')->name('crear-tipo');
Route::post('editar-tipo','PrensaTiposController@editar')->name('editar-tipo');
Route::POST('change-status-tipo','PrensaTiposController@changeStatus');

<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 03/06/2019
 * Time: 04:54 PM
 */
Route::get('users','UsersController@index')->name('users');
Route::get('get-table-users-ajax','UsersController@getUsersByAjax')->name('get-table-users-ajax');
Route::post('save-user','UsersController@save')->name('save-user');
Route::post('update-user','UsersController@edit')->name('update-user');
Route::post('change-status','UsersController@changeStatus')->name('change-status');
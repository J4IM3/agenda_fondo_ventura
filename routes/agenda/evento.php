<?php
/**
 * Created by PhpStorm.
 * User: jsanchez
 * Date: 6/9/19
 * Time: 12:14 AM
 */
Route::get('evento','EventoController@index')->name('evento');
Route::post('save-event','EventoController@save')->name('save-event');
Route::get('get-table-eventos-ajax','EventoController@getDatabyAjax')->name('get-table-eventos-ajax');
Route::post('update-event','EventoController@update')->name('update-event');
Route::post('delete-event','EventoController@delete')->name('delete-event ');
Route::post('editar-evento','EventoController@editar')->name('editar-evento');
Route::post('create-sount-test','EventoController@createSoundTest')->name('create-sount-test');
Route::post('get-sede-by-type','EventoController@getSedeByType')->name('get-sede-by-type');

//Libreria


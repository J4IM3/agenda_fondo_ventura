<?php
/**
 * Created by PhpStorm.
 * User: jsanchez
 * Date: 6/5/19
 * Time: 6:08 PM
 */

Route::get('itinerario','ItinerarioController@index')->name('itinerario');
Route::post('save-itinerario','ItinerarioController@save')->name('save-itinerario');
Route::post('update-itinerario','ItinerarioController@update')->name('update-itinerario');
Route::post('add-honorarios','ItinerarioController@addHonorarios')->name('add-honorarios');
Route::get('generar-programa','ItinerarioController@generarPrograma')->name('generar-programa');

<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 01/07/2019
 * Time: 05:23 PM
 */
Route::get('filoando','FiloandoController@index')->name('filoando');
Route::post('create-filoando','FiloandoController@create')->name('create-filoando');
Route::get('get-data-filoando-ajax','FiloandoController@getDataFiloando')->name('get-data-filoando-ajax');
Route::post('update-filoando','FiloandoController@update')->name('update-filoando');
Route::post('delete-filoando','FiloandoController@delete')->name('delete-filoando');
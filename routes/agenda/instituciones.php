<?php
Route::get('instituciones','InstitucionesController@index')->name('instituciones');
Route::get('get-data-instituciones-ajax','InstitucionesController@getDataInstituciones')->name('get-data-instituciones-ajax');
Route::post('nueva-institucion','InstitucionesController@create')->name('nueva-institucion');
Route::post('editar-institucion','InstitucionesController@update')->name('editar-institucion');
Route::post('change-status-instituciones','InstitucionesController@changeStatus')->name('change-status-instituciones');

<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 12/06/2019
 * Time: 01:22 PM
 */
Route::get('prensa','PrensaController@index')->name('prensa');
Route::post('save-interview','PrensaController@save')->name('save-interview');
Route::get('get-table-prensa-ajax','PrensaController@getPrensaAjax')->name('get-table-prensa-ajax');
Route::post('delete-event-press','PrensaController@delete')->name('delete-event-press');
Route::post('update-interview','PrensaController@update')->name('update-interview');

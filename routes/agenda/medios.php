<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 05/08/2019
 * Time: 06:35 PM
 */
Route::get('medios','MediosController@index')->name('medios');
Route::get('get-data-medios-ajax','MediosController@getData')->name('get-data-medios-ajax');
Route::post('create-medio','MediosController@create')->name('create-medio');
Route::post('editar-medio','MediosController@editar')->name('editar-medio');
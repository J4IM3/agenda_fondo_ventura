<?php
/**
 * Created by PhpStorm.
 * User: jsanchez
 * Date: 6/5/19
 * Time: 12:39 AM
 */
Route::get('aerolineas','AerolineasController@index')->name('aerolineas');
Route::post('save-aerolinea','AerolineasController@save')->name('save-aerolinea');
Route::get('get-table-aerolinea-ajax','AerolineasController@getDataByAjax')->name('get-table-aerolinea-ajax');
Route::post('change-status','AerolineasController@changeStatus')->name('change-status');
Route::post('update-aerolinea','AerolineasController@update')->name('update-aerolinea');

<?php
Route::GET('sedes','SedesController@index')->name('sedes');
Route::GET('get-data-sedes-ajax','SedesController@getData')->name('get-data-sedes-ajax');
Route::POST('add-sede','SedesController@add')->name('add-sede');
Route::POST('edit-sede','SedesController@edit')->name('edit-sede');
Route::POST('change-status-sede','SedesController@changeStatus')->name('change-status-sede');

<?php
/**
 * Created by PhpStorm.
 * User: jsanchez
 * Date: 7/24/19
 * Time: 5:21 AM
 */
Route::get('tipo','TipoController@index')->name('tipo');
Route::get('get-data-tipos-ajax','TipoController@geTipoByAjax')->name('get-data-tipos-ajax');
Route::post('add-tipo','TipoController@create')->name('add-tipo');
Route::post('edit-tipo','TipoController@edit')->name('edit-tipo');
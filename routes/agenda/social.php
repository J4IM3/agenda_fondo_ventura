<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 12/06/2019
 * Time: 01:22 PM
 */
Route::get('social','SocialController@index')->name('social');
Route::get('get-table-social-ajax','SocialController@getSocialByAjax')->name('get-table-social-ajax');
Route::post('save-data-social','SocialController@save')->name('save-data-social');
Route::post('update-data-social','SocialController@update')->name('update-data-social');
Route::post('delete-event-social','SocialController@delete')->name('delete-event-social');


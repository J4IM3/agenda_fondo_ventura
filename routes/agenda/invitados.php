<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 04/06/2019
 * Time: 06:06 PM
 */
Route::get('invitados','InvitadosController@index')->name('invitados');
Route::post('save-invitado','InvitadosController@save')->name('save-invitado');
Route::get('get-table-invitado-ajax','InvitadosController@getInvitadoByAjax')->name('get-table-invitado-ajax');
Route::get('get-table-invitado-itinerario-ajax','InvitadosController@getInvitadoItinerarioAjax')->name('get-table-invitado-itinerario-ajax');
Route::post('update-invitado','InvitadosController@update')->name('update-invitado');
Route::post('get-data-guests','InvitadosController@getDataGuests')->name('get-data-guests');
Route::post('delete-guest','InvitadosController@delete')->name('delete-guest');
Route::get('download/{id}','InvitadosController@download')->name('download');
Route::post('delete-image','InvitadosController@deleteImage')->name('delete-image');
Route::post('find-email-or-phone','InvitadosController@findEmailOrPhone')->name('find-email-or-phone');

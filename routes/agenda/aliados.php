<?php
Route::get('aliados','AliadosController@index')->name('aliados');
Route::get('get-table-aliados-ajax','AliadosController@getDataByAjax')->name('get-table-aliados-ajax');
Route::post('save-aliado','AliadosController@save')->name('save-aliado');
Route::post('edit-aliado','AliadosController@edit')->name('edit-aliado');

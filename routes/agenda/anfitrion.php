<?php
/**
 * Created by PhpStorm.
 * User: jsanchez
 * Date: 7/21/19
 * Time: 12:15 AM
 */
Route::get('anfitrion','AnfitrionController@index')->name('anfitrion');
Route::get('get-table-anfitrion-ajax','AnfitrionController@getDataByAjax')->name('get-table-anfitrion-ajax');
Route::post('save-anfitrion','AnfitrionController@save')->name('save-anfitrion');
Route::post('update-anfitrion','AnfitrionController@update')->name('update-anfitrion');

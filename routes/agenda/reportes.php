<?php
Route::get('reportes','ReportesController@index')->name('reportes');
Route::post('reporte-programacion','ReportesController@reporteProgramacion')->name('reporte-programacion');
Route::get('download-semblance/{id}','ReportesController@downloadSemblance')->name('download-semblance');
Route::get('reporte-invitados-programacion','ReportesController@invitadosProgramacion')->name('reporte-invitados-programacion');
Route::get('reporte-foro','ReportesController@getForos')->name('reporte-foro');
Route::post('reporte-foro-fecha','ReportesController@reporteForoFecha')->name('reporte-foro-fecha');
Route::post('reporte-por-foro','ReportesController@reportePorForo')->name('reporte-por-foro');
Route::post('reporte-todos-foro','ReportesController@reporteForos')->name('reporte-todos-foro');
Route::get('reporte-requerimientos-actividad','ReportesController@requerimientosActividad')->name('reporte-requerimientos-actividad');
Route::post('reporte-actividades-fecha','ReportesController@reporteActividadesPorFecha')->name('reporte-actividades-fecha');
Route::POST('reporte-actividades-foro','ReportesController@reportePorForoRequerimientos')->name('reporte-actividades-foro');
Route::GET('reporte-eventos','ReportesController@reporteEventos')->name('reporte-eventos');
Route::get('libros-presentadores','ReportesController@librosPresentadores')->name('libros-presentadores');

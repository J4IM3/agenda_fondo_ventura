<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware' => 'auth'], function() {

    Route::get('index','HomeController@index')->name('index');
    Route::get('/','HomeController@index')->name('index');

    require_once "agenda/users.php";
    require_once "agenda/invitados.php";
    require_once "agenda/aerolienas.php";
    require_once "agenda/itinerario.php";
    require_once "agenda/evento.php";
    require_once "agenda/prensa.php";
    require_once "agenda/social.php";
    require_once "agenda/filoando.php";
    require_once "agenda/aliados.php";
    require_once "agenda/anfitrion.php";
    require_once "agenda/tipo.php";
    require_once "agenda/foro.php";
    require_once "agenda/prensa_tipos.php";
    require_once "agenda/instituciones.php";
    require_once "agenda/medios.php";
    require_once "agenda/reportes.php";
    require_once "agenda/sedes.php";

});

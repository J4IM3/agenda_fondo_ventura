$(document).ready(function (){
    let icon,properties_status = "";
    $("#table-sedes").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-data-sedes-ajax',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'direction', name: 'direction'},
        ],
        "columnDefs": [{
            "targets": 3,
            "data": "download_link",
            "render": function (data, type, row, meta) {

                properties_status = getIconForStatus(row.status);
                return '<a href="#" class="btn btn-info edit-sede" data-val="' + row + '"    data-toggle="tooltip" title="Editar sede"><span class="far fa-edit"></span></span></a> ' +
                '<a href="#" class="btn btn-'+ properties_status.color+' update-status-sede" data-val="' + row.id + '"    data-toggle="tooltip" title="Cambiar status">' +
                    '<span class="'+ properties_status.icon+'"></span></span>' +
                    '</a> '
            }
        }
        ]
    });

    $(".new-sede").on('click',function (e){
        e.preventDefault();
        $("#modal-sede").modal('show');
        $(".item-action").val('add');
        $('#form-sede')[0].reset();
    })

    $("#table-sedes tbody").on('click','.edit-sede',function (e){
        e.preventDefault();
        let sede_table = $("#table-sedes").DataTable();
        let rowData = sede_table.row( $(this).parents('tr') ).data();
        $.each(rowData,function (index,element){
           $(".item-"+index).val(element);
        });
        $("#modal-sede").modal('show');
        $(".item-action").val('edit');
    })

    $("#table-sedes tbody").on('click','.update-status-sede',function (e){
        data = new FormData();
        data.append('id',$(this).data('val'));
        $("#loadingGifFV").addClass("loading-fv");
        sendData(data,'change-status-sede')
    });

    $(".form-sede").on('submit',function (e) {
        e.preventDefault();
        let data = new FormData(this);
        let route =  $(".item-action").val() == "add" ? "add-sede" : "edit-sede";
        $("#loadingGifFV").addClass("loading-fv");
        clearSpan();
        sendData(data,route);
    });

    function sendData(data,route)
    {
        $.ajax({
            url: route,
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                $("#loadingGifFV").removeClass("loading-fv")
                modalEvent(data.success, data.msg);
                if (data.success == "success") {
                    registerSuccess();
                }
            },
            error: function(errors) {
                $("#loadingGifFV").removeClass("loading-fv")
                var indices = errors.responseJSON.errors;
                $.each(indices, function(index, value) {
                    $(".item-error-" + index).append(value);
                    $(".item-error-" + index).show();
                });

            }
        });
    }

    function registerSuccess()
    {
        $("#modal-sede").modal('hide');
        $("#table-sedes").DataTable().ajax.reload();
    }

    function clearSpan()
    {
        $(".item-error-name").html("");
        $(".item-error-direction").html("");
    }

    function getIconForStatus(status)
    {
        let icon  = status == "1" ? "fas fa-thumbs-up" : "fas fa-thumbs-down";
        let color = status == "1" ? "success" : "danger";
        return {'icon' : icon,'color':color};
    }

});

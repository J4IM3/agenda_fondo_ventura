$(document).ready(function() {
    $('.internacional').hide();
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        startDate: '-3d'
    });

    $('.form-control-chosen').chosen();
    /*
    $(".show-items-internacional").on('change',function (e) {
        e.preventDefault();

        $('.internacional').show();
    })
    */
    $('input[type="checkbox"]').click(function() {
        if ($(this).is(":checked")) {
            $('.internacional').show();

        } else if ($(this).is(":not(:checked)")) {
            $('.internacional').hide();
        }
    });


    $(".table_itinerarios").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-table-invitado-itinerario-ajax',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
        ],
        "columnDefs": [{
            "targets": 2,
            "data": "download_link",
            "render": function(data, type, row, meta) {
                console.log(row);
                return '<a href="#" class="btn btn-info show-detail-hospedaje" data-val="' + row + '"    data-toggle="tooltip" title="Mostrar detalles vuelo-alojamiento"><span class="fas fa-plane"></span></a> ' +
                    '<a href="#" class="btn btn-info edit-hospedaje" data-val="' + row + '"    data-toggle="tooltip" title="Editar usuario"><span class="far fa-edit"></span></a> '+
                    '<a href="#" class="btn btn-info add-honorarios" data-id ="'+ row.id +'" data-val="' + row + '"    data-toggle="tooltip" title="Agregar honorarios"><i class="fas fa-plus-square"></i></a> '

            }
        }]
    });



    $('.add-itinerario').on('click', function() {
        $(".form-new-itinerario")[0].reset();
        btnLoadingdeactivate();
        $(".chosen").val('').trigger("chosen:updated");
        $(".item-anfitrion_id").trigger("chosen:updated");
        $(".item-invitado_id").trigger("chosen:updated");

        $("#modal-nuevo-itinerario").modal('show');
    });

    $(".form-new-itinerario").on("submit", function(e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "save-itinerario";
        method = "POST";
        btnLoadingActivate();
        sendDataItinerario(ruta, method, data, "#modal-nuevo-itinerario");
    });

    $("#form-edit-itinerario").on('submit', function(e) {
        e.preventDefault();
        btnLoadingActivate();
        var data = new FormData(this);
        ruta = "update-itinerario";
        method = "POST";
        sendDataItinerario(ruta, method, data, "#modal-edit-itinerario");
    })


    /**
     * Editar itinerario
     */

    $(".table_itinerarios tbody").on('click', '.edit-hospedaje', function(e) {
        e.preventDefault();
        $("#modal-edit-itinerario").modal('show');
        btnLoadingdeactivate();
        var itinerarios_table = $(".table_itinerarios").DataTable();
        var rowData = itinerarios_table.row($(this).parents('tr')).data();

        $.each(rowData,function (index,item) {
            $(".item-" + index).val(item).trigger("chosen:updated");
        });

        $.each(rowData.itinerario2[0], function(index, value) {
            console.log(index,value);
            $(".item-" + index).val(value);
            $(".item-" + index).val(value).trigger("chosen:updated");

            if (index == "hotel") {
                $(".item-" + index).val(value[0].id).trigger("chosen:updated");
                console.log(".item-" + index, value[0].id);
            }
        });

        var aerolinea_inter = $(".item-aerolinea_internacional_id").val();
        if (aerolinea_inter != "" && aerolinea_inter != null)
        {
            $('.show-items-internacional').prop("checked", true);
            $('.internacional').show();
        }
        else
            {
            $('.show-items-internacional').prop('checked', false);
            $('.internacional').hide();
            }


    });

    /**
     * Muestra modal para agregar honorarios a un invitado
     */

    $(".table_itinerarios tbody").on('click','.add-honorarios',function (e) {
        e.preventDefault();

        $(".item-invitado_id").val($(this).data('id'));
        $("#modal-create-honorarios").modal('show');

        $(".form-new-honorarios")[0].reset();

        var itinerarios_table = $(".table_itinerarios").DataTable();
        var rowData = itinerarios_table.row($(this).parents('tr')).data();
        /*
        $.each(rowData.itinerario[0]['honorarios'][0], function(index, value) {
            $(".item-"+index).val(value);

        });
        */
        $.each(rowData.presupuesto[0], function(index, value) {
            $(".item-"+index).val(value);
        });
    });


    $(".table_itinerarios tbody").on('click','.show-detail-hospedaje', function(e) {
        e.preventDefault();
        console.log("Aquii");
         var itinerarios_table = $(".table_itinerarios").DataTable();
         var rowData = itinerarios_table.row($(this).parents('tr')).data();
        $(".items-aerolineas_inter").empty();

         $.each(rowData,function (index,value) {
             console.log(index,value);
             $(".items-"+index).html(value);

             if (index == "anfitrion"  )
             {
                 $.each(value, function (element,item) {
                     $(".items-"+element).html(item);
                 })
             }
            });
        $.each(rowData,function (element,item) {
            if (element == "itinerario2"){

                if(item[0].aerolineas_inter !== null)
                {
                    $(".items-aerolineas_inter").html(item[0].aerolineas_inter.name);
                }
                if (item[0].aerolineas != null)
                {
                    $(".items-aerolineas").html(item[0].aerolineas.name);
                }

                if ( item[0].hotel !== null )
                {
                    console.log("Holi"+item[0].hotel[0]);
                    $(".items-hotel-name").html(item[0].hotel[0].name);
                }
            }
            });
            //$(".items-aerolineas").html(item.aerolineas[0].name);
        $("#modal-details-itinerario").modal('show');
    });


    /**
     * Envia la funcion sendDataItinerario las cantidades de
     * la modal presupuesto
     */

    $(".form-new-honorarios").on("submit",function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "add-honorarios";
        method = "POST";
        sendDataItinerario(ruta, method, data, "#modal-create-honorarios");

    });


    /**
     * Hace el envio de los parametros al crear un invitado desde la vista del itinerario
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */
    function sendDataItinerario(ruta, method, data, nombreModal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.success == "error") {
                    modalEventPrensa(data.success, data.msg);
                    btnLoadingdeactivate();
                    return;
                }
                btnLoadingdeactivate();
                modalEvent(data.success, data.msg);
                $(nombreModal).modal('hide');
                console.log("ok")
                //$(".table_itinerarios").DataTable().ajax.reload();
            },
            error: function(errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function(index, value) {
                    btnLoadingdeactivate()
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }


    $(document).on('keyup', '.chosen-search-input', function (e) {
        if(e.keyCode == 13 && $(this).val().length > 0) {
            $('.input-anfitrion-add').append('<option value="'+$(this).val()+'" selected>'+$(this).val()+'</option>');
            $('.input-anfitrion-add').trigger("chosen:updated");
        }
    });

    function btnLoadingActivate() {
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span class="spinner-grow spinner-grow-sm " role="status" aria-hidden="true"></span> Espere un momento...'
        );
    }

    function btnLoadingdeactivate() {
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span></span> Guardar')

    }


    /**
     * Saca la diferencia entre el dia de llegada y el dia de salida
     * Y actualiza el campo estancia con el numero de dias que permanecera el invitado.

    $(".item-llegada_estancia").on('change',function (e) {
        e.preventDefault();
        $(".item-estancia").val();

        var llegada_estancia = $(".item-salida_estancia").val();

        if(isNaN(llegada_estancia)) {
            $(".item-salida_estancia").val(null);
        }
        if($(".item-llegada_estancia").val() != null &&  $(".item-salida_estancia").val() != null)
        {
            console.log($(".item-salida_estancia").val());
            var fecha_llegada = $(".item-llegada_estancia").val();
            var fecha_salida = $(".item-salida_estancia").val();
            console.log(fecha_llegada+" - "+ fecha_salida)
            if (fecha_llegada > fecha_salida )
            {
                modalDate("La fecha de salida debe ser mayor a la fecha de llegada");
            }
            var fechaInicio = new Date(fecha_llegada).getTime();
            var fechaFin    = new Date(fecha_salida).getTime();

            var diff = fechaFin - fechaInicio;
            console.log(diff/(1000*60*60*24) );
            $(".item-estancia").val( diff/(1000*60*60*24) );
        }
    })
     */
});


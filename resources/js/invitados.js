$(document).ready(function () {
    $(".btn-delete").hide();
    var avatar_img = "avatar/guest.png";
    $("#table_invitado").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive:true,
        ajax:'get-table-invitado-ajax',
        columns:[
            {data: 'id', name:'id'},
            {data: 'name', name:'name'},
        ],
        "columnDefs": [{
            "targets": 2,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {
                console.log(row);
                    return '<a href="#" class="btn btn-info edit-invitado" data-val="'+ row +'"    data-toggle="tooltip" title="Editar usuario"><span class="far fa-edit"></span></span></a> '+
                        '<a href="#" class="btn btn-success show-details-itenerario" data-val="'+ row.id +'"    data-toggle="tooltip" title="Ver itinerario"><i class="fas fa-list-ul"></i></a> '+
                        '<a href="#" class="btn btn-success download-semblance" data-val="'+ row.id +'"    data-toggle="tooltip" title="Descargar semblanza"><i class="fas fa-file-download"></i></a> '+
                        '<a href="#" class="btn btn-danger delete-guest"   data-id ="'+ row.id +'"  data-toggle="tooltip" title="Eliminar usuario"><i class="fas fa-user-slash"></i></a> '
            }}
        ]
    });


    $('.add-invitado').on('click',function () {
        $(".form-add-user").trigger("reset");
        $(".chosen").val('').trigger("chosen:updated");
        $(".section-password").removeClass("d-none");
        $('.section-password').find('input').prop('disabled',false);
        $('.item-sexo').trigger('chosen:updated');
        $('.item-residencia').trigger('chosen:updated');
        limpiarSpan();
        $(".avatarImg").attr("src",avatar_img);
        $("#modal-create-invitado").modal('show');
    });

    /**
     *  Envia los parametros de la modal para crear un nuevo invitado
     */
    $("#form-save-invitado").on("submit", function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "save-invitado";
        method = "POST";
        modal ="#modal-create-invitado";
        sendDataUser(ruta,method,data,modal);
        limpiarSpan();
    });


    /**
     * Hace el envio de los parametros al crear o editar un usuario para enviarlo al controlador
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */
    function sendDataUser(ruta,method,data,modal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if (data.success == "success")
                {
                    $(modal).modal('hide');
                    modalEvent(data.success, data.msg);
                    $("#table_invitado").DataTable().ajax.reload();
                }
                /*if (data.success != "deleteError") {
                    $(modal).modal('hide');
                    modalEvent(data.success, data.msg);
                    $("#table_invitado").DataTable().ajax.reload();
                }*/

                if (data.success == "error")
                {
                    modalEvent(data.success,data.msg)
                }
            },
            error: function (errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }

    /**
     * Muestra modal para editar un usuario
     */
    $('#table_invitado tbody').on( 'click', '.edit-invitado', function (e) {
        e.preventDefault();
        var invitado_table = $("#table_invitado").DataTable();
        var rowData = invitado_table.row( $(this).parents('tr') ).data();
        var id ;
        $("#modal-edit-invitado").modal('show');
        $(".section-password").addClass("d-none");
        $('.section-password').find('input').attr('disabled','disabled');
        $(".deleteImage").empty();


        $.each(rowData, function(index, element) {
            if(index == "sexo")
            {
                $(".item-sexo").val(element).trigger("chosen:updated");
            }

            if(index == "residencia")
            {
                $(".item-residencia").val(element).trigger("chosen:updated");
            }
            $(".item-"+index).val(element);
            if(index  == "id")
            {
                id = element;
            }
            if (index == "avatar")
            {
                if (element == " " || element == null)
                {
                    $(".avatarImg").attr("src",avatar_img);
                }else{
                    $('.deleteImage').
                        append($('<button>').
                        attr("class", "btn btn-danger btn-delete-image").
                        attr("type", "button").
                        attr("data-id", id).
                        text("Eliminar imagen")
                    );
                    $(".btn-delete").show();
                    $(".avatarImg").attr("src",'/avatar/'+element);
                }
            }
        });
    });


    /**
     * Envia los parametros de la modal editar invitado al metodo sendDataUSer que se encarga de enviarlos al controlador
     */
    $("#form-edit-invitado").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "update-invitado";
        method = "POST";
        modal ="#modal-edit-invitado";
        $('.avatarImg').removeAttr('src');
        sendDataUser(ruta,method,data,modal);
    });

    /**
     * Toma el id de invitado a eliminar su foto
     */
    $(document).on('click','.btn-delete-image',function (e) {
        e.preventDefault();
        var data = new FormData();
        data.append('id',$(this).data('id'));
        ruta = "delete-image";
        method = "POST";
        modal ="#modal-edit-invitado";
        $(".avatarImg").attr("src",avatar_img);
        $('.avatarImg').removeAttr('src');
        sendDataUser(ruta,method,data,modal);
    })

    $("#table_invitado tbody").on('click','.show-detail-hospedaje',function (e) {
        e.preventDefault();
        var invitados_table = $("#table_invitado").DataTable();
        var rowData = invitados_table.row( $(this).parents('tr') ).data();
        $("#modal-details-itinerario").modal('show');

        $.each(rowData.itinerario[0], function(index,value){
            $("#"+index).text(value)
            if(index == "aerolineas" || index == "hotel" )
            {
                $("#"+index).text(value[0].name)
            }
        });
    });

    $("#table_invitado tbody").on('click','.show-details-itenerario',function () {
        var id = $(this).data('val');
        window.location.href='/download/'+id+'';
    });

    /**
     * Envia al controlador reportes controller para descargar un archivo de word
     */

    $("#table_invitado tbody").on('click','.download-semblance',function () {
        var id = $(this).data('val');
        window.location.href='/download-semblance/'+id+'';
    });

    $("#table_invitado tbody").on('click','.delete-guest',function(){

        var data = new FormData();
        data.append('id',$(this).data('id'));
        var ruta = "delete-guest";
        var method="POST";

        swal({
            title: '¿Estas seguro?',
            text: "Este usuario sera eliminado",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if(result.value)
        {
            sendDataUser(ruta,method,data)
        }
    })
    });

    /**
     * Funcion que se encarga de traer los datos del hospedaje y vuelos del invitado
     * @param ruta
     * @param method
     * @param data
     */
    function getData(ruta,method,data) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $("#invitado-details").modal('show');
                var fila="<tr><td>"+_nom+"</td><td>"+_ape +"</td><td>"+_ced +"</td></tr>";

                var btn = document.createElement("TR");
                btn.innerHTML=fila;
                document.getElementById("show-details-guests").appendChild(btn);
            }
        });
    }

    $(".item-email").on('keyup',function (e){
       e.preventDefault();
       let data;
       let str = $(".item-email").val();
       $at = str.indexOf('@')
        if ($at > 0)
        {
            data = new FormData;
            data.append('field',"email")
            data.append('value',str)
            findEmailOrPhone(data,'email');
        }
    });

    $(".item-phone").on('keyup',function (e){
        e.preventDefault();
        data = new FormData;
        let phone = $(".item-phone").text();
        data.append('field',"phone")
        data.append('value',phone)
        $(".error-phone").text("");
        findEmailOrPhone(data,'phone');
    });


    function findEmailOrPhone(value,field)
    {
        $(".error-"+field).text("");
        $.ajax({
            url: "find-email-or-phone",
            type: "post",
            data: value,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                if (data.success == "error")
                {
                    $(".error-"+field).text(data.msg);
                }
            },
            error: function (errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }

    function limpiarSpan() {
        $("#error-name").text('');
        $("#error-slug").text('');
        $("#error-email").text('');
        $("#error-phone").text('');
    }

});

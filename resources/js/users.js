$(document).ready(function () {

    $("#table_users").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive:true,
        ajax:'get-table-users-ajax',
        columns:[
            {data: 'id', name:'id'},
            {data: 'name', name:'name'},
            {data: 'email', name:'email'},
        ],
        "columnDefs": [{
            "targets": 3,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {
                var icon , title,colorClas;
                if(row.status != 0 )
                {
                    icon = "far fa-check-circle";
                    title = "Desactivar";
                    colorClas ="primary";
                }else{
                    icon = "fas fa-times-circle";
                    title = "Activar";
                    colorClas = "danger";
                }
                return  '<a href="#" class="btn btn-info edit-user"    data-val="'+ row +'"    data-toggle="tooltip" title="Editar usuario"><span class="far fa-edit"></span></span></a> '+
                    '<a href="#" class="btn btn-'+ colorClas +' changeStatus"   data-id ="'+ row.id +'"  data-toggle="tooltip" title="'+ title +'"><span class="' + icon + '"></span></a> '
            }}
        ]
    });

    $('.new-user').on('click',function () {
         $(".form-add-user").trigger("reset");
         $(".section-password").removeClass("d-none");
         $('.section-password').find('input').prop('disabled',false);
         $("#modal-create-user").modal('show');
    });

    /**
     *  Envia los parametros de la modal para crear un nuevo usuario
     */
    $("#form-save-user").on("submit", function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "save-user";
        method = "POST";
        sendDataUser(ruta,method,data);
        $("#modal-create-user").modal('hide');
    });

    /**
     * Hace el envio de los parametros al crear o editar un usuario para enviarlo al controlador
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */
    function sendDataUser(ruta,method,data) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                if(data.success == "error")
                {
                    modalEvent(data.success, data.msg);
                    return ;
                }
                modalEvent(data.success, data.msg);
                $("#table_users").DataTable().ajax.reload();
            },
            error: function (errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }

    /**
     * Muestra modal para editar un usuario
     */
    $('#table_users tbody').on( 'click', '.edit-user', function (e) {
        e.preventDefault();
        var users_table = $("#table_users").DataTable();
        var rowData = users_table.row( $(this).parents('tr') ).data();

        $("#modal-edit-user").modal('show');

        $(".section-password").addClass("d-none");
        $('.section-password').find('input').attr('disabled','disabled');

        $.each(rowData, function(index, element) {
            $(".item-"+index).val(element);
        });
    });

    /**
     * Envia los parametros de la modal editar usuario al metodo sendDataUSer que se encarga de enviarlos al controlador
     */
    $("#form-edit-user").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "update-user";
        method = "POST";
        sendDataUser(ruta,method,data);
        $("#modal-edit-user").modal('hide');
    });

    /**
     * Envia los parametros para hacer un cambio de estatus en el usuario
     */
    $('#table_users tbody').on( 'click', '.changeStatus', function (e) {
        e.preventDefault();
        var data = new FormData();
        data.append('id',$(this).data('id'));
        ruta = "change-status";
        method = "POST";
        sendDataUser(ruta,method,data);
    });

});

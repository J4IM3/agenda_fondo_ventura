$(document).ready(function () {

    $("#table-tipos").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-data-tipos-ajax',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nombre', name: 'nombre'},
            {data: 'tipo', name: 'tipo'},
        ],
        "columnDefs": [{
            "targets": 3,
            "data": "download_link",
            "render": function (data, type, row, meta) {
                return '<a href="#" class="btn btn-info edit-tipo" data-val="' + row + '"    data-toggle="tooltip" title="Editar tipo"><span class="far fa-edit"></span></span></a> '
            }
        }
        ]
    });

    $(".open-modal-nuevo").on('click',function (e) {
        e.preventDefault();
        $("#modal-nuevo-tipo").modal('show');
    })

    $(".form-nuevo-tipo").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "add-tipo";
        method = "POST";
        sendDataItinerario(ruta, method, data, "#modal-nuevo-tipo");
    });

    /**
     * Editar tipo
     */

    $("#table-tipos tbody").on('click','.edit-tipo',function (e) {
        e.preventDefault();
        $("#modal-edit-tipo").modal('show');
        var tipo_table = $("#table-tipos").DataTable();
        var rowData = tipo_table.row( $(this).parents('tr') ).data();

        $.each(rowData, function(index, element) {
            console.log("item-"+index);
            $(".item-"+index).val(element);
        });
    });

    $(".form-edit-tipo").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "edit-tipo";
        method = "POST";
        sendDataItinerario(ruta, method, data, "#modal-edit-tipo");
    })


    /**
     * Hace el envio de los parametros al crear un invitado desde la vista del itinerario
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */
    function sendDataItinerario(ruta, method, data, nombreModal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                console.log(data);

                if (data.success == "error") {
                    modalEvent(data.success, data.msg);
                    return;
                }
                modalEvent(data.success, data.msg);
                $(nombreModal).modal('hide');
                $("#table-tipos").DataTable().ajax.reload();
            },
            error: function(errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function(index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });

            }

        });
    }


});

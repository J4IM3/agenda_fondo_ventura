$(document).ready(function() {
    $("#table_aliados").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-table-aliados-ajax',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'direccion', name: 'direccion' },
            { data: 'telefono', name: 'telefono' },
            { data: 'contacto', name: 'contacto' },
        ],
        "columnDefs": [{
            "targets": 5,
            "data": "download_link",
            "render": function(data, type, row, meta) {
                return '<a href="#" class="btn btn-info edit-negocio" data-val="' + row + '"    data-toggle="tooltip" title="Editar negocio"><span class="far fa-edit"></span></span></a> '
            }
        }]
    });

    $(".new-aliado").on('click',function (e) {
        e.preventDefault();
        $('#form-new-aliado').trigger("reset");

        $("#new-modal-aliado").modal("show");
    });


    $("#form-new-aliado").on("submit", function(e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "save-aliado";
        method = "POST";
        sendDataItinerario(ruta, method, data, "#new-modal-aliado");
    });


    /**
     * Editar un aliado
     */

    $("#table_aliados tbody").on('click', '.edit-negocio', function(e) {
        e.preventDefault();

        $("#modal-edit-aliado").modal('show');

        var aliados_table = $("#table_aliados").DataTable();
        var rowData = aliados_table.row($(this).parents('tr')).data();


        $.each(rowData, function(index, value) {
            console.log(index+" - "+value);
            $(".item-" + index + "-aliados").val(value);
            $(".item-" + index + "-aliados").val(value).trigger("chosen:updated");
        });

    });



    $("#form-edit-aliado").on("submit", function(e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "edit-aliado";
        method = "POST";
        sendDataItinerario(ruta, method, data, "#modal-edit-aliado");
    });




    /**
     * Hace el envio de los parametros al crear un aliado
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */
    function sendDataItinerario(ruta, method, data, nombreModal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                console.log(data);
                if (data.success == "error") {
                    modalEvent(data.success, data.msg);
                    return;
                }
                modalEvent(data.success, data.msg);
                $(nombreModal).modal('hide');
                $("#table_aliados").DataTable().ajax.reload();
            },
            error: function(errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function(index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }



});

$(document).ready(function () {
    let properties_status = "";
    $("#table-foros").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-data-foros-ajax',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nombre', name: 'nombre'},
            {data: 'foro_sede.name', name: 'foro_sede'},
            {data: 'type', name: 'type'},
        ],
        "columnDefs": [{
            "targets": 4,
            "data": "download_link",
            "render": function (data, type, row, meta) {
                console.log(row);
                properties_status = getIconForStatusGlobal(row.status);
                return '<a href="#" class="btn btn-info edit-foro" data-val="' + row + '"    ' +
                    'data-toggle="tooltip" title=Editar tipo"><span class="far fa-edit"></span></span></a> ' +
                    '<a href="#" class="btn btn-'+ properties_status.color +' update-status-foro" data-val="' + row.id + '"    ' +
                    'data-toggle="tooltip" title="Actualizar estatus"><span class="'+ properties_status.icon+'"></span></span></a> '

            }
        }
        ]
    });

    $(".nuevo-foro").on('click',function (e) {
        e.preventDefault();
        $(".form-nuevo-foro")[0].reset();
        $('.item-type').trigger('chosen:updated');
        $('.item-sede_id').trigger('chosen:updated');
        $("#modal-nuevo-foro").modal('show');

    })

    $(".form-nuevo-foro").on('submit',function (e) {
        e.preventDefault();
        let data = new FormData(this);
        ruta = "create-foro";
        method = "POST";
        $("#loadingGifFV").addClass("loading-fv");
        sendDataItinerario(ruta, method, data, "#modal-nuevo-foro");
    });

    $("#table-foros tbody").on('click','.update-status-foro',function (e){
       e.preventDefault();
       let data = new FormData();
        $("#loadingGifFV").addClass("loading-fv");
       data.append('id',$(this).data('val'));
       sendDataItinerario('update-status','POST',data);
    });

    /**
     * Editar tipo
     */

    $("#table-foros tbody").on('click','.edit-foro',function (e) {
        e.preventDefault();
        $("#modal-edit-foro").modal('show');
        var foro_table = $("#table-foros").DataTable();
        var rowData = foro_table.row( $(this).parents('tr') ).data();

        $.each(rowData, function(index, element) {
            $(".item-"+index).val(element);
            if (index == "type")
            {
                $(".item-type").val(element).trigger("chosen:updated");
            }
            if (index == "sede_id")
            {
                $(".item-sede_id").val(element).trigger("chosen:updated");
            }
        });
    });

    $(".form-edit-foro").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $("#loadingGifFV").addClass("loading-fv");
        ruta = "edit-foro";
        method = "POST";
        sendDataItinerario(ruta, method, data, "#modal-edit-foro");
    })


    /**
     * Hace el envio de los parametros al crear un invitado desde la vista del itinerario
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */
    function sendDataItinerario(ruta, method, data, nombreModal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                $("#loadingGifFV").removeClass("loading-fv")
                if (data.success == "error") {
                    modalEvent(data.success, data.msg);
                    return;
                }
                modalEvent(data.success, data.msg);
                $(nombreModal).modal('hide');
                $("#table-foros").DataTable().ajax.reload();
            },
            error: function(errors) {
                var indices = errors.responseJSON.errors;
                $("#loadingGifFV").removeClass("loading-fv")
                $.each(indices, function(index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });

            }

        });
    }


});

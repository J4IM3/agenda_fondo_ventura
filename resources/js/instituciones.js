$(document).ready(function () {
    $("#table-instituciones").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-data-instituciones-ajax',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nombre', name: 'nombre'},
        ],
        "columnDefs": [{
            "targets": 2,
            "data": "download_link",
            "render": function (data, type, row, meta) {
                var color,icon;
                if(row.status == 1)
                {
                    color = "btn btn-success changeStatus";
                    icon = "far fa-thumbs-up"
                }else
                {
                    color = "btn btn-danger changeStatus";
                    icon = "far fa-thumbs-down"
                }
                return '<a href="#" class="btn btn-info edit-institucion" data-val="' + row + '"    data-toggle="tooltip" title="Editar institucion"><span class="far fa-edit"></span></a> '+
                        '<a href="#" class="'+ color +'" data-id="' + row.id + '"    data-toggle="tooltip" title="Editar institucion"><span class="'+ icon +'"></span></a> '
            }
        }
        ]
    });

    /**
     * Abre modal para crear una nueva institucion
     */
    $(".crear-institucion").on('click',function (e) {
        e.preventDefault();
        $(".form-nueva-institucion")[0].reset();
        $("#modal-crear-institucion").modal('show');
    })

    /**
     * Enviar los dtos de la institucion a la funcion sendData para enviar al controlador
     */

    $(".form-nueva-institucion").on("submit",function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "nueva-institucion";
        method = "POST";
        sendData(ruta, method, data, "#modal-crear-institucion");
    });

    /**
     * Abre la modal para editar una institucion
     */

    $("#table-instituciones tbody").on('click','.edit-institucion',function (e) {
        e.preventDefault();
        var institucion_table = $("#table-instituciones").DataTable();
        var rowData = institucion_table.row( $(this).parents('tr') ).data();
        $.each(rowData,function (index,value) {
            $(".item-"+index).val(value);
        })
        $("#modal-editar-institucion").modal('show');

    });

    /**
     * Envia los parametros a la funcion sendData para actualizar una institucion
     */

    $(".form-editar-institucion").on("submit",function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "editar-institucion";
        method = "POST";
        sendData(ruta, method, data, "#modal-editar-institucion");
    });

    /**
     * Obtiene y envia los parametros para cambiar el status
     */

    $("#table-instituciones tbody").on('click','.changeStatus',function (e) {
        e.preventDefault();
        var data = new FormData();
        data.append('id',$(this).data('id'));
        ruta = "change-status-instituciones";
        method = "POST";
        sendData(ruta,method,data);
    })

    /**
     * Hace el envio de los parametros para crear una intitucion
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */
    function sendData(ruta, method, data, nombreModal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.success == "error") {
                    modalEvent(data.success, data.msg);
                    return;
                }
                modalEvent(data.success, data.msg);
                $(nombreModal).modal('hide');
                $("#table-instituciones").DataTable().ajax.reload();

            },
            error: function(errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function(index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });

            }

        });
    }



})

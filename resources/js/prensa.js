/**
 * Created by Jaime on 12/06/2019.
 */
$(document).ready(function () {
    $('select').chosen({
        allow_single_deselect: true
    });
    $("#table_prensa").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive:true,
        ajax:'get-table-prensa-ajax',
        columns:[
            {data: 'id', name:'id'},
            {data: 'prensa.[0].name', name:'prensa'},
            {data: 'medios[0].nombre', name:'medios[0].nombre'},
            {data: 'entrevistador', name:'entrevistador'},
            {data: 'fecha', name:'fecha'},
            {data: 'hora_inicio', name:'hora_inicio'},
            {data: 'hora_fin', name:'hora_fin'}
        ],
        "columnDefs": [{
            "targets": 7,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {
                console.log(row);

                return  '<a href="#" class="btn btn-primary show-detail-prensa" data-val="'+ row +'"    data-toggle="tooltip" title="Mostrar detalles vuelo-alojamiento"><i class="fas fa-info-circle"></i></a> '+
                        '<a href="#" class="btn btn-success edit-event-press" data-val="'+ row +'"    data-toggle="tooltip" title="Editar usuario"><span class="far fa-edit"></span></a> '+
                    '<a href="#" class="btn btn-danger delet-event-press" data-val="'+ row.id +'"    data-toggle="tooltip" title="Editar usuario"><i class="far fa-trash-alt"></i></a> '

            }}
        ]
    });

    $(".add-new-interview").on('click',function () {
        $(".form-new-interview")[0].reset();
        btnLoadingdeactivate();
        $(".moda-new-interview").modal('show');
    })

    $('.moda-new-interview').on('shown.bs.modal', function () {
        $('.chosen-select', this).chosen({
            no_results_text: "Presionar enter si desea agregar el autor",
            max_selected_options:10
        });

        $('.invitado_id',this).chosen({});
    });


    /**
     * Recibe de la modal los datos para crear una nueva entrevista
     */

    $(".form-new-interview").on('submit',function (e) {
        e.preventDefault();
        btnLoadingActivate();
        var data = new FormData(this);
        ruta = "save-interview";
        method = "POST";
        sendDataInterview(ruta,method,data,".moda-new-interview");
    })

    /**
     * Envia los parametros a la funcion sentData para en el controlador actualizar los datos del evento
     */
    $(".form-edit-event-press").on('submit',function (e) {
        e.preventDefault();
        btnLoadingActivate();
        var data = new FormData(this);
        ruta = "update-interview";
        method = "POST";

        sendDataInterview(ruta,method,data,'#modal-edit-event-press');
    })

    /**
     * Editar un evento de presna .
     * Lanza la modal con el detalle
     */

    $("#table_prensa tbody").on('click','.edit-event-press',function (e) {
        e.preventDefault();
        btnLoadingdeactivate();
        var prensa_table = $("#table_prensa").DataTable();
        var rowData = prensa_table.row( $(this).parents('tr') ).data();
        $.each(rowData , function (index,value) {
            $(".item-"+index).val(value)

            if(index == "invitado_id"){
                $(".item-invitado_id").val(value).trigger("chosen:updated");
            }
            if(index == "medio_id"){
                $(".item-medio_id").val(value).trigger("chosen:updated");
            }
            if(index == "tipo_evento_id"){
                $('.item-'+index).val(value).trigger("chosen:updated");
            }
        });

        $("#modal-edit-event-press").modal('show');

    })

    /**
     * Mostrar detalles del evento de prensa
     */

    $("#table_prensa tbody").on('click','.show-detail-prensa',function (e) {
        e.preventDefault();
        var prensa_table = $("#table_prensa").DataTable();
        var rowData = prensa_table.row( $(this).parents('tr') ).data();
        $("#modal-details-prensa").modal('show');
        $.each(rowData , function (index,value) {
            console.log(index,value);
            $("."+index).text(value);
            if (index == "medios") {
                $(".medio").text(value[0].nombre)

            }
            if (index == "prensa" ){
                $(".invitados").text(value[0].name)
            }
            if ( index == "prensa_tipos"){
                $(".tipos").text(value.nombre)
            }

        })
    });



    /**
     * Funcion que se encarga de enviar los datos al controlador
     */
    function sendDataInterview(ruta,method,data,nameModal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if (data.success == "error") {
                    btnLoadingdeactivate();
                    modalEventPrensa(data.success, data.msg);
                    return;
                }
                $(nameModal).modal('hide');
                modalEvent(data.success, data.msg);
                $("#table_prensa").DataTable().ajax.reload();
            },
            error: function (errors) {
                btnLoadingdeactivate();
                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }

    /**
     * Eliminar un evento de prensa
     */

    $("#table_prensa tbody").on('click','.delet-event-press',function (e) {
        e.preventDefault();
        var id = $(this).data('val');
        var data = new FormData();
        ruta = "delete-event-press";
        method = "post";
        data.append('id',id = $(this).data('val'));

        swal({
            title: '¿Estas seguro?',
            text: "Este evento sera eliminado",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if(result.value)
        {
            sendDataInterview(ruta,method,data)
        }
    })
    });

    function btnLoadingActivate() {
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span class="spinner-grow spinner-grow-sm " role="status" aria-hidden="true"></span> Espere un momento...'
        );
    }

    function btnLoadingdeactivate() {
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span></span> Guardar')
    }
})

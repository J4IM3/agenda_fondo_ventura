$(document).ready(function () {
    $(".table-filoando").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-data-filoando-ajax',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'fecha', name: 'fecha'},
            {data: 'hora_traslado_ida', name: 'hora_traslado_ida'},
            {data: 'hora_traslado_regreso', name: 'hora_traslado_regreso'},
            {data: 'instituciones.nombre', name: 'instituciones.nombre'},
            {data: 'invitados', name: 'invitados'},
        ],
        "columnDefs": [{
            "targets": 6,
            "data": "download_link",
            "render": function (data, type, row, meta) {
                return '<a href="#" class="btn btn-info edit-filoando" data-val="' + row + '"    data-toggle="tooltip" title="Editar tipo"><span class="far fa-edit"></span></span></a> '+
                       '<a href="#" class="btn btn-danger delete-filoando" data-val="' + row.id + '"  data-toggle="tooltip" title="Eliminar visita"><i class="fas fa-trash-alt"></i></a> '
            }
        }
        ]
    });



   $(".filoando-nuevo").on('click',function (e) {
       e.preventDefault();

       $(".btnFetch").prop("disabled", false);
       $(".btnFetch").html(
           '<span></span> Guardar'
       );
       $("#form-nuevo-filoando")[0].reset();
       $(".chosen").val('').trigger("chosen:updated");
       $("#modal-show-filoando").modal('show');
   })

    $("#form-nuevo-filoando").on("submit",function (e) {
        e.preventDefault();
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span class="spinner-grow spinner-grow-sm " role="status" aria-hidden="true"></span> Espere un momento...'
        );

        var data = new FormData(this);
        ruta = "create-filoando";
        method = "POST";
        sendData(ruta, method, data, "#modal-show-filoando");
    })

    $(".table-filoando tbody").on('click','.edit-filoando',function (e) {
        e.preventDefault();
        $("#modal-edit-filoando").modal("show");
        $(".btnFetch").prop("disabled", false);
        $(".btnFetch").html(
            '<span></span> Guardar'
        );
        var table_filoando= $(".table-filoando").DataTable();
        var rowData = table_filoando.row( $(this).parents('tr') ).data();
        $.each(rowData,function (index,item) {
            $(".item-"+index).val(item);
            $(".item-" + index).val(item).trigger("chosen:updated");
        });
        var filoando = new Array();
        $.each(rowData.filoando, function(index, value) {
            filoando.push('id',value.invitado_id);
        });
        $(".item-invitado_id").val(filoando).trigger("chosen:updated");

    });

    $("#form-editar-filoando").on("submit",function (e) {
        e.preventDefault();

        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span class="spinner-grow spinner-grow-sm " role="status" aria-hidden="true"></span> Espere un momento...'
        );

        var data = new FormData(this);
        ruta = "update-filoando";
        method = "POST";
        sendData(ruta, method, data, "#modal-edit-filoando");
    })


    $(".table-filoando tbody").on('click','.delete-filoando',function (e){
        e.preventDefault();
        var data = new FormData();
        data.append('id',id = $(this).data('val'));
        ruta = "delete-filoando";
        method = "post";


        swal({
            title: '¿Estas seguro?',
            text: "Este evento sera eliminado",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) =>
        {
            if(result.value)
        {
            sendData(ruta,method,data,"#modal-edit-filoando")
        }
        })

    });

    /**
     * Hace el envio de los parametros al crear un invitado desde la vista del itinerario
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */

    function sendData(ruta, method, data, nombreModal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.success == "error") {
                    modalEventPrensa(data.success, data.msg);
                    return;
                }
                modalEvent(data.success, data.msg);
                $(nombreModal).modal('hide');
                $(".table-filoando").DataTable().ajax.reload();
            },
            error: function(errors) {
                $(".btnFetch").prop("disabled", false);
                $(".btnFetch").html(
                    '<span></span> Guardar'
                );
                var indices = errors.responseJSON.errors;
                $.each(indices, function(index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });

            }

        });
    }


});

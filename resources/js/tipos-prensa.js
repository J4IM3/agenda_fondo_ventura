$(document).ready(function () {
    var icon;
    var colorButton;
    $("#table-tipos-prensa").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-data-tipos-prensa-ajax',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nombre', name: 'nombre'},
        ],
        "columnDefs": [{
            "targets": 2,
            "data": "download_link",
            "render": function (data, type, row, meta) {
                if (row.status == 1 )
                {
                    icon = "far fa-thumbs-up";
                    colorButton ="btn btn-success";

                }else{
                    icon = "far fa-thumbs-down";
                    colorButton ="btn btn-danger";
                }
                return '<a href="#" class="btn btn-info edit-tipo-prensa" data-val="' + row + '"    data-toggle="tooltip" title="Editar tipo"><span class="far fa-edit"></span></span></a> ' +
                '<a href="#" class="'+ colorButton +' change-status-prensa"  data-id ="'+ row.id +'" data-val="' + row + '"    data-toggle="tooltip" title="Cambiar status"><i class="'+ icon +'"></i></a> '
            }
        }
        ]
    });
    /**
     * Abre la modal para crear un nuevo tipo de evento para prensa
     */
    $(".nuevo-tipo-prensa").on('click', function (e) {
        e.preventDefault();
        $("#form-nuevo-tipo")[0].reset();
        $("#modal-nuevo-tipo").modal('show');
    });

    /**
     * Obtiene los datos para crear el tipo
     */

    $("#form-nuevo-tipo").on("submit",function (e) {
        e.preventDefault()
        var data = new FormData(this);
        ruta = "crear-tipo";
        method = "POST";
        sendDataType(ruta, method, data, "#modal-nuevo-tipo");
    });

    /**
     * Abre la modal para editar el tipo de evento de prensa
     *
     */

    $("#table-tipos-prensa tbody").on('click','.edit-tipo-prensa',function (e) {
        e.preventDefault();
        $("#modal-editar-tipo").modal('show');
        var tipo_table = $("#table-tipos-prensa").DataTable();
        var rowData = tipo_table.row( $(this).parents('tr') ).data();
        $.each(rowData, function(index, element) {
            console.log("item-"+index);
            $(".item-"+index).val(element);
        });
    });


    /**
     * Obtiene y envia los datos para actualizar el tipo a la funcion senddata
     */

    $("#form-editar-tipo").on("submit",function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "editar-tipo";
        method = "POST";
        sendDataType(ruta, method, data, "#modal-editar-tipo");
    });

    /**
     * Obtiene los datos para activar o desactivar un tipo
     */

    $("#table-tipos-prensa tbody").on('click','.change-status-prensa',function (e) {
        e.preventDefault();
        var data = new FormData();
        data.append('id',$(this).data('id'));
        ruta = "change-status-tipo";
        method = "POST";
        sendDataType(ruta, method, data, "#modal-editar-tipo");
    });


    /**
     * Hace el envio de los parametros al controlador de las funciones crear,editar,cambiarStatus
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */
    function sendDataType(ruta, method, data, nombreModal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                console.log(data);

                if (data.success == "error") {
                    modalEvent(data.success, data.msg);
                    return;
                }
                modalEvent(data.success, data.msg);
                $(nombreModal).modal('hide');
                $("#table-tipos-prensa").DataTable().ajax.reload();
            },
            error: function(errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function(index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });

            }

        });
    }
});

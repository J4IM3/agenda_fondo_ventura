$(document).ready(function () {

    $("#table_aerolineas").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive:true,
        ajax:'get-table-aerolinea-ajax',
        columns:[
            {data: 'id', name:'id'},
            {data: 'name', name:'name'},
        ],
        "columnDefs": [{
            "targets": 2,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {
                console.log(row);
                var icon , title,colorClas;
                if(row.status != 0 )
                {
                    icon = "far fa-check-circle";
                    title = "Desactivar";
                    colorClas ="primary";
                }else{
                    icon = "fas fa-times-circle";
                    title = "Activar";
                    colorClas = "danger";
                }
                return  '<a href="#" class="btn btn-info edit-aerolinea" data-val="'+ row +'"    data-toggle="tooltip" title="Editar usuario"><span class="far fa-edit"></span></span></a> '+
                    '<a href="#" class="btn btn-'+ colorClas +' changeStatus"   data-id ="'+ row.id +'"  data-toggle="tooltip" title="'+ title +'"><span class="' + icon + '"></span></a> '
            }}
        ]
    });



    /**
     * Agregar nueva aerolinea
     */

    $('.add-aerolinea').on('click',function () {
        $("#form-add-aerolinea").trigger("reset");
        $(".section-password").removeClass("d-none");
        $('.section-password').find('input').prop('disabled',false);
        $("#modal-create-aerolinea").modal('show');
    });

    /**
     *  Envia los parametros de la modal para crear una nueva aerolinea
     */
    $("#form-add-aerolinea").on("submit", function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "save-aerolinea";
        method = "POST";
        sendDataAero(ruta,method,data);
        $("#modal-create-aerolinea").modal('hide');
    });


    /**
     * Hace el envio de los parametros al crear o editar una aeroilinea para enviarlo al controlador
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */
    function sendDataAero(ruta,method,data) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(data.success == "error")
                {
                    modalEvent(data.success, data.msg);
                    return ;
                }
                modalEvent(data.success, data.msg);
                $("#table_aerolineas").DataTable().ajax.reload();
            },
            error: function (errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }

    /**
     * Muestra modal para editar una aerolinea
     */
    $('#table_aerolineas tbody').on( 'click', '.edit-aerolinea', function (e) {
        e.preventDefault();
        var aerolinea_table = $("#table_aerolineas").DataTable();
        var rowData = aerolinea_table.row( $(this).parents('tr') ).data();

        $("#modal-edit-aerolinea").modal('show');
        $(".section-password").addClass("d-none");
        $('.section-password').find('input').attr('disabled','disabled');

        $.each(rowData, function(index, element) {
            $(".item-"+index).val(element);
        });
    });

    /**
     * Envia los parametros de la modal editar aerolinea al metodo sendDataUSer que se encarga de enviarlos al controlador
     */
    $("#form-edit-aerolinea").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "update-aerolinea";
        method = "POST";
        sendDataAero(ruta,method,data);
        $("#modal-edit-aerolinea").modal('hide');
    });

    /**
     * Envia los parametros para hacer un cambio de estatus de la aerolinea
     */
    $('#table_aerolineas tbody').on( 'click', '.changeStatus', function (e) {
        e.preventDefault();
        var data = new FormData();
        data.append('id',$(this).data('id'));
        ruta = "change-status";
        method = "POST";
        sendDataAero(ruta,method,data);
    });



});

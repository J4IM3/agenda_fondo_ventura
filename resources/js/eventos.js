$(document).ready(function () {
    $('select').chosen({
        allow_single_deselect: true
    });

    $(".moderadores").hide();
    $(".presentadores").hide();
    $(".colaboracion").hide();
    var presentadores = [];
    var obj = {};
    $('.div-item-horario-prueba-sonido').hide();
    $("#table_eventos").DataTable({
        proccesing: true,
        serverSide: true,
        order: [[ 1, "ASC" ],[ 2, "ASC" ]],
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
        },
        responsive:true,
        ajax:'get-table-eventos-ajax',
        columns:[
            {data: 'id', name:'id',"searchable": false, },
            {data: 'fecha', name:'fecha'},
            {
                data: 'hora_inicio',
                render: function (data, type, row, meta) {
                    return row.hora_inicio + ' - ' + row.hora_fin
                }
            },
            {data: 'nombreSedes', name:'nombreSedes'},
            {data: 'tipo.nombre', name:'tipo.nombre',"searchable": false},
            {data: 'nombre_evento', name:'nombre_evento'},
            {data: 'descripcion_alterna', name:'descripcion_alterna',
                "render": function(data, type, row, meta){
                    data = '<a href="#" data-description="' + row.descripcion + '" class="view-description">' + data + '</a>';
                    return data;
                }
            },
            {data: 'participantesPrueba', name:'participantesPrueba'}
        ],
        "columnDefs": [{
            "targets": 8,
            "render": function ( data, type, row, meta ) {
                console.log(row);
                return  '<a href="#" class="btn btn-info detail-event" data-val="'+ row +'"    data-toggle="tooltip" title="Requerimientos"><span class="fas fa-info-circle"></span></span></a> '+
                '<a href="#" class="btn btn-info edit-event" data-id = "'+ row.id +'"data-toggle="tooltip" title="Editar evento"><span class="far fa-edit"></span></span></a> '+
                    '<a href="#" class="btn btn-success sound-test" data-val="'+ row.id+'"    data-toggle="tooltip" title="Agregar prueba sonido evento"><i class="fas fa-podcast"></i></a> '+
                    '<a href="#" class="btn btn-danger delet-event" data-val="'+ row.id+'"    data-toggle="tooltip" title="Eliminar evento"><i class="fas fa-trash-alt"></i></a> '
            }}
        ],
    });

    $(".new-event").on('click', function(){
        /*$("#form-save-event").trigger("reset");*/
        $('#form-save-event')[0].reset();
        $(".chosen").val('').trigger("chosen:updated");
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span></span> Guardar'
        );
        $(".item-invitados").val("Seleccona invitados").trigger("chosen:updated");
        $(".item-tipo_id").val("Selecciona una opción").trigger("chosen:updated");
        $(".item-moderadores").val("Selecciona una opción").trigger("chosen:updated");
        $(".item-presentadores").val("Selecciona una opción").trigger("chosen:updated");
        $(".item-programacion_id").val("Seleccona una opción").trigger("chosen:updated");
        $(".item-formato").val("Seleccona una opción").trigger("chosen:updated");
        $(".item-evento_foro_id").val("Seleccona una opción").trigger("chosen:updated");

        $("#modal-create-event").modal({backdrop: 'static', keyboard: false});
        $("#modal-create-event").modal('show');

    });

    /**
     * Abre modal para ver los requerimientos de la presentacion
     */

    $("#table_eventos tbody").on('click','.detail-event',function (e) {
        e.preventDefault();

        var eventos_table = $("#table_eventos").DataTable();
        var rowData = eventos_table.row( $(this).parents('tr') ).data();
        $.each(rowData, function (index, value) {
            console.log(index,value);
            if (index == "requirimientos"){
                $("#detallesReq").text(value)
            }
        })
        $("#modal-details-event").modal('show');
    });


    $("#table_eventos tbody").on('click','.view-description ',function (e){
        swal.fire({
            type: 'info',
            title: 'Descripción',
            text: $(this).data('description'),
        })
    });

    /**
     *  Envia los parametros de la modal para crear un nuevo evento
     */
    $("#form-save-event").on("submit", function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "save-event";
        method = "POST";

        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span class="spinner-grow spinner-grow-sm " role="status" aria-hidden="true"></span> Espere un momento...'
        );
        sendDataEvent(ruta, method, data, "#modal-create-event");

    });


    /**
     * Muestra la modal para agregar una prueba de sonido
     */

    $("#table_eventos tbody").on('click','.sound-test',function(e){
        e.preventDefault();
        $("#form-create-sound-test").trigger("reset");
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span></span> Guardar')
        $("#modal-sound-test").modal('show');
        $(".input-evento-id").val($(this).data('val'));
        var eventos_table = $("#table_eventos").DataTable();
        var rowData = eventos_table.row( $(this).parents('tr') ).data();
        $.each(rowData.sound_test, function (index, value) {
            if(index == "foro_id")
            {
                $(".item-foro_id_prueba_sonido").val(value).trigger("chosen:updated");
            }
            $(".item-"+index).val(value);
        })
    });

    $('#table_eventos tbody').on('click','.edit-event',function (e) {
        e.preventDefault();
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span></span> Guardar'
        );
        let data = new FormData();
        data.append('id',$(this).data('id'));
        $.ajax({
            url: "editar-evento",
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                $("#modal-edit-event").modal('show');
                let invitados = new Array();
                let sedes = new Array();
                let presentadores = new Array();
                let moderador = new Array();

                $.each(data,function (index,value)
                {
                    $(".item-"+index).val(value)
                   if(index == "formato") {
                        $(".item-formato").val(value).trigger("chosen:updated");
                    }

                    if(index == "programacion_id") {
                        $(".item-programacion_id").val(value).trigger("chosen:updated");
                    }

                    //todo hacer pruebas con este item
                    if (index == "colaborador") {
                        if (value != null) {
                            $("#colaborador").prop('checked', true);
                            $(".colaboracion").show();
                        } else {
                            $('.item-colaborador').prop('checked', false);
                            $(".colaboracion").hide();
                        }
                    }

                    if (index === "tipo_id") {
                        $(".item-tipo_id").val(value).trigger("chosen:updated");
                        if (value==10)
                        {
                            $.each(data.presentador,function (index,value) {
                                presentadores.push('id',value.presentador_id);
                            });
                            $(".item-presentadores").val(presentadores).trigger("chosen:updated");

                            $(".presentadores").show();
                            $(".moderadores").hide();
                        }
                        else if(value==11) {
                            $.each(data.moderador,function (index,value) {
                                moderador.push('id',value.moderador_id);
                            });
                            $(".item-moderadores").val(moderador).trigger("chosen:updated");

                            $(".presentadores").hide();
                            $(".moderadores").show();

                        }else
                        {
                            $(".presentadores").hide();
                            $(".moderadores").hide();
                        }
                    }

                    $.each(data.invitados,function (index,value) {
                        invitados.push('id',value.invitado_id);
                    });
                    $(".item-invitados").val(invitados).trigger("chosen:updated");

                    $.each(data.sedes,function (idx,val) {
                        sedes.push('id',val.sede_id);
                    });
                    $(".item-evento_foro_id").val(sedes).trigger("chosen:updated");
                })
                $.each(data.event_books,function (index,value) {
                    let pos = index + 1 ;
                    $(".item-title"+pos).val(value.titulo);
                    $(".quantity"+[pos]).val(value.quantity_for_presenter)

                    if (value.is_presenter == 1 ) {
                        $(".item-for_promotor"+pos).prop('checked', true);
                    }
                    if (value.quantity_for_presenter > 0) {
                        $(".quantity"+[pos]).attr("readonly", false);
                    }
                });
            }


        });




    });

    $("#table_eventos tbody").on('click','.delet-event',function (e) {
        e.preventDefault();
        var id = $(this).data('val');
        var data = new FormData();
        data.append('id',id = $(this).data('val'));
        ruta = "delete-event";
        method = "post";

        swal({
            title: '¿Estas seguro?',
            text: "Este evento sera eliminado",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) =>
        {
            if(result.value)
        {
            sendDataEvent(ruta,method,data)
        }
        })


    });


    /**
     * Envia los parametros de la modal editar un evento al metodo sendDataEvent que se encarga de enviarlos al controlador
     */
    $("#form-edit-event").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "update-event";
        method = "POST";
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span class="spinner-grow spinner-grow-sm " role="status" aria-hidden="true"></span> Espere un momento...'
        );
        sendDataEvent(ruta, method, data, "#modal-edit-event");
    });

    $("#form-create-sound-test").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "create-sount-test";
        method = "POST";
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span class="spinner-grow spinner-grow-sm " role="status" aria-hidden="true"></span> Espere un momento...'
        );
        sendDataEvent(ruta, method, data, "#modal-sound-test");

    });

    /**
     * Hace el envio de los parametros al crear o editar un evento para enviarlo al controlador
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */
    function sendDataEvent(ruta,method,data,nombreModal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data)
                if (data.success == "error") {
                    $(".btnFetch").prop("disabled", false);
                    // add spinner to button
                    $(".btnFetch").html(
                        '<span></span> Guardar'
                    );
                    modalEventPrensa(data.success, data.msg);
                    return;
                }
                modalEvent(data.success, data.msg);
                $(nombreModal).modal('hide');
                $("#table_eventos").DataTable().ajax.reload(null,false);
            },
            error: function (errors)
            {
                $(".btnFetch").prop("disabled", false);
                // add spinner to button
                $(".btnFetch").html(
                    '<span></span> Guardar'
                );
                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {
                    console.log(index,value);
                    $(".error-" + index).append(value);
                    $(".error-" + index).show();
                });
            }

        });
    }

    /**
     * Al seleccinar el tipo de evento mesa, se muestra
     * el div de moderadores
     */

    $(".item-tipo_id").on('change',function () {

        let option_select = $(this).find(":selected").text();
        console.log(option_select);
        if (option_select == "Presentación editorial")
        {
            $('.item-moderadores').val('').trigger('chosen:updated');
            $(".presentadores").show();
            $(".moderadores").hide();
        }
        else if (option_select == "Mesa")
        {
            $('.item-presentadores').val('').trigger('chosen:updated');
            $(".moderadores").show();
            $(".presentadores").hide();
        }
        else
        {
            $('.item-presentadores').val('').trigger('chosen:updated');
            $('.item-moderadores').val('').trigger('chosen:updated');
            $(".presentadores").hide();
            $(".moderadores").hide();
        }
    })


    /**
     * Trae las sedes dependiendo de la opción seleccionada
     */

    $(".item-formato").on('change',function () {
        var option_select = $(this).find(":selected").text();
        var data = new FormData();
        data.append('type', option_select);
        $.ajax({
            url: "get-sede-by-type",
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data)
                $(".item-evento_foro_id").html("");

                $.each(data, function (idx, obj) {
                    console.log(idx,obj);
                    $(".item-evento_foro_id").append('<option value="' + obj.id + '">' + obj.nombre + '</option>');
                    /*$('.item-evento_foro_id').append(new Option(1, "2"));*/
                });
                $('.item-evento_foro_id').trigger("chosen:updated");
                /*$(".item-foro_id").trigger("liszt:updated");

                $(".item-foro_id").chosen({ width: "95%" });*/

            },
            error: function (errors) {
                $(".btnFetch").prop("disabled", false);
                // add spinner to button
                $(".btnFetch").html(
                    '<span></span> Guardar'
                );
                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {
                    console.log(index, value);
                    $(".error-" + index).append(value);
                    $(".error-" + index).show();
                });
            }
        })
    });

    window.updatePromotor = function (param,element) {

        if (element.checked) {
            $(".quantity"+param).prop('readonly',false);
        }else{
            $(".quantity"+param).prop('readonly',true);
            swal.fire({
                icon: 'info',
                title: '¡ Atencion !',
                text: 'Si desea activar nuevamente este libro para el presentador debera ingresar nuevamente una cantidad',
            })
            $(".quantity"+param).val(0);
        }

    }
    /*$(document).on('change', '.for_promotor', function() {

        let index = 0;
        $('.for_promotor').each(function() {
            if (this.checked) {
                console.log(index);
                $(".quantity"+index).prop('readonly',false);
            }else{
                $(".quantity"+index).prop('readonly',true);
                $(".quantity"+index).val('');
            }
            index++;
        });
    });*/

        /**
     * Comprueba si esta en on el check para agregar un colaborador
     */

    $('input[type="checkbox"]').click(function(){
        if($(this).prop("checked") == true){
            $(".colaboracion").show();
        }
        else if($(this).prop("checked") == false){
            $(".colaboracion").hide();
        }
    });

    function visible(nombre) {
        $("."+nombre).show();
    }

});

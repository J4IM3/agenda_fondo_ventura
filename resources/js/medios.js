/**
 * Created by Jaime on 05/08/2019.
 */
$(document).ready(function () {
    $("#table-medios").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-data-medios-ajax',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nombre', name: 'nombre'},
        ],
        "columnDefs": [{
            "targets": 2,
            "data": "download_link",
            "render": function (data, type, row, meta) {
                return '<a href="#" class="btn btn-info edit-medios" data-val="' + row + '"    data-toggle="tooltip" title="Editar tipo"><span class="far fa-edit"></span></span></a> '
            }
        }
        ]
    });

    $(".nuevo-medio").on('click',function (e) {
        e.preventDefault();
        $("#modal-nuevo-medio").modal('show');
    })


    $(".form-nuevo-medio").on("submit",function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "create-medio";
        method = "POST";
        sendData(ruta, method, data, "#modal-nuevo-medio");
    })

    $("#table-medios tbody").on('click','.edit-medios',function (e) {
        e.preventDefault();
        var medios_table = $("#table-medios").DataTable();
        var rowData = medios_table.row( $(this).parents('tr') ).data();

        $.each(rowData, function(index, element) {
            console.log("item-"+index);
            $(".item-"+index).val(element);
        });
        $("#modal-editar-medio").modal('show');
    });

    $(".form-edit-medio").on("submit",function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "editar-medio";
        method = "POST";
        sendData(ruta, method, data, "#modal-editar-medio");

    })

    function sendData(ruta, method, data, nombreModal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                console.log(data);

                if (data.success == "error") {
                    modalEvent(data.success, data.msg);
                    return;
                }
                modalEvent(data.success, data.msg);
                $(nombreModal).modal('hide');
                $("#table-medios").DataTable().ajax.reload();
            },
            error: function(errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function(index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });

            }

        });
    }

});

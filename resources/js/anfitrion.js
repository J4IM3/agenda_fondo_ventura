$(document).ready(function () {
        $("#table-anfitrion").DataTable({
            proccesing: true,
            serverSide: true,
            language: {
                //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            responsive: true,
            ajax: 'get-table-anfitrion-ajax',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'nombre', name: 'nombre'},
                {data: 'telefono', name: 'telefono'}
            ],
            "columnDefs": [{
                "targets": 3,
                "data": "download_link",
                "render": function (data, type, row, meta) {
                    return '<a href="#" class="btn btn-info" id="edit-anfitrion" data-val="' + row + '"    data-toggle="tooltip" title="Editar negocio"><span class="far fa-edit"></span></span></a>'
                }
            }]
        });


        $("#table-anfitrion tbody").on('click', '#edit-anfitrion', function(e) {
        e.preventDefault();
        $("#modal-edit-anfitrion").modal('show');
        var anfitrion_table = $("#table-anfitrion").DataTable();
        var rowData = anfitrion_table.row($(this).parents('tr')).data();
        $.each(rowData, function(index, value) {
            console.log(index+" - "+value);
            $(".item-" + index + "-anfitrion").val(value);
            $(".item-" + index + "-anfitrion").val(value).trigger("chosen:updated");
            });
        });

    $(".create-anfitrion").on('click',function (e) {
        e.preventDefault();
        $("#form-save-anfitrion")[0].reset();
        $("#modal-create-anfitrion").modal('show');
    })

    $("#form-save-anfitrion").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "save-anfitrion";
        method = "POST";
        sendDataAnfitrion(ruta, method, data, "#modal-create-anfitrion");
    });

    $("#form-update-anfitrion").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta = "update-anfitrion";
        method = "POST";
        sendDataAnfitrion(ruta, method, data, "#modal-edit-anfitrion");
    })

    /**
     * Hace el envio de los parametros al crear un anfitrion
     * @param ruta
     * @param method
     * @param data
     * @param nameModal
     */
    function sendDataAnfitrion(ruta, method, data, nombreModal) {
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.success == "error") {
                    modalEvent(data.success, data.msg);
                    return;
                }
                modalEvent(data.success, data.msg);
                $(nombreModal).modal('hide');
                $("#table-anfitrion").DataTable().ajax.reload();

            },
            error: function(errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function(index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }


})

/**
 * Created by Jaime on 12/06/2019.
 */
$(document).ready(function () {
    $("#table_social").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            //"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive: true,
        ajax: 'get-table-social-ajax',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'social_invitado[0].name', name: 'invitado'},
            {data: 'social_hotel.name', name: 'social_hotel.name'},
            {data: 'actividad', name: 'actividad'},
            {data: 'fecha', name: 'fecha'},
            {data: 'traslado_ida', name: 'traslado_id'},
            {data: 'traslado_regreso', name: 'traslado_regreso'}
        ],
        "columnDefs": [{
            "targets": 7,
            "data": "download_link",
            "render": function (data, type, row, meta) {
                return '<a href="#" class="btn btn-success  show-details-social" data-val="' + row.id + '"    data-toggle="tooltip" title="Mostrar detalles"><i class="fas fa-plus-square"></i></a> '+
                        '<a href="#" class="btn btn-info edit-event-social" data-val="' + row + '"    data-toggle="tooltip" title="Editar usuario"><span class="far fa-edit"></span></a> ' +
                        '<a href="#" class="btn btn-danger delet-event-social" data-val="' + row.id + '"    data-toggle="tooltip" title="Editar usuario"><i class="far fa-trash-alt"></i></a> '
            }
        }
        ]
    });
    /**
     *
     */
    $(".new-social").on('click',function (e) {
        e.preventDefault();
        btnLoadingdeactivate();
        $(".form-new-event-social")[0].reset();
        $(".chosen").val('').trigger("chosen:updated");
        $(".moda-new-social").modal('show');
    })
    /***
     *
     */

    $(".form-new-event-social").on('submit',function (e) {
        e.preventDefault();
        btnLoadingActivate();
        var data = new FormData(this);
        ruta = "save-data-social";
        method = "POST";
        sendDataSocial(ruta,method,data);
    })

    /**
     * Editar evento
     */

    $("#table_social tbody").on('click','.edit-event-social',function (e) {
        e.preventDefault();
        btnLoadingdeactivate();
        var social_table = $("#table_social").DataTable();
        var rowData = social_table.row( $(this).parents('tr') ).data();
        $.each(rowData , function (index,value) {
            $(".item-"+index).val(value)

            if(index == "invitado_id"){
                $(".item-invitado_id").val(value).trigger("chosen:updated");
            }
            if(index == "lugar_id"){
                $(".item-lugar_id").val(value).trigger("chosen:updated");
            }
        });

        $("#modal-edit-event-social").modal('show');
    });

    $(".form-edit-event-social").on('submit',function (e) {
        e.preventDefault();
        btnLoadingActivate();
        var data = new FormData(this);
        ruta = "update-data-social";
        method = "POST";
        sendDataSocial(ruta,method,data);
    })

    function sendDataSocial(ruta,method,data){
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if (data.success == "error") {
                    btnLoadingdeactivate();
                    modalEventPrensa(data.success, data.msg);
                    return;
                }
                modalEvent(data.success, data.msg);
                $(".moda-new-social").modal('hide');
                $("#modal-edit-event-social").modal('hide');
                $("#table_social").DataTable().ajax.reload();
            },
            error: function (errors) {
                btnLoadingdeactivate();
                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {
                    $(".error-" + index).append(value);
                    $(".error-" + index).show();
                });
            }
        });
    }


    /**
     * Eliminar un evento de prensa
     */

    $("#table_social tbody").on('click','.delet-event-social',function (e) {
        e.preventDefault();
        var id = $(this).data('val');
        var data = new FormData();
        ruta = "delete-event-social";
        method = "post";
        data.append('id',id = $(this).data('val'));

        swal({
            title: '¿Estas seguro?',
            text: "Este evento sera eliminado",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if(result.value)
        {
            sendDataSocial(ruta,method,data)
        }
        })
    });


    /**
     * Muestra los detalles del evento social
     *
     */

    $("#table_social tbody").on('click','.show-details-social',function (e) {
        e.preventDefault();
        var social_table = $("#table_social").DataTable();
        var rowData = social_table.row( $(this).parents('tr') ).data();
        $.each(rowData, function (index, value) {
            if (index == "social_invitado" )
            {
                $(".item-social_invitado_name").text(value[0].name);
            }
            if (index == "social_hotel")
            {
                $.each(rowData.social_hotel,function (element,item) {
                    $(".item-"+element).text(item);
                })
            }
            $(".item-"+index).text(value)
        });
        $("#modal-show-event").modal('show');
    });


    function btnLoadingActivate() {
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span class="spinner-grow spinner-grow-sm " role="status" aria-hidden="true"></span> Espere un momento...'
        );
    }

    function btnLoadingdeactivate() {
        $(".btnFetch").prop("disabled", false);
        // add spinner to button
        $(".btnFetch").html(
            '<span></span> Guardar')
    }

});

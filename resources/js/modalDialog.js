/**
 * Created by Jaime on 04/06/2019.
 */
$(document).ready(function () {
    /**
     * Retorna una modal de sweet alert para mostrar el mensaje correspondiente
     * se le envian los parametros de tipo pueden ser success , error, y el mensaje a mostrar
     */

    window.modalEvent = function(type,msg)
    {
        swal({
            position: 'center',
            type: type,
            title: msg,
            showConfirmButton: false,
            timer: 1500
        });
    }
    window.modalUserDelete = function(type,msg)
    {
        swal({
            position: 'center',
            type: 'error',
            title: msg,
            showConfirmButton: true,
        });
    }

    window.modalEventPrensa = function (type,msg){
        swal.fire({
            type: type,
            title: '',
            text: msg,
            showConfirmButton: true,
        })
    }

    window.modalDate = function (msg)
    {
        swal.fire(msg);
    }

    window.getIconForStatusGlobal = function (status)
    {
        let icon  = status == "1" ? "fas fa-thumbs-up" : "fas fa-thumbs-down";
        let color = status == "1" ? "success" : "danger";
        return {'icon' : icon,'color':color};
    }


});

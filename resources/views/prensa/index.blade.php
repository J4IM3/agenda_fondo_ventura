@extends('layouts.app')
@section('content')
    <div class="container">
        <button class="btn btn-primary add-new-interview"> Nueva <i class="fas fa-broadcast-tower"></i> </button>
        <br> <br>
        <div class="col-md-12 col-sm-12">
        <table class="table table-bordered table-responsive-sm table-responsive-md  table-responsive-lg table-striped" id="table_prensa">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Medio</th>
                <th>Entrevistador</th>
                <th>Fecha</th>
                <th>Hora Inicio</th>
                <th>Hora fin</th>
                <th>Opciones</th>
            </tr>
            </thead>

        </table>
        </div>
        @include('prensa.modal-new-interview')
        @include('prensa.modal-edit-event-press')
        @include('prensa.details')

    </div>
    @endsection

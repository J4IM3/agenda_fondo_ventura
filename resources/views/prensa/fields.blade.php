<div class="col-md-12 mb-3">
    <label for="validationCustomUsername">Seleccionar autor </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen-select item-invitado_id" required="required" name="invitado_id[]" placeholder="Seleccione .." data-placeholder="Seleccion invitado(s)" single>
            <option value="">Seleccione una opcion</option>
            @foreach($invitados as $invitado)
                <option value="{{ $invitado->id }}">{{$invitado->name}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-12 mb-3">
    <label for="validationCustomUsername">Otros participantes</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-list"></i></span>
        </div>
        <input type="text" class="form-control item-participante" name="participante"  placeholder="Ingresar otros participantes" aria-describedby="inputGroupPrepend">
    </div>
</div>

<div class="col-md-12" align="center" >
    <hr> <h5>Datos medios</h5> <hr>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Seleccionar medio </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen-select item-medio_id" required="required" name="medio_id" placeholder="Seleccione .." data-placeholder="Seleccion medios">
            <option value="">Seleccione una opcion</option>
            @foreach($medios as $medio)
                <option value="{{ $medio->id }}">{{$medio->nombre}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Entrevistador</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-microphone-alt"></i></span>
        </div>
        <input type="text" class="form-control item-entrevistador" name="entrevistador" required placeholder="Ingresar entrevistador(es)" aria-describedby="inputGroupPrepend" >
    </div>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Seleccionar tipo. </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen-select item-tipo_evento_id" required name="tipo_evento_id" required placeholder="Seleccione .." data-placeholder="Seleccion tipo" single>
            <option value="">Seleccione una opcion</option>
            @foreach($tipos as $tipo)
                <option value="{{ $tipo->id }}">{{$tipo->nombre}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-12" align="center" >
    <hr> <h5>Datos generales</h5> <hr>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Fecha</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-clock"></i></span>
        </div>
        <input type="text" class="form-control datepicker item-fecha" name="fecha" required placeholder="Ingresar fecha"  >
    </div>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Hora inicio</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_inicio" required placeholder="Ingresar hora inicio" name="hora_inicio" >
    </div>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Hora fin</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_fin" required placeholder="Ingresar hora fin" name="hora_fin" >
    </div>
</div>


<div class="col-md-12 mb-3">
    <label for="validationCustomUsername">Lugar</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fab fa-html5"></i> </span>
        </div>
        <input type="text" class="form-control item-lugar" name="lugar" placeholder="Ingresar lugar" >
    </div>
</div>








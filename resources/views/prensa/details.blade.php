<div class="modal" tabindex="-1" id="modal-details-prensa" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Prensa detalle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Fecha</th>
                        <th colspan="2">Hora</th>
                        <th>Lugar </th>
                        <th>Medio</th>
                        <th>Tipo</th>
                        <th>Invitado</th>
                        <th>Otros participantes</th>
                        <th>Entrevistador</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td> <span class="fecha"></span> </td>
                        <td colspan="2"> <span class="hora_inicio"></span> a <span class="hora_fin"></span> </td>
                        <td> <span class="lugar"></span> </td>
                        <td> <span class="medio"></span> </td>
                        <td> <span class="tipos"></span> </td>
                        <td> <span class="invitados"></span> </td>
                        <td> <span class="participante"></span> </td>
                        <td> <span class="entrevistador"></span> </td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

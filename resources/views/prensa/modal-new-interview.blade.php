<div class="modal moda-new-interview" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crear entrevista</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" class="form-new-interview" >
            <div class="modal-body">
                <div class="form-row">
                    @include('prensa.fields')
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btnFetch">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </form>
        </div>
    </div>
</div>

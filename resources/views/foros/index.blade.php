@extends('layouts.app')
@section('content')
    <div class="container">
        <h4>Listado de sedes</h4>
        <br>
        <button class="btn btn-primary nuevo-foro">Agregar foro</button>
        @include('foros.modal-nuevo-foro')
        @include('foros.modal-edit-foro')
        <br><br>

        <table class="table table-striped table-bordered" id="table-foros">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Tipo</th>
                <th>Sede</th>
                <th>Opciones</th>
            </tr>
            </thead>
        </table>
    </div>
    @endsection

<div class="col-md-12">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-th-list"></i></span>
        </div>
        <input type="text" name="nombre" required class="form-control item-nombre" placeholder="Nombre" aria-label="Nombre" aria-describedby="basic-addon1">
    </div>
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-name"></span>
    </div>
</div>


<div class="col-md-12">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-th-list"></i></span>
        </div>
        <select name="type" class="form-control item-type" id="type">
            <option value="">Seleccione tipo de evento</option>
            <option value="Presencial">Presencial</option>
            <option value="Virtual">Virtual</option>
        </select>
    </div>
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-tipo"></span>
    </div>
</div>

<div class="col-md-12">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1">
            <i class="fas fa-gopuram"></i>
        </span>
        </div>

        <select name="sede_id" class="form-control item-sede_id" id="sede_id">
            <option value="">Seleccione una sede</option>
            @foreach($sedes as $sede)
                <option value="{{ $sede->id }}">{{ $sede->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="col-md-12">
        <span class="badge badge-danger" id="error-sede_id"></span>
    </div>
</div>

<br>

@extends('layouts.app')
@section('content')
    <div class="container">
        <button class="btn btn-primary add-aerolinea" ><i class="fas fa-plane"></i> Agregar</button>
        <br> <br>
        <table class="table table-bordered table-striped" id="table_aerolineas">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Opciones</th>
            </tr>
            </thead>
        </table>
        @include('aerolineas.modal-new-aerolinea')
        @include('aerolineas.modal-edit-aerolinea')
    </div>
    @endsection


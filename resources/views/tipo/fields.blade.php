<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-th-list"></i></span>
    </div>
    <input type="text" name="nombre" required class="form-control item-nombre" placeholder="Nombre" aria-label="Nombre" aria-describedby="basic-addon1">
</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-name"></span>
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text " id="basic-addon1"><i class="fas fa-hand-pointer"></i></span>
    </div>
    <select required name="tipo" id="tipo" class="form-control item-tipo">
        <option value="">Selecciona el tipo de evento</option>
        <option value="Tipo evento">Tipo de evento</option>
        <option value="Programación">Programación</option>
    </select>
</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-name"></span>
</div>

<br>

@extends('layouts.app')
@section('content')
    <div class="container">
        <h4>Listado de tipos </h4>
        <button class="btn btn-success open-modal-nuevo">Agregar nuevo tipo</button>
        @include('tipo.modal-nuevo-tipo')
        @include('tipo.modal-edit-tipo')
        <br><br>
        <table class="table table-striped table-bordered" id="table-tipos">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Tipo</th>
                <th>Opciones</th>
            </tr>
            </thead>
        </table>
    </div>
    @endsection

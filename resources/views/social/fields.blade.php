<div class="col-md-12 mb-3">
    <label for="validationCustomUsername">Seleccionar autor </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen chosen-select item-invitado_id" name="invitado_id[]" placeholder="Seleccione .." data-placeholder="Seleccion invitado(s)" single>
            <option value="">Seleccione una opcion</option>
            @foreach($invitados as $invitado)
                <option value="{{ $invitado->id }}">{{$invitado->name}}</option>
            @endforeach
        </select>
    </div>
</div>


<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Lugar</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-map-marked"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen chosen-select item-lugar_id" name="lugar_id" placeholder="Seleccione .." data-placeholder="Seleccion lugar" single>
            <option value="">Seleccione una opcion</option>
            @foreach($restaurants as $restaurant)
                <option value="{{ $restaurant->id }}">{{$restaurant->name}}</option>
            @endforeach
        </select>
    </div>
    <span class="error-lugar_id badge badge-danger"></span>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Actividad</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-hiking"></i></span>
        </div>
        <input type="text" class="form-control item-actividad" name="actividad" placeholder="Ingresar actividad" aria-describedby="inputGroupPrepend" required>
    </div>
    <span class="error-actividad badge badge-danger"></span>
</div>


<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Fecha</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-clock"></i></span>
        </div>
        <input type="text" class="form-control datepicker item-fecha" name="fecha" placeholder="Ingresar fecha"  required>
    </div>
    <span class="error-fecha badge badge-danger"></span>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Hora inicio de trasalado </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-traslado_ida" name="traslado_ida" placeholder="Ingresar minutos traslado ida"  required>
    </div>
    <span class="error-fecha badge badge-danger"></span>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Hora inicio actividad</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_inicio" placeholder="Ingresar hora inicio" name="hora_inicio" required>
    </div>
    <span class="error-hora_inicio badge badge-danger"></span>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Hora fin actividad</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_fin" placeholder="Ingresar hora fin" name="hora_fin" required>
    </div>
    <span class="error-hora_fin badge badge-danger"></span>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Hora final de trasalado</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-traslado_regreso" name="traslado_regreso" placeholder="Ingresar minutos traslado regreso"  required>
    </div>
    <span class="error-fecha badge badge-danger"></span>
</div>




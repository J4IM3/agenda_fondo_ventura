@extends('layouts.app')
@section('content')
    <div class="container">
        <button class="btn btn-primary new-social"> Crear nuevo</button>
        <br><br>
        <table class="table table-striped table-bordered" id="table_social">
            <thead>
            <tr>
                <th>Id</th>
                <th>Invitado</th>
                <th>Lugar</th>
                <th>Actividad</th>
                <th>Fecha</th>
                <th>Inicio traslado</th>
                <th>Fin traslado</th>
                <th style="width: 180px">Opciones</th>
            </tr>
            </thead>
        </table>
        @include('social.modal-new-social')
        @include('social.modal-edit-event-social')
        @include('social.modal-show-details')
    </div>

    @endsection

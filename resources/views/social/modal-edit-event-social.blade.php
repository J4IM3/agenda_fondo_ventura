<div class="modal fade" id="modal-edit-event-social" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar evento social</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="form-edit-event-social">
                <div class="modal-body">
                    <div class="form-row">
                        <input type="hidden" name="id" class="form-control item-id">
                        @include('social.fields')
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary send-data-user btnFetch">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

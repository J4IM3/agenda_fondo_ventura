<div class="modal" tabindex="-1" role="dialog" id="modal-show-event">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalles de lugar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Invitado</th>
                            <th>Actividad</th>
                            <th>Fecha</th>
                            <th>Traslado ida</th>
                            <th>Inicio actividad</th>
                            <th>Fin actividad</th>
                            <th>Traslado regreso</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><p class="item-social_invitado_name"></p></td>
                            <td><p class="item-actividad"></p></td>
                            <td><p class="item-fecha"></p></td>
                            <td><p class="item-traslado_ida"></p></td>
                            <td><p class="item-hora_inicio"></p></td>
                            <td><p class="item-hora_fin"></p></td>
                            <td><p class="item-traslado_regreso"></p></td>
                    </tr>
                    </tbody>
                    </table>

                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Nombre lugar</th>
                        <th>Direccion</th>
                        <th>Contacto</th>
                        <th>Telefono</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><p class="item-name"></p></td>
                        <td><p class="item-direccion"></p></td>
                        <td><p class="item-contacto"></p></td>
                        <td><p class="item-telefono"></p></td>
                    </tr>
                    </tbody>
                </table>
                </p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

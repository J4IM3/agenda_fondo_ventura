<div class="col-md-12 row">

    <div class="col-md-12">
        <label class="sr-only" for="inlineFormInputGroup">Nombre de sede</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-signature"></i>
                </div>
            </div>
            <input type="text" class="form-control item-name" name="name" id="item-name"
                   placeholder="Ingresar nombre de sede" required>
        </div>
        <div class="col-md-12">
            <span style="color: red" class="item-error-name"></span>
        </div>
    </div>

    <div class="col-md-12">
        <label class="sr-only" for="inlineFormInputGroup">Dirección de sede</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-map-marked"></i>
                </div>
            </div>
            <input type="text" class="form-control item-direction" name="direction" id="item-direction"
                   placeholder="Ingresar dirección de sede" required>
        </div>
        <div class="col-md-12">
            <span style="color: red" class="item-error-direction"></span>
        </div>
    </div>
</div>


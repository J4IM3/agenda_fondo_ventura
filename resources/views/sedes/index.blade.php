@extends('layouts.app')
@section('content')
    <div class="container">
        <h4>Listado de sedes</h4>
        <br>
        <button class="btn btn-primary new-sede">Agregar sede</button>
        @include('sedes.modal-sede')
        <br><br>
        <table class="table table-striped table-bordered" id="table-sedes">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Opciones</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection


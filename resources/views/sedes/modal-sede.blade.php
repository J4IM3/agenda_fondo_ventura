<div class="modal" tabindex="-1" role="dialog" id="modal-sede">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-sede"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" class="form-sede" name="form-sede" id="form-sede">
                <div class="modal-body">
                    <input type="hidden" name="id" class="item-id">
                    <input type="hidden" name="action" class="item-action">
                    @include('sedes.fields')
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="modal" tabindex="-1" id="modal-details-itinerario" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Itinerios detalle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="internacional-items-details">
                    <h5>Vuelo internacional</h5>
                    <table class="table table-bordered table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>Origen internacional</th>
                            <th>Aerolinea</th>
                            <th>Num vuelo internacional</th>
                            <th>Fecha salida origen</th>
                            <th>Hora salida origen</th>
                            <th>Fecha salida regreso</th>
                            <th>Hora salida regreso </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td> <span class="items-origen_internacional"></span> </td>
                            <td> <span class="items-aerolineas_inter"></span> </td>
                            <td> <span class="items-num_vuelo_internacional"></span> </td>
                            <td> <span class="items-fecha_salida_internacional_origen"></span> </td>
                            <td> <span class="items-hora_salida_internacional_origen"></span> </td>
                            <td> <span class="items-fecha_regreso"></span> </td>
                            <td> <span class="items-hora_regreso"></span> </td>
                        </tr>
                        </tbody>
                    </table>
                </div>



                <table class="table table-bordered table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>Origen</th>
                        <th>Aerolinea</th>
                        <th>Fecha llegada</th>
                        <th>Hora llegada</th>
                        <th>Destino salida</th>
                        <th>Fecha y hora salida</th>
                        <th>Hotel</th>
                        <th>Estancia</th>
                        <th>Anfitrion</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td> <span class="items-origen"></span> </td>
                        <td> <span class="items-aerolineas"></span> </td>
                        <td> <span class="items-fecha_llegada"></span> </td>
                        <td> <span class="items-hora_llegada"></span> </td>
                        <td> <span class="items-origen_salida"></span> </td>
                        <td>
                            <span class="items-fecha_salida"></span>
                            <span class="items-hora_salida"></span>
                        </td>
                        <td> <span class="items-hotel-name"></span> </td>
                        <td> <span class="items-estancia"></span> dias </td>
                        <td> <span class="items-nombre"> </span><br>
                             <span class="items-telefono"></span>
                        </td>

                    <tr>
                        <td colspan="9"> Observaciones: <span class="items-observaciones"></span> </td>
                    </tr>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

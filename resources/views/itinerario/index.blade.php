@extends('layouts.app')
@section('content')
    <div class="container">
        <button class="btn btn-primary add-itinerario"> Agregar vuelo-hospedaje </button>
        <br> <br>
        @include('itinerario.nuevo')
        @include('itinerario.modal-new-invitado')
        @include('itinerario.details')
        @include('itinerario.modal-edit-itinerario')
        @include('itinerario.honorarios')
        <table class="table table-bordered table-striped table_itinerarios">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Opciones</th>
            </tr>
            </thead>
        </table>
    </div>
    @endsection

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Honorarios</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="far fa-credit-card"></i></span>
        </div>
        <input type="text" name="honorarios" class="form-control item-honorarios">
    </div>
</div>


<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Monto hotel</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-hotel"></i></span>
        </div>
        <input type="text" name="monto_hotel" class="form-control item-monto_hotel">
    </div>
</div>


<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Monto alimentos</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-utensils"></i></span>
        </div>
        <input type="text" name="monto_alimentos" class="form-control item-monto_alimentos">
    </div>
</div>


<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Viaticos</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-plane"></i></span>
        </div>
        <input type="text" name="monto_viaticos" class="form-control item-monto_viaticos">
    </div>
</div>


<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Monto transporte</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-bus"></i></span>
        </div>
        <input type="text" name="monto_transporte" class="form-control item-monto_transporte">
    </div>
</div>


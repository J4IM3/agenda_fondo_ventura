<div class="col-md-12 row">

    <div class="col-md-3 mb-3">
        <label for="validationCustomUsername">Monto hotel</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-money-check-alt"></i></span>
            </div>
            <input type="text" class="form-control item-monto_hotel" name="monto_hotel" placeholder="" required>
        </div>
    </div>


    <div class="col-md-2 mb-3">
        <label for="validationCustomUsername">Monto comida</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-money-check-alt"></i></span>
            </div>
            <input type="text" class="form-control item-monto_comida" name="monto_comida" required>
        </div>
    </div>

    <div class="col-md-2 mb-3">
        <label for="validationCustomUsername">Monto cena</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-money-check-alt"></i></span>
            </div>
            <input type="text" class="form-control item-monto_cena" name="monto_cena" required>
        </div>
    </div>

    <div class="col-md-2 mb-3">
        <label for="validationCustomUsername">Honorarios</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-money-check-alt"></i></span>
            </div>
            <input type="text" class="form-control item-honorarios" name="honorarios">
        </div>
    </div>

    <div class="col-md-3 mb-3">
        <label for="validationCustomUsername">Monto transporte</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-money-check-alt"></i></span>
            </div>
            <input type="text" class="form-control item-monto_transporte" name="monto_transporte">
        </div>
    </div>


    <div class="col-md-12 mb-3">
        <label for="validationCustomUsername">Observaciones</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-align-left"></i></span>
            </div>
            <textarea class="form-control item-observaciones" rows="3" name="observaciones" placeholder="Observaciones"></textarea>
        </div>
    </div>

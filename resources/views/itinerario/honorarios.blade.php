<div class="modal" tabindex="-1" id="modal-create-honorarios" role="dialog">
    <div class="modal-dialog modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agregar presupuesto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" class="form-new-honorarios" novalidate>
            <div class="modal-body">
                <div class="form-row">
                    <input type="hidden" class="form-control item-invitado_id" name="invitado_id">
                    @include('itinerario.fields_honorarios')
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary international-hide" data-dismiss="modal">Cancelar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Llegada</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
        </div>
        <input type="text" class="form-control datepicker item-fecha_llegada" name="fecha_llegada" placeholder="Ingresar fecha llegada" aria-describedby="inputGroupPrepend" required>
    </div>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Hora llegada</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_llegada" name="hora_llegada" placeholder="Ingresar hora llegada"  required>
    </div>
</div>


<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Salida</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
        </div>
        <input type="text" class="form-control datepicker item-fecha_salida" name="fecha_salida" placeholder="Ingresar fecha salida" required>
    </div>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Hora salida</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_salida" placeholder="Ingresar hora salida" name="hora_salida" required>
    </div>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Aerolinea</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-plane-arrival"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen-select item-aerolinea_id" name="aerolinea_id"  data-placeholder="Seleccion aerolinea" >
            <option value=""></option>
            @foreach($aerolineas as $aerolinea)
            <option value="{{ $aerolinea->id }}">{{$aerolinea->name}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Hotel</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-hotel"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen-select item-hotel" name="hotel" placeholder="Seleccione .." data-placeholder="Seleccion hotel"  >
            <option value=""></option>
            @foreach($hoteles as $hotel)
            <option value="{{ $hotel->id }}">{{$hotel->name}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Estancia</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fab fa-html5"></i> </span>
        </div>
        <input type="text" class="form-control item-estancia" name="estancia" placeholder="Numero de dias" required>
    </div>
</div>


<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Monto hotel</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-money-check-alt"></i></span>
        </div>
        <input type="text" class="form-control item-monto_hotel" name="monto_hotel" placeholder="" required>
    </div>
</div>

<div class="col-md-2 mb-3">
    <label for="validationCustomUsername">Monto comida</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-money-check-alt"></i></span>
        </div>
        <input type="text" class="form-control item-monto_comida" name="monto_comida" required>
    </div>
</div>

<div class="col-md-2 mb-3">
    <label for="validationCustomUsername">Monto cena</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-money-check-alt"></i></span>
        </div>
        <input type="text" class="form-control item-monto_cena" name="monto_cena" required>
    </div>
</div>

<div class="col-md-2 mb-3">
    <label for="validationCustomUsername">Honorarios</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-money-check-alt"></i></span>
        </div>
        <input type="text" class="form-control item-honorarios" name="honorarios">
    </div>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Monto transporte</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-money-check-alt"></i></span>
        </div>
        <input type="text" class="form-control item-monto_transporte" name="monto_transporte">
    </div>
</div>

<div class="col-md-12 mb-3">
    <label for="validationCustomUsername">Observaciones</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-align-left"></i></span>
        </div>
        <textarea class="form-control item-observaciones" rows="3" name="observaciones" placeholder="Observaciones"></textarea>
    </div>
</div>



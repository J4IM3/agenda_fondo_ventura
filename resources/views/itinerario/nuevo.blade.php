<div class="modal create-nuevo-itinerario" tabindex="-1" id="modal-nuevo-itinerario" role="dialog">
    <div class="modal-dialog modal-xl"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Vuelo - Alojamiento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" class="form-new-itinerario" novalidate>
            <div class="modal-body">
                <div class="form-row">
                    @include('itinerario.fields')
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btnFetch">Guardar</button>
                <button type="button" class="btn btn-secondary international-hide" data-dismiss="modal">Cancelar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-create-invitado-from-itinerario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar invitado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="form-save-invitado-from-itinerario" class="form-add-user" autocomplete="off">
                <div class="modal-body">
                    @include('invitados.fields')
                    <input type="time" name="hora" value="11:45:00" max="22:30:00" min="10:00:00" step="1">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary send-data-user btnFetch">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

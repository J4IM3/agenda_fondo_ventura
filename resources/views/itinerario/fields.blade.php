<div class="col-md-12 mb-3">
    <label for="validationCustomUsername">Invitado</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen item-invitado_id" required name="invitado_id" placeholder="Seleccione .." data-placeholder="Seleccion invitado(s)" single>
            <option value="">Elegir invitado</option>
            @foreach($invitados as $invitado)
                <option value="{{ $invitado->id }}">{{$invitado->name}}</option>
            @endforeach
        </select>
        <div class="input-group-append">
            <button class="btn btn-primary add-user-from-itinerario"><i class="fas fa-user-plus"></i></button>
        </div>
        <div class="col-md-12">
            <span id="error-invitado_id" style="color: red"></span>
        </div>

    </div>
</div>

    <div class="col-md-12 mb-3 form-check" >
        <label for="chk_internacional">Vuelo internacional</label>
        <input type="checkbox" class="show-items-internacional" id="chk_internacional" name="show-items-internacional" value="on">
    </div>
    <!-- Campos para vuelos internacionales -->

    @include('itinerario.field_internacional')

    <!-- Fin -->

    <!-- Campos para vuelos internacionales -->
    @include('itinerario.fields-nacional')
    <!-- Fin -->


    @include('itinerario.estancia')


    <div class="col-md-3 mb-3">
        <label for="validationCustomUsername">Anfitrión</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-user-tie"></i> </span>
            </div>
            <select class="form-control form-control-chosen chosen item-anfitrion_id" required name="anfitrion_id" placeholder="Seleccione .." data-placeholder="Selecciona anfitrion" single>
                <option value="">Seleccionar anfitrion</option>
                @foreach($anfitriones as $anfitrion)
                    <option value="{{ $anfitrion->id }}">{{$anfitrion->nombre}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-12 mb-3">
        <label for="validationCustomUsername">Observaciones</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-align-left"></i></span>
            </div>
            <textarea class="form-control item-observaciones" required rows="3" name="observaciones" placeholder="Observaciones"></textarea>
        </div>
    </div>
























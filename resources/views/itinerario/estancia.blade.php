<div class="col-md-12" align="center" >
    <hr> <h5>Estancia</h5> <hr>
</div>
<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Hotel</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-hotel"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen chosen-select item-hotel" required name="hotel" placeholder="Seleccione .." data-placeholder="Seleccion hotel"  >
            <option value=""></option>
            @foreach($hoteles as $hotel)
                <option value="{{ $hotel->id }}">{{$hotel->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-12">
        <span id="error-hotel" style="color: red"></span>
    </div>
</div>


<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Fecha llegada hospedaje</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fab fa-html5"></i> </span>
        </div>
        <input type="text" class="form-control datepicker item-llegada_estancia" required name="llegada_estancia" placeholder="Ingresar fecha llegada al hotel" required>
    </div>
    <div class="col-md-12">
        <span id="error-llegada_estancia" style="color: red"></span>
    </div>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Fecha salida hospedaje</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fab fa-html5"></i> </span>
        </div>
        <input type="text" class="form-control datepicker item-salida_estancia" required name="salida_estancia" placeholder="Ingresar fecha salida al hotel" required>
    </div>
    <div class="col-md-12">
        <span id="error-salida_estancia" style="color: red"></span>
    </div>
</div>

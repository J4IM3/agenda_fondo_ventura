<div class="internacional row">

    <div class="col-md-12" align="center" >
        <hr> <h5>Internacional</h5> <hr>
    </div>


    <div class="col-md-4 mb-3">
        <label for="validationCustomUsername">Origen</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-plane-arrival"></i></span>
            </div>
            <input type="text" class="form-control item-origen_internacional" name="origen_internacional" placeholder="Ingresar origen " required>
        </div>
    </div>

    <div class="col-md-4 mb-3">
        <label for="validationCustomUsername">Aerolinea internacional</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-plane-arrival"></i></span>
            </div>
            <select class="form-control form-control-chosen chosen chosen-select item-aerolinea_internacional_id" name="aerolinea_internacional_id"  data-placeholder="Seleccion aerolinea" >
                <option value=""></option>
                @foreach($aerolineas as $aerolinea)
                    <option value="{{ $aerolinea->id }}">{{$aerolinea->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-4 mb-3">
        <label for="validationCustomUsername">Datos de vuelo inter</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fab fa-500px"></i></span>
            </div>
            <input type="text" class="form-control item-num_vuelo_internacional" name="num_vuelo_internacional" placeholder="Numero vuelo" required>
        </div>
    </div>

    <div class="col-md-3 mb-1">
        <label for="validationCustomUsername">Fecha salida</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
            </div>
            <input type="text" class="form-control datepicker item-fecha_salida_internacional_origen" name="fecha_salida_internacional_origen" placeholder="Ingresar fecha salida" required>
        </div>
    </div>

    <div class="col-md-3 mb-2">
        <label for="validationCustomUsername">Hora Salida</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
            </div>
            <input type="time" class="form-control item-hora_salida_internacional_origen" placeholder="Ingresar hora salida" name="hora_salida_internacional_origen" required>
        </div>
    </div>

    <div class="col-md-3 mb-1">
        <label for="validationCustomUsername">Fecha llegada</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
            </div>
            <input type="text" class="form-control datepicker item-fecha_llegada_internacional_origen" name="fecha_llegada_internacional_origen" placeholder="Ingresar fecha salida" required>
        </div>
    </div>

    <div class="col-md-3 mb-2">
        <label for="validationCustomUsername">Hora llegada</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
            </div>
            <input type="time" class="form-control item-hora_llegada_internacional_origen" placeholder="Ingresar hora salida" name="hora_llegada_internacional_origen" required>
        </div>
    </div>

    <div class="col-md-2 mb-3">
        <label for="validationCustomUsername">Destino retorno</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-plane-arrival"></i></span>
            </div>
            <input type="text" class="form-control item-destino_internacional" name="destino_internacional" placeholder="Ingresar destino retorno" required>
        </div>
    </div>

    <div class="col-md-3 mb-3">
        <label for="validationCustomUsername">Aerolinea retorno internacional</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-plane-arrival"></i></span>
            </div>
            <select class="form-control form-control-chosen chosen chosen-select item-aerolinea_retorno_internacional_id" name="aerolinea_retorno_internacional_id"  data-placeholder="Seleccion aerolinea" >
                <option value=""></option>
                @foreach($aerolineas as $aerolinea)
                    <option value="{{ $aerolinea->id }}">{{$aerolinea->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-3 mb-3">
        <label for="validationCustomUsername">Num vuelo retonor inter</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fab fa-500px"></i></span>
            </div>
            <input type="text" class="form-control item-num_vuelo_retorno_internacional" name="num_vuelo_retorno_internacional" placeholder="Datos vuelo retorno" required>
        </div>
    </div>

    <div class="col-md-2 mb-1">
        <label for="validationCustomUsername">Fecha regreso</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
            </div>
            <input type="text" class="form-control datepicker item-fecha_regreso" name="fecha_regreso" placeholder="Ingresar fecha regreso" required>
        </div>
    </div>


    <div class="col-md-2 mb-2">
        <label for="validationCustomUsername">Hora regreso</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
            </div>
            <input type="time" class="form-control item-hora_regreso" placeholder="Ingresar hora regreso" name="hora_regreso" required>
        </div>
    </div>

</div>

<div class="col-md-12" align="center" >
    <hr> <h5>Nacional</h5> <hr>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Origen</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-plane-departure"></i></span>
        </div>
        <input type="text" class="form-control item-origen " name="origen" placeholder="Ingresar origen" required>
    </div>
    <div class="col-md-12">
        <span id="error-origen" style="color: red"></span>
    </div>
</div>


<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Aerolinea</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-plane-arrival"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen chosen-select item-aerolinea_id" name="aerolinea_id"  data-placeholder="Seleccion aerolinea" >
            <option value=""></option>
            @foreach($aerolineas as $aerolinea)
                <option value="{{ $aerolinea->id }}">{{$aerolinea->name}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Num vuelo</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-ticket-alt"></i></span>
        </div>
        <input type="text" class="form-control item-num_vuelo" name="num_vuelo" placeholder="Numero vuelo" required>
    </div>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Fecha salida origen</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
        </div>
        <input type="text" class="form-control datepicker item-fecha_origen_salida" name="fecha_origen_salida" placeholder="Ingresar fecha salida origen" required>
    </div>
    <div class="col-md-12">
        <span id="error-fecha_salida_origen" style="color: red"></span>
    </div>
</div>


<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Hora salida origen</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_origen_salida" name="hora_origen_salida" placeholder="Ingresar fecha salida origen" required>
    </div>
    <div class="col-md-12">
        <span id="error-hora_salida_origen" style="color: red"></span>
    </div>
</div>


<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Fecha llegada</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
        </div>
        <input type="text" class="form-control datepicker item-fecha_llegada" name="fecha_llegada" placeholder="Ingresar hora llegada" required>
    </div>
    <div class="col-md-12">
        <span id="error-fecha_llegada" style="color: red"></span>
    </div>
</div>

<div class="col-md-2 mb-1">
    <label for="validationCustomUsername">Hora llegada</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_llegada" name="hora_llegada" placeholder="Ingresar fecha llegada" required>
    </div>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Destino</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-bus"></i></span>
        </div>
        <input type="text" class="form-control item-origen_salida" name="origen_salida" placeholder="Ingresar el destino de regreso" required>
    </div>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Datos transporte salida </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-bus"></i></span>
        </div>
        <input type="text" class="form-control item-transporte_retorno" name="transporte_retorno" placeholder="Ingresar el transporte de regreso" required>
    </div>

</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Salida</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
        </div>
        <input type="text" class="form-control datepicker item-fecha_salida" name="fecha_salida" placeholder="Ingresar fecha salida" required>
    </div>

    <div class="col-md-12">
        <span id="error-fecha_salida" style="color: red"></span>
    </div>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Hora salida</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_salida" name="hora_salida" placeholder="Ingresar hora salida" required>
    </div>
</div>

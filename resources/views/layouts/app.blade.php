<style>
    .custom-toggler.navbar-toggler {
        border-color: rgb(201,17,110);
    }
    .custom-toggler .navbar-toggler-icon {
        background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgba(255,102,203, 0.7)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E");
    }
</style>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-inverse navbar-laravel" style="background-color: white; color: black; border-bottom: 5px solid red;">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!--<img src="https://i.ibb.co/jR2M2Gj/imgpsh-fullsize-anim.png" width="120px" height="100px" alt=""> -->
<!--                    <img src="{{ asset('img/logo.png') }}"alt="">-->
                <img src="https://www.filoaxaca.com/images/logo.png"alt="" height="100px">
                </a>
                <button class="navbar-toggler ml-auto custom-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navba -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest

                        @else

                            <li class="nav-item active">
                                <a class="nav-link" href="{{ route('index') }}">Inicio <span class="sr-only">(current)</span></a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Sistema
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{ route('users') }}">Usuarios</a>
                                    <a class="dropdown-item" href="{{ route('anfitrion') }}">Anfitriones</a>
                                </div>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Invitados
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{ route('invitados') }}">Invitados</a>
                                </div>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Catálogo
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{ route('aerolineas') }}">Aerolinea</a>
                                    <a class="dropdown-item" href="{{ route('aliados') }}">Hospedaje - Alimentacion</a>
                                    <a class="dropdown-item" href="{{ route('tipo') }}">Tipo eventos-programación</a>
                                    <a class="dropdown-item" href="{{ route('sedes') }}">Sedes</a>
                                    <a class="dropdown-item" href="{{ route('foro') }}">Foros</a>
                                    <a class="dropdown-item" href="{{ route('evento_prensa') }}">Tipos de eventos prensa</a>
                                    <a class="dropdown-item" href="{{ route('medios') }}">Medios</a>
                                    <a class="dropdown-item" href="{{ route('instituciones') }}">Instituciones</a>
                                </div>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Itinerario
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{ route('itinerario') }}">Logistica invitados</a>
                                    <a class="dropdown-item" href="{{ route('evento') }}">Evento(Filo)</a>
                                    <a class="dropdown-item" href="{{ route('filoando') }}">FILOando</a>
                                    <a class="dropdown-item" href="{{ route('prensa') }}">Prensa</a>
                                    <a class="dropdown-item" href="{{ route('social') }}">Social</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Reportes
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{ route('reportes') }}">Listado de invitados por día.</a>
                                    <a class="dropdown-item" href="{{ route('reporte-invitados-programacion') }}">Invitados por programación.</a>
                                    <a class="dropdown-item" href="{{ route('reporte-foro') }}">Reporte por foro.</a>
                                    <a class="dropdown-item" href="{{ route('reporte-requerimientos-actividad') }}">Reporte actividad-Requerimientos.</a>
                                    <a class="dropdown-item" href="{{ route('reporte-eventos') }}">Reporte eventos.</a>
                                    <a class="dropdown-item" href="{{ route('libros-presentadores') }}">Reporte de libros para presentadores.</a>
                                </div>

                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
            <div id="loadingGifFV"></div>
        </main>
    </div>

</body>
</html>

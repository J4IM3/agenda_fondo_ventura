<br><br>
<table class="customers" border="0" cellspacing="0" cellpadding="0">
    <caption><h4>Prensa</h4></caption>
    <thead>
    <tr>
        <th>Fecha y hora</th>
        <th>Actividad</th>
        <th>Sede</th>
    </tr>
    </thead>
    <tbody>
    @foreach($prensa as $items)
        <tr>
            <td>
                {{ ucwords(Date::parse($items['fecha'])->format('l j F Y')) }} <br>
                {{ $items['hora_inicio'] }}
            </td>
            <td>
                <b>{{ $items['medio'] }}</b> <br>
                {{ $items['tipo'] }}
                <p>
                    @if(!empty($items['participante']))
                        Otros participantes: {{ $items['participante'] }}</p>
                        @endif
                    @if($items['entrevistador'] != ".")
                        <p>Entrevistador: {{ $items['entrevistador'] }}</p>
                    @endif

            </td>
            <td>
                <p>Lugar: {{ $items['lugar'] }}</p>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Example 2</title>


    <style>
        @font-face {
            font-family: 'Barlow', sans-serif;
            src: url('../../fonts/SourceSansPro-Regular.ttf');
        }


        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #0087C3;
            text-decoration: none;
        }

        body {
            position: relative;
            width: 19cm;
            height: 29.7cm;
            color: #555555;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 16px;
            font-family: SourceSansPro;
            margin-top: 0px;
            margin-bottom: 10px;
            margin-right: 120px;
            margin-left: 40px;
        }

        header {
            padding: 5px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        #logo {
            float: right;
            margin-top: 8px;
            margin-right: 8px;
        }
        .saludo{
            margin-left: 25%;

        }

        .fv {
            height: 105px;
            width:85px;
        }

        .filo {
            height: 105px;
            width:125px;
        }

        company {
            float: left;
            text-align: left;
            width: 100%;
        }


        #details {
            margin-bottom: 50px;
        }

        #client {
            padding-left: 6px;
            border-left: 6px solid red;
            float: left;
            width: 100%;

        }

        #client .to {
            color: #777777;
        }

        h2.name {
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
        }

        #invoice {
            float: left;
            text-align: left;
        }

        #invoice h1 {
            color: red;
            font-size: 2.4em;
            line-height: 1em;
            font-weight: normal;
            margin: 0  0 10px 0;
        }

        #invoice .date {
            font-size: 1.1em;
            color: #777777;
        }

        table {
            width: 90%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
            border: black 1px solid;

        }

        table th,
        table td {
            padding: 20px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid black;
        }

        table th {
            white-space: nowrap;
            font-weight: normal;
            background: white;
            color: black;

        }

        table td {
            text-align: right;
            background: white;
            color: black;
            border-bottom-color: #1d2124;
            text-align: left;
        }

        table td h3{
            color: #57B223;
            font-size: 1.2em;
            font-weight: normal;
            margin: 0 0 0.2em 0;
            background: #DDDDDD;
        }


        table .desc {
            text-align: left;
            font-size: 1.0em;
            width: 10%;
        }

        table .unit {
            font-size: 1.0em;
        }

        table .qty {
            font-size: 1.0em;
        }

        table .total {
            color: #FFFFFF;
            font-size: 1.0em;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.0em;
            text-align: left;
        }

        table tbody tr:last-child td {
            border: none;
        }

        table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        table tfoot tr:first-child td {
            border-top: none;
        }

        table tfoot tr:last-child td {
            color: #57B223;
            font-size: 1.4em;
            border-top: 1px solid #57B223;

        }

        table tfoot tr td:first-child {
            border: none;
        }

        #thanks{
            font-size: 2em;
            margin-bottom: 50px;
        }

        #notices{
            padding-left: 6px;
            border-left: 6px solid red;
        }

        #notices .notice {
            font-size: 1.2em;
        }

        footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: fixed;
            bottom: 0;
            border-top: 1px solid #AAAAAA;
            padding: 8px 0;
            text-align: center;
        }
        #redaccion{
            font-size: 15px;
        }
    </style>
</head>
@include('invitados.itinerario.header')
<main>
    <body>
    <div id="details" class="clearfix">
    <div id="details" class="clearfix">
        <div id="client">
            <p>
                <div class="to">Estimade: {{ $invitado->name }}</div>
                {{--<h2 class="name">{{ $invitado->name }}</h2>--}}
            </p>
        </div>
        <div>
            <p id="redaccion" align="justify">
                <br>
                <br><br>
                Recibe la más afectuosa bienvenida de parte del equipo organizador de la 41 Feria Internacional
                del Libro de Oaxaca (FILO), estamos muy agradecidos y contentos de que nos acompañes en
                esta fiesta que cada año preparamos para ti y para todas las y los oaxaqueños. <br>
                Esperamos que tu estancia y participación sean de lo más gratas y te sientas como en casa
                <br>



                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agradecemos tu compañía en la edición 39 de esta fiesta, que cada año preparamos con mucho <br>
                cariño para ti y toda la comunidad de Oaxaca. Esperamos que tu estancia y participación sean de lo más <br>
                gratas y te sientas como en casa.
                <br> <br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Te compartimos que este año la FILO cuenta con más de 500 actividades y casi 400 invitadxs, <br>
                divididos en programas para públicos de diversas edades y ocupaciones.<br><br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Asimismo, que este año la mayoría de la FILO gira en torno a las mujeres, con una serie de
                <br>
                actividades que buscan abordar los más diversos temas con presencia y perspectivas feministas. <br>
                Igualmente, las lenguas indígenas tienen un papel preponderante. En torno a estos y otros temas, la <br>
                FILO busca ser un espacio de diálogo donde todas las voces tengan cabida.
                <br> <br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                A continuación, te presentamos información útil para tu visita y participación en la Feria: <br> <br>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La FILO cuenta con un área que denominamos “El flat”, el cual está a tu disposición como <br>
                zona de descanso, esparcimiento o trabajo. Es importante portar tu gafete para acceder a éste y <br>
                los demás espacios y servicios.
                <br> <br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sugerimos verificar la hora y duración de tus actividades para evitar cualquier contratiempo,
                <br>
                y de manera muy especial, te pedimos tu apoyo para difundir la FILO y tu participación a través de <br>
                los medios a  tu disposición, como tus redes sociales. Agradeceremos mucho esta ayuda.
                <br><br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;En la FILO nos sentimos honradxs de que seas parte de esta 39 edición. Esperamos la <br>
                disfrutes tanto como nosotrxs.
                <br><br>

                Cualquier duda que surja, el equipo de atención a invitadxs puede apoyarte para resolverla.
                <br>
                <div class="saludo">
                    <strong> ¡Gracias por ser parte de esta FILO! </strong>
            <p style="margin-left: 10%"> Vania y Guillermo.</p>
        </div>
        <br>
        <br>
        @include('invitados.itinerario.header')
        <br>
        Información adicional: <br>

        Para tus alimentos, la FILO otorga viáticos, los cuales te fueron entregados a tu llegada. <br>
        Algunas comidas y cenas son ofrecidas por la FILO a fin de que convivamos tanto organizadorxs <br>
        como artistas asistentes en un ambiente festivo y fraterno. <br> <br>

        Te compartimos el itinerario de estos eventos: <br>
        Con tu gafete de invitadx tienes el 10% de descuento en el restaurante Sabina Sabe <br>
        (5 de mayo No. 209, col. Centro). Horario: Lunes a domingo 13:00 a 23:00 hrs, excepto martes.

        </p>
    </div>
    </div>

    @if(!empty($invitado->itinerarioVuelo[0]->origen_internacional ))
        @include('invitados.itinerario.internacional')
    @endif
    <hr style="width: 100%;margin-right: 10%">

    @include('invitados.itinerario.nacional')

    <hr>
    <table>
        <thead>
        <tr>
            <th></th>
        </tr>
        </thead>

    </table>

    <h3 align="center">Hospedaje</h3>
    <p> Hotel de hospedaje: {{ $invitado->itinerarioVuelo[0]->hoteles[0]->name }} </p>
    <p> Dirección:  {{ $invitado->itinerarioVuelo[0]->hoteles[0]->direccion }} </p>
    <p> Fecha de reservación {{ $invitado->itinerarioVuelo[0]->fecha_llegada }} al {{ $invitado->itinerarioVuelo[0]->fecha_salida }}</p>
    <hr>

    <table border="0" cellspacing="0" cellpadding="0">
        <caption><h3>CALENDARIO DE ACTIVIDADES:</h3></caption>
        <thead>
        <tr>
            <th class="no">Fecha y hora</th>
            <th class="desc">Actividad</th>
            <th class="qty">Foro</th>
        </tr>
        </thead>

        <tbody>
        @foreach($actividades as $items)
            <tr>
                <td> {{ $items->fecha }}  - {{   $items->hora }}</td>
                <td> {{ $items->actividad }} </td>
                <td> {{ $items->lugar}} </td>
            </tr>
        @endforeach
        </tbody>
        </tfoot>
    </table>

    <div id="notices">
        <div>Directorio: </div>
        <div class="notice">
            Para cualquier duda o información adicional que requiera, el equipo organizador, de logística y apoyo a invitados queda a su disposición:

            <table border="0" cellspacing="0" cellpadding="0" align="center">
                <caption><h3></h3></caption>
                <thead>
                <tr>
                    <th class="no">Contactos</th>
                    <th class="no">Filo</th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <td>
                        Rebeca Cruz Martínez<br>
                        (044) 9515484458 <br>

                        Gustavo Cruz<br>
                        (044) 9512289744 <br>

                        Elena Vergara<br>
                        (044) 9512433604 <br>

                        Estrella Gil <br>
                        (044) 9511058009 <br>
                    </td>
                    <td>
                        Daniela Belmonte <br>
                        (044) 9515484458 <br>

                        Julio Morales <br>
                        (044) 9512289744 <br>

                        Carmelina Méndez <br>
                        (044) 9512433604 <br>

                        Estrella Gil <br>
                        (044) 9511058009 <br>
                    </td>

                </tr>
                </tbody>
                </tfoot>
            </table>


        </div>
    </div>
    </body>
</main>
<footer>
    @include('invitados.itinerario.footer')
</footer>

</html>


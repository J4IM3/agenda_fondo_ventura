@if(!empty($invitado->itinerarioVuelo->anfitrion->nombre))
    <p align="center"><b>DIRECTORIO</b></p>
    <p>Para cualquier duda o información adicional que requiera, contactar a:</p>
    <p> {{ $invitado->itinerarioVuelo->anfitrion->nombre }} </p>
    <p> {{ $invitado->itinerarioVuelo->anfitrion->telefono }} </p>
@endif


{{--
@if(!empty($invitado->itinerarioVuelo->anfitrion->nombre))

    <table class="customers" border="0" cellspacing="0" cellpadding="0">
        <caption><h4>Datos del anfitrión</h4></caption>
        <thead>
        </thead>
        <tbody>
            <tr>
                <td>Nombre</td>
                <td> <p>{{ $invitado->itinerarioVuelo->anfitrion->nombre }}</p> </td>
            </tr>
            <tr>
                <td>Télefono</td>
                <td><p> {{ $invitado->itinerarioVuelo->anfitrion->telefono }} </p></td>
            </tr>
        </tbody>
    </table>
    @endif
--}}

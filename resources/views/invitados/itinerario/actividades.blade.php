<br><br>

<table class="customers" border="0" cellspacing="0" cellpadding="0">
    <caption><h4>Calendario de actividades</h4></caption>
    <thead>
    <tr>
        <th>Fecha y hora</th>
        <th>Actividad</th>
        <th>Foro</th>
    </tr>
    </thead>
    <tbody>
    @foreach($actividades as $items)
        <tr>
            <td>
                {{ ucwords(Date::parse($items['fecha'])->format('l j F Y')) }} <br>
                {{ $items['hora'] }}
            </td>
            <td>
                <b>{{ $items['nombre_evento'] }}</b> <br>
                {{ $items['descripcion'] }}
                <p>Participa(n): {{ $items['participan'] }}</p>
                @if(!empty($items['modera']))
                    <p>Modera: {{ $items['modera'] }}</p>
                @endif
                @if(!empty($items['presenta']))
                    <p>Presenta: {{ $items['presenta'] }}</p>
                @endif

            </td>
            <td>
                {{ $items['sede'] }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>




{{--<table class="customers" border="0" cellspacing="0" cellpadding="0">
    <caption><h4>Calendario de actividades</h4></caption>
    <thead>
    </thead>
    <tbody>

        @foreach($actividades as $items)
            <tr>
                <td >
                <strong>{{ ucwords(Date::parse($items->fecha)->format('l j F ')) }}</strong> <br>
                Hora: {{ $items->hora }} <br>
                Foro: {{ $items->lugar }} <br>
                    @if(!empty($items->ciclo))
                        Ciclo: {{$items->ciclo  }} <br>
                    @endif

                    @if(!empty($items->tipo_programa))
                        {{$items->tipo_programa }} <br>
                    @endif

                    @if($items->tipo_evento == "Mesa")
                        {{ $items->tipo_evento }}:
                    @endif

                        {{ $items->actividad }} <br>
                        @if(!empty($items->descripcion))
                          Descripcion: {{ $items->descripcion }} <br>
                          @endif
                        Participan: {{ $items->invitados }} <br>
                    @if(!empty($items->moderadores))
                        Modera: {{$items->moderadores }} <br>
                    @endif

                    @if(!empty($items->presentadores))
                        Presenta: {{$items->presentadores }} <br>
                    @endif
                </td>
            </tr>
            @endforeach
    </p>
    </tbody>
</table>--}}

<p align="center"><b>Hospedaje</b></p>
<p>Hotel: {{ $invitado->itinerarioVuelo->hoteles[0]->name }}</p>
<p>Dirección: {{ $invitado->itinerarioVuelo->hoteles[0]->direccion }}</p>
<p>Fechas de reservación:
    @if(!empty($invitado->itinerarioVuelo->llegada_estancia))
        {{ ucwords(Date::parse($invitado->itinerarioVuelo->llegada_estancia)->format('l j F Y ')) }} al
        {{ ucwords(Date::parse($invitado->itinerarioVuelo->salida_estancia)->format('l j F Y')) }}</td>
    @endif</p>



{{--
<table class="customers" border="0" cellspacing="0" cellpadding="0">
    <caption><h4>Hospedaje</h4></caption>
    <thead>
    </thead>
    <tbody>
    <tr>
        <td align="center">Hotel de hospedaje: </td>
        <td align="center">{{ $invitado->itinerarioVuelo->hoteles[0]->name }}</td>
    </tr>

    <tr>
        <td align="center">Dirección</td>
        <td align="center">{{ $invitado->itinerarioVuelo->hoteles[0]->direccion }}</td>
    </tr>

    <tr>
        <td align="center">Fecha de reservación</td>
        <td align="center">
            @if(!empty($invitado->itinerarioVuelo->llegada_estancia))
                {{ ucwords(Date::parse($invitado->itinerarioVuelo->llegada_estancia)->format('l j F ')) }} al
                {{ ucwords(Date::parse($invitado->itinerarioVuelo->salida_estancia)->format('l j F ')) }}</td>
        @endif
    </tr>
</table>
--}}

<table class="customers" border="0" cellspacing="0" cellpadding="0">
    <caption></caption>
    <thead>
    <tr>
        <th>Origen</th>
        <th>Fecha</th>
        <th>Salida</th>
        <th>Llegada</th>
        <th>Vuelo</th>
    </tr>
    </thead>
    <tbody>
        @if(!empty($invitado->itinerarioVuelo->origen_internacional))
            <tr>
                <td>{{ $invitado->itinerarioVuelo->origen_internacional }} -  {{ $invitado->itinerarioVuelo->origen }}</td>
                <td> {{ ucwords(Date::parse($invitado->itinerarioVuelo->fecha_salida_internacional_origen)->format('l j F Y ')) }} </td>
                <td> {{ $invitado->itinerarioVuelo->hora_salida_internacional_origen }} </td>
                <td> {{ $invitado->itinerarioVuelo->hora_llegada_internacional_origen }} </td>
                <td> {{ $invitado->itinerarioVuelo->aerolineasInter->name }} # {{ $invitado->itinerarioVuelo->num_vuelo_internacional }}</td>
            </tr>
        @endif

        @if(!empty($invitado->itinerarioVuelo->origen))
            <tr>
                <td>{{ $invitado->itinerarioVuelo->origen }} - Oaxaca</td>
                <td>{{ ucwords(Date::parse($invitado->itinerarioVuelo->fecha_llegada)->format('l j F Y')) }}</td>
                <td>{{ $invitado->itinerarioVuelo->hora_origen_salida }}</td>
                <td>{{ $invitado->itinerarioVuelo->hora_llegada }}   </td>
                <td>{{ $invitado->itinerarioVuelo->aerolineas->name }}#{{ $invitado->itinerarioVuelo->num_vuelo }}</td>
            </tr>
            @endif

        @if(!empty($invitado->itinerarioVuelo->origen_salida))
            <tr>
                <td> Oaxaca - {{ $invitado->itinerarioVuelo->origen }}</td>
                <td>{{ ucwords(Date::parse($invitado->itinerarioVuelo->fecha_salida)->format('l j F Y')) }}</td>
                <td>{{ $invitado->itinerarioVuelo->hora_salida }}</td>
                <td></td>
                <td>{{ $invitado->itinerarioVuelo->transporte_retorno }}</td>
            </tr>
            @endif

        @if(!empty($invitado->itinerarioVuelo->origen_internacional))
            <tr>
                <td> {{ $invitado->itinerarioVuelo->origen }} - {{ $invitado->itinerarioVuelo->origen_internacional }} </td>
                <td> {{ ucwords(Date::parse($invitado->itinerarioVuelo->fecha_regreso)->format('l j F Y ')) }} </td>
                <td> {{ $invitado->itinerarioVuelo->hora_regreso }} </td>
                <td> </td>
                <td> {{ !empty($invitado->itinerarioVuelo->aerolineasInterRetorno->name) ? $invitado->itinerarioVuelo->aerolineasInterRetorno->name. '#'. $invitado->itinerario[0]->num_vuelo_retorno_internacional   : '' }} </td>
            </tr>
        @endif
    </tbody>
</table>



{{--
@if(!empty($invitado->itinerarioVuelo))
    <caption><h4>{{ $invitado->itinerarioVuelo->origen }} - OAXACA </h4></caption>
    <thead>
    </thead>
    <tbody>
    <tr>
        <td align="center">Fecha y hora  de salida: </td>
        <td align="center">{{ ucwords(Date::parse($invitado->itinerarioVuelo->fecha_llegada)->format('l j F ')) }} - {{ $invitado->itinerarioVuelo->hora_llegada }}</td>
    </tr>

    <tr>
        <td align="center">Vuelo</td>
        <td align="center"> {{ $invitado->itinerarioVuelo->aerolineas->name }} {{ $invitado->itinerarioVuelo->num_vuelo }}</td>
    </tr>

    <tr>
        <td align="center">Fecha y hora de regreso</td>
        <td align="center">{{ ucwords(Date::parse($invitado->itinerarioVuelo->fecha_salida)->format('l j F ')) }} - {{ $invitado->itinerarioVuelo->hora_salida }}</td>
    </tr>
@endif--}}

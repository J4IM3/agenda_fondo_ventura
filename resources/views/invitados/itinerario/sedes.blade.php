<h3>Sedes en Oaxaca Centro</h3>
<table class="customers" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <p><b>Casa de la Cultura Oaxaqueña</b></p>
            <p>González Ortega No. 403, Centro, Oaxaca de Juárez.</p>
        </td>
        <td>
            <p><b>Museo de la Filatelia de Oaxaca (MUFI)</b></p>
            <p>Calle de la Constitución No.201, Centro, Oaxaca de Juárez.</p>
        </td>
    </tr>
    <tr>
        <td>
            <p><b>Foro El Huaje</b></p>
            <p>Av. Independencia No.1001, Planta Alta, Centro, Oaxaca de Juárez.</p>
        </td>

        <td>
            <p><b>La Proveedora Alcalá</b></p>
            <p>Macedonio Alcalá No. 400, Centro, Oaxaca de Juárez.</p>
        </td>
    </tr>
    <tr>
        <td>
            <p><b>Matamoros 404</b></p>
            <p>Matamoros No. 404, Centro, Oaxaca de Juárez.</p>
        </td>
        <td>
            <p><b>Instituto de Artes Gráficas de Oaxaca (IAGO)</b></p>
            <p>Macedonio Alcalá No. 507, Centro, Oaxaca de Juárez.</p>
        </td>
    </tr>
    <tr>
        <td>
            <p><b>Biblioteca Pública "Margarita Maza de Juárez</b></p>
            <p>Av. Macedonio No.200 esquina Av. Morelos, Centro Oaxaca de Juárez.</p>
        </td>
        <td>
            <p><b>Museo Textil de Oaxaca</b></p>
            <p>Hidalgo No. 917, Centro, Oaxaca</p>
        </td>
    </tr>
    <tr>
        <td>
            <p><b>Selva Oaxaca Cocktail Bar</b></p>
            <p>Macedonio Alcalá No. 403 interior 6, Centro, Oaxaca de Juárez.</p>
        </td>
        <td>
            <p><b>1450 Estación de las Artes</b></p>
            <p>Macedonio Alcalá No. 305 int. 3, Centro, Oaxaca de Juárez.</p>
        </td>
    </tr>
    <tr>
        <td>
            <p><b>Teatro Macedonio Alcalá</b></p>
            <p>Av. de la Independencia 900, Centro,Oaxaca de Juárez</p>
        </td>
        <td>
            <p><b>Bodega Quetzalli</b></p>
            <p>Murguía No. 400, Centro, Oaxaca de Juárez.</p>
        </td>
    </tr>
    <tr>
        <td>
            <p><b>La Proveedora Reforma</b></p>
            <p>H. Escuela Naval Militar No. 518, Reforma, Oaxaca de Juárez.</p>
        </td>
        <td>
            <p><b>Espacio Chipi Chipi</b></p>
            <p>Carretera Monte Albán 800, Colonia San Juan Chapultepec</p>
        </td>
    </tr>
    <tr>
        <td>
            <p><b>Estudio LaPiztola</b></p>
            <p>Av. Fuerza Aérea Mexicana No. 1208, Reforma, Oaxaca de Juárez.</p>
        </td>
        <td>
            <p><b>Central de Abastos </b></p>
            <p> Local 15, Módulo L, Zona Modular, Central de Abastos, Oaxaca de Juárez  </p>
        </td>
    </tr>
</table>

<table class="customers" border="0" cellspacing="0" cellpadding="0">
    <caption><h4>Sedes en comunidades</h4></caption>
    <tr>
        <td>
            <p><b>Centro de las Artes de San Agustín.</b></p>
            <p>Av. Independencia s/n, Vista Hermosa, San Agustín Etla.</p>
        </td>
        <td>
            <p><b>Centro Cultural Ex Hacienda San José Hidalgo</b></p>
            <p> Avenida Independencia No. 31, esquina Camino Antiguo a la Ex Hacienda, San José Hidalgo</p>
        </td>
    </tr>

    <tr>
        <td>
            <p><b>Centro Comunitario de Teotitlán del Valle</b></p>
            <p>Av Al. Hidalgo, Tecutlan, 70420 Teotitlán del Valle.</p>
        </td>
        <td>
            <p><b>Biblioteca Donají en Zaachila.</b></p>
            <p>Calle Bitopaa S/N, barrio Lexio Villa de Zaachila</p>
        </td>
    </tr>
    <tr>
        <td>
            <p><b>Centro Social Ayuuk .</b></p>
            <p>Calle Neptäjk Nº12 Barrio de San Pablo Ayutla, Mixe, Oaxaca</p>
        </td>
        <td>
            <p><b>Centro Cultural María Taurina </b></p>
            <p>Av. Juárez #72, Cuarta Sección, Juchitán de Zaragoza, Oaxaca</p>
        </td>
    </tr>
</table>

<p>FILO Jóvenes en:</p>
<p>Telebachilleratos comunitarios</p>
<p>1.	No.049, Santa María Tonameca, Cerro Gordo</p>
<p>2.	No. 024, San Sebastián Coatlán, San José́ Cieneguilla</p>
<p>3.	No. 064, San Miguel Quetzaltepec, San Juan Bosco Chuxnaban</p>
<p>4.	No. 025, Santa Inés Del Monte, Santa Inés Del Monte</p>
<p>5.	No. 084, San Agustín Etla, San Agustín Etla</p>
<p>6.	No. 100, Yogana, Yogana</p>



 <p id="redaccion">
     Recibe la más afectuosa bienvenida de parte del equipo organizador de la 41 Feria
     Internacional del Libro de Oaxaca (FILO), estamos muy agradecidos y contentos de que nos
     acompañes en esta fiesta que cada año preparamos para ti y para todas las y los oaxaqueños.
     <br>
     Esperamos que tu estancia y participación sean de lo más gratas y te sientas como en casa
 </p>
<p id="redaccion">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La FILO este año no se concentrará en un solo lugar, sino que ocupará varios museos,
    bibliotecas, centros culturales y espacios públicos de la Ciudad de Oaxaca y municipios
    conurbados y algunas comunidades al interior del estado a fin de rehabitarlos y reactivarlos
    a través de nuestras actividades.
</p>

 <p id="redaccion">
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Para garantizar que tu visita se realice sin ningún contratiempo, a continuación
     encontrarás reunida información útil sobre vuelos, hospedaje, y por supuesto, de tu
     participación en el programa de actividades, te comentamos también que un(a) representante
     de la FILO te estará esperando en el aeropuerto para llevarte a tu hotel y estará atento(a) a
     cualquier duda.
 </p>
 <p id="redaccion">
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sugerimos verifiques la hora y duración de las actividades en que participarás para evitar
     tener cualquier contratiempo. Y de manera muy especial, te pedimos tu apoyo para
     difundir la FILO y tu participación en ella a través de los medios a tu disposición, como tus
     redes sociales. Agradeceremos mucho esta ayuda.
 </p>

<div class="saludo">
    <p align="center"> <strong> ¡Gracias por ser parte de esta FILO! </strong> </p>
    <p align="center"> <b>Vania Reséndiz Cerna</b></p>
</div>


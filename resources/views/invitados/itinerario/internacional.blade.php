<table class="customers" border="0" cellspacing="0" cellpadding="0">
    <caption><h3></h3></caption>
    @if(!empty($invitado->itinerarioVuelo->origen_internacional))
    <caption><h4></h4></caption>
    <thead>
    <tr>
        <th>Origen</th>
        <th>Fecha</th>
        <th>Salida</th>
        <th>Llegada</th>
        <th>Vuelo</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td> {{ $invitado->itinerarioVuelo->origen_internacional }} -  {{ $invitado->itinerarioVuelo->origen }} </td>
        <td> {{ ucwords(Date::parse($invitado->itinerarioVuelo->fecha_salida_internacional_origen)->format('l j F Y ')) }} </td>
        <td> {{ $invitado->itinerarioVuelo->hora_salida_internacional_origen }} </td>
        <td> </td>
        <td> {{ $invitado->itinerarioVuelo->aerolineasInter->name }} # {{ $invitado->itinerarioVuelo->num_vuelo_internacional }}</td>
    </tr>
    <tr>
        <td> {{ $invitado->itinerarioVuelo->origen }} - {{ $invitado->itinerarioVuelo->origen_internacional }} </td>
        <td> {{ ucwords(Date::parse($invitado->itinerarioVuelo->fecha_regreso)->format('l j F Y ')) }} </td>
        <td> {{ $invitado->itinerarioVuelo->hora_regreso }} </td>
        <td> </td>
        <td> </td>
    </tr>
    </tbody>
    @endif
    </tfoot>
</table>

{{--
<td align="center" >
    Fecha y hora  de salida:
</td>
<td align="center">
    {{ ucwords(Date::parse($invitado->itinerarioVuelo->fecha_salida_internacional_origen)->format('l j F ')) }}
    a las {{ $invitado->itinerarioVuelo->hora_salida_internacional_origen }}
</td>
</tr>


<tr>
    <td align="center">Vuelo:</td>
    <td align="center">
        Clave: {{ $invitado->itinerarioVuelo->aerolineasInter->name }} - {{ $invitado->itinerarioVuelo->num_vuelo_internacional }}
    </td>
</tr>

<tr>
    <td align="center">
        Fecha y hora de regreso:
    </td>
    <td align="center">
        {{ ucwords(Date::parse($invitado->itinerarioVuelo->fecha_regreso)->format('l j F ')) }}
        a las
        {{ $invitado->itinerarioVuelo->hora_regreso }}
    </td>
</tr>--}}

<style>
    @font-face {
        font-family: 'Barlow', sans-serif;
        src: url('../../fonts/SourceSansPro-Regular.ttf');
    }

    @page { margin: 100px 45px }
    header { position: fixed; top: -85px; left: 0px; right: 0px; height:100px; width: 19cm;}
    footer { position: fixed; bottom: -80px; left: 0px; right: 0px; height: 60px; width: 19cm; }



    .clearfix:after {
        content: "";
        display: table;
        clear: both;
    }

    #logo {
        float: right;
        margin-top: 10px;
        margin-right: 0px;
        margin-bottom:10px ;
    }

    #pie{
        width: 100%;
        height: 70px;
        margin-bottom:20px ;
    }

    .pieImg{
        width:280px;
        height:80px;
        padding-bottom: 180px;

    }

    body {
        position: relative;
        width: 16cm;
        height: 29.7cm;
        color: #555555;
        background: #FFFFFF;
        margin-top: 0px;
        margin-bottom: 10px;
        margin-right: 15px;
        margin-left: 45px;
    }



    #details {
        margin-top: 10px;
        margin-bottom: 20px;
    }

    #client {
        padding-left: 6px;
        border-left: 6px solid red;
        float: left;
        width: 100%;

    }

    #client .to {
        color: #777777;
    }

    h2.name {
        font-size: 1.4em;
        font-weight: normal;
        margin: 0;
    }


    /*.saludo{
        margin-left: 25%;
    }*/

    .fv {
        height: 105px;
        width:85px;
    }

    .filo {
        height: 105px;
        width:125px;
    }


    #redaccion{
        font-style:normal;
        /*HERMOSA PROPIEDAD CSS*/
        font-varianxt:small-caps;/*CAMBIA TODO A MAYUSCULA Y LE PONE UN ESTILO TIPO LIBRO*/
        /*font-weight:lighter;/*normal,bold,ligh,lighter,100-900*/
        font-size:100%;
        line-height:20px;/*Separa cada linea de texto  de la otra que sigue*/
        font-family:{{asset('fonts/barlow/Barlow-Light.otf')}};
        direction: rtl;
        text-align: justify;
        letter-spacing: 0;
    }


    .customers {
        border-collapse: collapse;
        width: 100%;
    }

    .customers td, .customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    .customers tr:nth-child(even){background-color: white;}

    .customers tr:hover {background-color: white;}

    .customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: white;
        color: black;
    }
</style>

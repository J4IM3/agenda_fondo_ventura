<html>
<head>
    @include('invitados.itinerario.estilo')
</head>
<body>
<header>
    @include('invitados.itinerario.header')
</header>
<footer>
    @include('invitados.itinerario.footer')
</footer>
<main>
    <div align="center"><h3><b>Itinerario</b></h3></div>
    <div id="details" class="clearfix">
        <div id="client">
            <div class="to">Estimade: {{ $invitado->name }}</div>
            {{--<h2 class="name"></h2>--}}
        </div>
    </div>
    @include('invitados.itinerario.texto')
    <div style="page-break-after:always;"></div>
    <br>
    @if(!empty($invitado->itinerarioVuelo->origen_internacional) or !empty($invitado->itinerarioVuelo->origen))
        <p><b>Vuelos: </b></p>
        <br>
    @endif

    @if(!empty($invitado->itinerarioVuelo->origen_internacional))
        {{--@include('invitados.itinerario.internacional')--}}
        <br>
    @endif


    @if(!empty($invitado->itinerarioVuelo->origen))
        @include('invitados.itinerario.nacional')
    @endif


    @if(!empty( $invitado->itinerarioVuelo->hoteles[0]->name ))
        @include('invitados.itinerario.hospedaje')
        <br>
    @endif

    @if(!empty($actividades))
        @include('invitados.itinerario.actividades')
        <br>
        @endif

    @if(!empty($prensa))
        <div style="page-break-after:always;"></div>
        @include('invitados.itinerario.prensa')
        <br>
        @endif

    @if(!empty($actividades))
        @include('invitados.itinerario.anfitrion')
        <br>
        @endif
    @if($visibleSede == 1)
        <div style="page-break-after:always;"></div>
        @include('invitados.itinerario.sedes')
        @endif
</main>

</body>
</html>

<div class="col-md-6 input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" name="name"  class="form-control item-name" placeholder="Introduce nombre del invitado" aria-label="Nombre" aria-describedby="basic-addon1">
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-name"></span>
    </div>
</div>



<div class="col-md-6 input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" name="slug"  class="form-control item-slug" placeholder="Introduce su nombre artistico" aria-label="Nombre" aria-describedby="basic-addon1">
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-slug"></span>
    </div>
</div>


<div class="input-group mb-3 col-md-6">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-at"></i></span>
    </div>
    <input type="email" name="email"  class="form-control item-email"  placeholder="Introduce email" aria-label="email" aria-describedby="basic-addon1">
    <div class="col-md-12 text-center mb-2">
        <span class="badge badge-danger error-email" id="error-email" ></span>
    </div>
</div>

<div class="input-group mb-3 col-md-6">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1">
            <i class="far fa-mobile"></i>
        </span>
    </div>
    <input type="phone" name="phone"  class="form-control item-phone" placeholder="Introduce teléfono"
           aria-label="phone" aria-describedby="basic-addon1" id="phone-inv">
    <div class="col-md-12 text-center mb-2">
        <span class="badge badge-danger error-phone" id="error-phone"></span>
    </div>
</div>


<div class="col-md-6 mb-3">
    <select class="form-control-chosen  item-sexo" name="sexo"  required = "required" data-placeholder="Seleccionar sexo">
        <option value="">Seleccionar sexo</option>
        <option value="M">F</option>
        <option value="H">M</option>
    </select>
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-phone"></span>
    </div>
</div>

<div class="col-md-6 mb-3">
    <select class="form-control-chosen  item-residencia" name="residencia"  required = "required"
            data-placeholder="Seleccionar lugar de residencia">
        <option value="">Seleccionar lugar de residencia</option>
        <option value="Local">Local</option>
        <option value="Foraneo">Foraneo</option>
    </select>
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-phone"></span>
    </div>

</div>



<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-align-justify"></i></span>
    </div>
    <textarea class="form-control item-semblanza" name="semblanza" id="exampleFormControlTextarea1" rows="3" placeholder="Introducir semblanza"></textarea>
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-semblanza"></span>
    </div>
</div>



<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-images"></i></span>
    </div>
    <div class="custom-file">
        <input type="file" name="avatar" class="custom-file-input" id="inputGroupFile01">
        <label class="custom-file-label" for="inputGroupFile01">Elegir foto invitado</label>
    </div>
</div>
<div class="col-md-3">
    <img src="#" class="avatarImg" alt="" width="180px" height="180px">
    <!--http://soporte.venturacsc.com/images/users/profile.png-->
</div>
<div class="deleteImage col-md-6"></div>




<br>






@extends('layouts.app')
@section('content')
    <div class="container">
        <button class="btn btn-primary add-invitado"><i class="fas fa-user">Agregar invitado</i></button>
        <br><br>

        <table class="table table-bordered table-striped" id="table_invitado">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Opciones</th>
            </tr>
            </thead>
        </table>
        @include('invitados.modal-new-invitado')
        @include('invitados.modal-edit-invitado')
        @include('itinerario.details')
        @include('invitados.model-details')
    </div>
    @endsection

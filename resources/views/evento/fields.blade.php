<div class="col-md-4 input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt"></i></span>
    </div>
    <input type="text" name="fecha"  class="form-control item-fecha datepicker" required placeholder="Ingresar fecha" aria-label="Nombre" aria-describedby="basic-addon1">
    <div class="col-md-12">
        <span  class="error-fecha" style="color: red;"> </span>
    </div>
</div>


<div class="col-md-4 input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1">Hora inicio</span>
    </div>
    <input type="time" name="hora_inicio"  class="form-control item-hora_inicio" required placeholder="Ingresar hora inicio" aria-label="Nombre" aria-describedby="basic-addon1">
    <div class="col-md-12">
        <span  class="error-hora_inicio" style="color: red;"> </span>
    </div>
</div>

<div class="col-md-4 input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1">Hora fin</span>
    </div>
    <input type="time" name="hora_fin"  class="form-control item-hora_fin " required placeholder="Ingresar hora final" aria-label="Nombre" aria-describedby="basic-addon1">
    <div class="col-md-12">
        <span  class="error-hora_fin" style="color: red;"> </span>
    </div>
</div>

<div class="col-md-12 mb-3">
    <label for="validationCustomUsername"></label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen item-invitados" name="invitado_id[]" required placeholder="Seleccione .." data-placeholder="Seleccion invitado(s)" multiple>
            @foreach($invitados as $invitado)
                <option value="{{ $invitado->id }}">{{$invitado->slug}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-12">
        <span  class="error-invitado_id" style="color: red;"> </span>
    </div>
</div>

<div class="col-md-6 input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" name="nombre_evento"  required class="form-control item-nombre_evento" placeholder="Ingresar nombre evento" aria-label="Nombre" aria-describedby="basic-addon1">
</div>

<div class="col-md-6 input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" name="ciclo"  class="form-control item-ciclo" placeholder="Ingresar ciclo" aria-label="Nombre" aria-describedby="basic-addon1">
</div>


<div class="col-md-12 input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" name="descripcion"  class="form-control item-descripcion" required placeholder="Ingresar descripcion" aria-label="Nombre" aria-describedby="basic-addon1">
    <div class="col-md-12">
        <span  class="error-descripcion" style="color: red;"> </span>
    </div>
</div>


<div class="col-md-3 mb-3">
    <label for="validationCustomUsername"></label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen item-tipo_id"  required="required" name="tipo_id" data-placeholder="Seleccione tipo de evento">
            @foreach($tipos as $tipo)
                <option data-name=" {{ $tipo->nombre }} "value="{{ $tipo->id }}">{{$tipo->nombre}}</option>
            @endforeach
        </select>
        <div class="col-md-12">
            <span  class="error-tipo_id" style="color: red;"> </span>
        </div>
    </div>


</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername"></label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen item-programacion_id" required="required"  name="programacion_id" data-placeholder="Seleccionar programa ..">
            @foreach($programaciones as $programacion)
                <option value="{{ $programacion->id }}">{{$programacion->nombre}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-12">
        <span  class="error-programa_id" style="color: red;"> </span>
    </div>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername"></label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen item-formato" required="required" name="formato" data-placeholder="Selecciona formato ?" placeholder="Selecciona formato">
            <option value=""> Selecciona formato </option>
            <option value="Virtual">Virtual</option>
            <option value="Presencial">Presencial</option>
            <option value="Hibrido">Hibrido</option>
        </select>
    </div>
</div>

<div class="col-md-6 mb-3 moderadores" >
    <label for="validationCustomUsername"></label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen item-moderadores" name="moderador_id[]"  data-placeholder="Seleccion moderador(s)" multiple>
            @foreach($invitados as $invitado)
                <option value="{{ $invitado->id }}">{{$invitado->name}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-6 mb-3 presentadores" >
    <label for="validationCustomUsername"></label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen item-presentadores" name="presentador_id[]"
                placeholder="Seleccione .." data-placeholder="Seleccion presentador(s)" multiple>
            @foreach($invitados as $invitado)
                <option value="{{ $invitado->id }}">{{$invitado->name}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-12">
    <div class="form-check">
        <input type="checkbox" class="form-check-input " id="colaborador">
        <label class="form-check-label" for="colaborador">Agregar colaborador</label>
    </div>
</div>


<div class="col-md-10 input-group mb-3 colaboracion">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" name="colaborador"  class="form-control item-colaborador" placeholder="Ingresar colaborador"
           data-placeholder="Ingresar colaborador" aria-label="Nombre" aria-describedby="basic-addon1">
</div>
<div class="col-md-12 mb-3">
    <label for="validationCustomUsername"></label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control item-evento_foro_id form-control-chosen"
                name="foro_id[]" required="required"
                data-placeholder="Seleccionar sede"
                multiple>
            @foreach($foros as $foro)
                <option value="{{ $foro->id }}"> {{ $foro->nombre }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-12">
        <span  class="error-foro_id" style="color: red;"> </span>
    </div>
</div>


<div class="col-md-12 input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class=""></i></span>
    </div>
    <textarea class="form-control item-requirimientos" rows="3" id="requirimientos" name="requirimientos" placeholder="Ingresar requerimientos"></textarea>
</div>

@include('evento.libros-sugeridos')






@extends('layouts.app')
@section('content')
    <div class="container container-fluid" style="max-width: 90%">
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary new-event">Nuevo evento</button>
            </div>
        </div>
        <br>
        <div class="table-responsive">
        <table class="table table-bordered table-striped" id="table_eventos">
            <thead>
                <tr>
                    <th style="width: 3%">id</th>
                    <th style="width: 9%;">Fecha</th>
                    <th style="width: 5%;">Hora</th>
                    <th style="width: 10%;">Foro</th>
                    <th style="width: 9%;">Programación</th>
                    <th style="width: 10%;">Evento</th>
                    <th style="width: 10%;">Descripcion</th>
                    <th style="width: 10%;">Participantes</th>
                    <th style="	width: 12%;">Acciones</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th style="width: 3%">id</th>
                    <th style="width: 9%;">Fecha</th>
                    <th style="width: 5%;">Hora</th>
                    <th style="width: 10%;">Foro</th>
                    <th style="width: 9%;">Programación</th>
                    <th style="width: 10%;">Evento</th>
                    <th style="width: 10%;">Descripcion</th>
                    <th style="width: 10%;">Participantes</th>
                    <th style="	width: 12%;">Acciones</th>
                </tr>
            </tfoot>
        </table>
        </div>
        @include('evento.modal-new-event')
        @include('evento.modal-edit-event')
        @include('evento.details')
        @include('evento.prueba-sonido')
    </div>
    @endsection

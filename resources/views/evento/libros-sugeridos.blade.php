<table class="table table-striped" id="table_books" border="1">
    <thead>
        <tr>
            <th width="70%">Titulo</th>
            <th>Es para promotor</th>
            <th>Cantidad</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <input type="text" class="form-control item-title1" name="title[1]" placeholder="Ingresar titulo, autor y editorial">
        </td>
        <td>
            <input type="checkbox" class="form-control item-for_promotor1 for_promotor"
                   onclick="updatePromotor('1',this)" name="title_for_promotor[1]">
        </td>
        <td>
            <input type="text" class="form-control quantity1" name="quantity[1]" placeholder="Ingresar cantidad" readonly>
        </td>
    </tr>

    <tr>
        <td>
            <input type="text" class="form-control item-title2" name="title[2]" placeholder="Ingresar titulo, autor y editorial">
        </td>
        <td>
            <input type="checkbox" class="form-control item-for_promotor2 for_promotor"
                   onclick="updatePromotor('2',this)" name="title_for_promotor[2]">
        </td>
        <td>
            <input type="text" class="form-control quantity2" name="quantity[2]"  placeholder="Ingresar cantidad" readonly>
        </td>
    </tr>

    <tr>
        <td>
            <input type="text" class="form-control item-title3" name="title[3]" placeholder="Ingresar titulo, autor y editorial">
        </td>
        <td>
            <input type="checkbox" class="form-control item-for_promotor3 for_promotor"
                   onclick="updatePromotor('3',this)" name="title_for_promotor[3]">
        </td>
        <td>
            <input type="text" class="form-control quantity3" name="quantity[3]" placeholder="Ingresar cantidad" readonly>
        </td>
    </tr>
    <tr>
        <td>
            <input type="text" class="form-control item-title4" name="title[4]" placeholder="Ingresar titulo, autor y editorial">
        </td>
        <td>
            <input type="checkbox" class="form-control item-for_promotor4 for_promotor"
                   onclick="updatePromotor('4',this)" name="title_for_promotor[4]">
        </td>
        <td>
            <input type="text" class="form-control quantity4" name="quantity[4]" placeholder="Ingresar cantidad" readonly>
        </td>
    </tr>
    <tr>
        <td>
            <input type="text" class="form-control item-title5" name="title[5]" placeholder="Ingresar titulo, autor y editorial">
        </td>
        <td>
            <input type="checkbox" class="form-control item-for_promotor5 for_promotor"
                   onclick="updatePromotor('5',this)" name="title_for_promotor[5]">
        </td>
        <td>
            <input type="text" class="form-control quantity5" name="quantity[5]" placeholder="Ingresar cantidad" readonly>
        </td>
    </tr>


    </tbody>












</table>

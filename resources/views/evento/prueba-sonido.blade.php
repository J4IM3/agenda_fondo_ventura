<div class="modal" tabindex="-1" role="dialog" id="modal-sound-test">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agregar prueba de sonido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-create-sound-test" >
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-12 input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt"></i></span>
                        </div>
                        <input type="text" name="fecha" required class="form-control item-fecha datepicker" placeholder="Ingresar fecha prueba sonido" aria-label="Nombre" aria-describedby="basic-addon1">
                        <div class="col-md-12">
                            <span  class="error-fecha" style="color: red;"> </span>
                        </div>
                    </div>

                    <div class="col-md-6 input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Hora inicio</span>
                        </div>
                        <input type="time" name="hora_inicio"  required class="form-control item-hora_inicio"  placeholder="Ingresar hora inicio" aria-label="Nombre" aria-describedby="basic-addon1">
                        <div class="col-md-12">
                            <span  class="error-hora_inicio" style="color: red;"> </span>
                        </div>
                    </div>

                    <div class="col-md-6 input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Hora fin</span>
                        </div>
                        <input type="time" name="hora_fin" required  class="form-control item-hora_fin"  placeholder="Ingresar hora inicio" aria-label="Nombre" aria-describedby="basic-addon1">
                        <div class="col-md-12">
                            <span  class="error-hora_fin" style="color: red;"> </span>
                        </div>
                    </div>

                    <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername"></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
                            </div>
                            <select class="form-control form-control-chosen item-foro_id_prueba_sonido" required = "required" name="foro_id" data-placeholder="Seleccionar foro">
                                @foreach($foros as $foro)
                                    <option value="{{ $foro->id }}">{{$foro->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span  class="error-foro_id_prueba_sonido" style="color: red;"> </span>
                        </div>
                    </div>

                    <input type="hidden" class="input-evento-id" name="evento_id">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btnFetch">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
            </form>
        </div>
    </div>
</div>


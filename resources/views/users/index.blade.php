@extends('layouts.app')
@section('content')
    <div class="container">

        <button class="btn btn-primary new-user"> <i class="fas fa-user-plus"></i> Nuevo usuario </button>
        <br><br>
        <table class="table table-bordered table-striped" id="table_users">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Opciones</th>
            </tr>
            </thead>
        </table>
    </div>
    @include('users.modal-new-user')
    @include('users.modal-edit-user')
    @endsection
<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" name="name"  class="form-control item-name" placeholder="Nombre" aria-label="Nombre" aria-describedby="basic-addon1">
</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-name"></span>
</div>
<br>

<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-at"></i></span>
    </div>
    <input type="email" name="email"  class="form-control item-email" placeholder="Email" value="admin@admin.com" aria-label="Nombre" aria-describedby="basic-addon1">
</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-email" data-type="email"></span>
</div>
<br>

<div class="input-group mb-3 section-password">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-unlock-alt"></i></span>
    </div>
    <input type="password" name="password" class="form-control" placeholder="Ingresar contraseña " aria-label="Nombre" aria-describedby="basic-addon1">
</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-password"></span>
</div>
<br>

<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text"  id="basic-addon1"><i class="fas fa-users"></i></span>
    </div>
    <input type="text" class="form-control item-perfil" name="perfil" placeholder="Perfil" aria-label="Nombre" aria-describedby="basic-addon1">
</div>

<div class="col-md-12">
    <span class="badge badge-danger" id="error-perfil"></span>
</div>







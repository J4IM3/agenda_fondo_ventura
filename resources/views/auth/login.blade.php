@extends('layouts.app')
@section('content')
    <div class="container">
        <main>
            <div class="container-fluid">
                <div class="row pt-5">

                    <div class="col-md-10 offset-md-1 row flotante">
                        <div class="col-sm-6 login-section-wrapper">
                            <div class="login-wrapper my-auto">
                                <h1 class="login-title" style="text-align: center;padding-top: 20px">
                                    <img src="{{ asset('img/filo_logo.jpeg') }}" alt="Logo Proveedora" height="50px" alt="">
                                </h1>
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <label class="sr-only" for="inlineFormInputGroup">Email</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fas fa-at"></i>
                                            </div>
                                        </div>
                                        <input type="email"  name="email" id="email" class="form-control" id="inlineFormInputGroup" placeholder="Email">
                                    </div>

                                    <br>

                                    <label class="sr-only" for="inlineFormInputGroup">Password</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fas fa-lock"></i>
                                            </div>
                                        </div>
                                        <input type="password" class="form-control" id="password" name="password" id="inlineFormInputGroup" placeholder="Password">
                                    </div>
                                    <br>
                                    <input name="login" id="login" class="btn btn-block login-btn" type="submit" value="Login">
                                </form>
                                <br>

                                <div class="login-wrapper row">
                                    {{--<div class="col-md-8">
                                        <a href="#" class="btn btn-link footer-text-style">¿Olvidaste tu password?</a>
                                    </div>
                                    <div class="col-md-4">
                                        <button class="btn btn-link footer-text-style" id="oppen-modal-register">Registrate</button>
                                    </div>--}}
                                </div>
                                <br>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-6 hide-img" style="padding-top: 10px;padding-left: 102px">
                            <div id="carouselExampleControls carrusel-properties" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active ">
                                        <img class="d-block img-fluid img-form w-100 img-carrusel" src="{{ asset('img/filo1.jpeg') }}" alt="La Proveedora">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block img-fluid img-form w-100 img-carrusel" src="{{ asset('img/filo2.jpeg') }}" alt="La Proveedora">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

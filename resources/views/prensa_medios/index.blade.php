@extends('layouts.app')
@section('content')
    <div class="container">
        <h4>Listado de medios</h4>
        <button class="btn btn-primary nuevo-medio">Nuevo medio</button>
        <br><br>
        @include('prensa_medios.modal-nuevo-medio')
        @include('prensa_medios.modal-editar-medio')
        <table class="table table-bordered table-striped table-responsive-md" id="table-medios">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Opciones</th>
            </tr>
            </thead>
        </table>
    </div>
    @endsection
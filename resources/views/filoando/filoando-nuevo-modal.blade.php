<div class="modal" tabindex="-1" role="dialog" id="modal-show-filoando">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Filoando</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-nuevo-filoando" >
            <div class="modal-body form-row">
                @include('filoando.fields')
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary send-data-user btnFetch">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

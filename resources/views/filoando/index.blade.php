@extends('layouts.app')
@section('content')
    <div class="container">
        <button class="btn btn-primary filoando-nuevo">Nuevo evento</button>
        <br><br>
        @include('filoando.filoando-nuevo-modal')
        @include('filoando.filoando-editar-modal')
        <table class="table table-striped table-bordered table-filoando" >
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Fecha</th>
                    <th>Hora traslado ida</th>
                    <th>Hora traslado retorno</th>
                    <th>Institución</th>
                    <th>Invitados</th>
                    <th style="width: 120px">Opciones</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

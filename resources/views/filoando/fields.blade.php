<div class="col-md-12 mb-3">
    <label for="validationCustomUsername">Seleccionar invitado</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen chosen-select item-invitado_id" name="invitado_id[]" multiple data-placeholder="Seleccion invitado(s)" single>
            @foreach($invitados as $invitado)
                <option value="{{ $invitado->id }}">{{$invitado->name}}</option>
            @endforeach
        </select>
    </div>
</div>

<hr>
<div class="col-md-12" align="center" >
    <hr> <h5>Datos institución</h5> <hr>
</div>

<hr>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Seleccionar institución </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-map-marked"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen chosen-select item-institucion_id" name="institucion_id" placeholder="Seleccione .." data-placeholder="Seleccion institución" single>
            <option value="">Seleccione</option>
            @foreach($instituciones as $institucion)
                <option value="{{ $institucion->id }}">{{$institucion->nombre}}</option>
            @endforeach
        </select>
    </div>
</div>


<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Nivel educativo</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-hiking"></i></span>
        </div>
        <input type="text" class="form-control item-nivel" name="nivel" placeholder="Ingresar nivel educativo" aria-describedby="inputGroupPrepend" required>
    </div>
    <span class="error-actividad badge badge-danger"></span>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Dirección</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-clock"></i></span>
        </div>
        <input type="text" class="form-control item-direccion" name="direccion" placeholder="Ingresar dirección"  required>
    </div>
    <span class="error-fecha badge badge-danger"></span>
</div>

<div class="col-md-6 mb-3">
    <label for="validationCustomUsername">Contacto institución</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-hiking"></i></span>
        </div>
        <input type="text" class="form-control item-contacto" name="contacto" placeholder="Ingresar nombre contacto" aria-describedby="inputGroupPrepend" required>
    </div>
    <span class="error-actividad badge badge-danger"></span>
</div>

<div class="col-md-6 mb-3">
    <label for="validationCustomUsername">Telefono contacto</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-hiking"></i></span>
        </div>
        <input type="text" class="form-control item-telefono_contacto" name="telefono_contacto" placeholder="Ingresar telefono contacto" aria-describedby="inputGroupPrepend" required>
    </div>
    <span class="error-actividad badge badge-danger"></span>
</div>



<div class="col-md-12" align="center" >
    <hr> <h5>Datos evento</h5> <hr>
</div>
<div class="col-md-2 mb-3">
    <label for="validationCustomUsername">Fecha</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-clock"></i></span>
        </div>
        <input type="text" class="form-control datepicker item-fecha" name="fecha" placeholder="Ingresar fecha"  required>
    </div>
    <span class="error-fecha badge badge-danger"></span>
</div>

<div class="col-md-2 mb-3">
    <label for="validationCustomUsername">Hora salida traslado </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_traslado_ida" name="hora_traslado_ida" placeholder="Ingresar hora traslado ida"  required>
    </div>
    <span class="error-fecha badge badge-danger"></span>
</div>


<div class="col-md-2 mb-3">
    <label for="validationCustomUsername">Hora inicio evento</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_inicio_evento" placeholder="Ingresar hora inicio" name="hora_inicio_evento" required>
    </div>
    <span class="error-hora_inicio badge badge-danger"></span>
</div>

<div class="col-md-2 mb-3">
    <label for="validationCustomUsername">Hora fin evento</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_final_evento" placeholder="Ingresar hora fin" name="hora_final_evento" required>
    </div>
    <span class="error-hora_fin badge badge-danger"></span>
</div>

<div class="col-md-3 mb-3">
    <label for="validationCustomUsername">Hora aprox finaliza traslado de regreso</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-clock"></i></span>
        </div>
        <input type="time" class="form-control item-hora_traslado_regreso" name="hora_traslado_regreso" placeholder="Ingresar hora traslado regreso"  required>
    </div>
    <span class="error-fecha badge badge-danger"></span>
</div>

<div class="col-md-12" align="center" >
    <hr> <h5>Datos traslado</h5> <hr>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Seleccionar tipo de transporte</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
        </div>
        <select class="form-control form-control-chosen chosen chosen-select item-tipo_transporte" name="tipo_transporte" placeholder="Seleccione .." data-placeholder="Seleccion tipo transporte" single>
            <option value="">Seleccione</option>
            <option value="Filo">FILO</option>
            <option value="Institucion">Institucion</option>
        </select>
    </div>
</div>



<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Ubicacion inicial autor</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-hiking"></i></span>
        </div>
        <input type="text" class="form-control item-ubicacion_inicial" name="ubicacion_inicial" placeholder="Lugar donde recojer al autor" aria-describedby="inputGroupPrepend" required>
    </div>
    <span class="error-actividad badge badge-danger"></span>
</div>

<div class="col-md-4 mb-3">
    <label for="validationCustomUsername">Ubicacion final autor</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-hiking"></i></span>
        </div>
        <input type="text" class="form-control item-ubicacion_final" name="ubicacion_final" placeholder="Lugar donde dejar al autor" aria-describedby="inputGroupPrepend" required>
    </div>
    <span class="error-actividad badge badge-danger"></span>
</div>

<div class="col-md-12" align="center" >
    <hr> <h5>Requerimientos</h5> <hr>
</div>

<div class="col-md-12 mb-3">
    <label for="validationCustomUsername">Requerimientos</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-hiking"></i></span>
        </div>
        <input type="text" class="form-control item-requerimientos" name="requerimientos" placeholder="Ingresar requerimientos" aria-describedby="inputGroupPrepend" required>
    </div>
    <span class="error-actividad badge badge-danger"></span>
</div>


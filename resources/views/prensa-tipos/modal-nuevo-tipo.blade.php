<div class="modal" tabindex="-1" role="dialog" id="modal-nuevo-tipo">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nuevo tipo evento prensa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="" id="form-nuevo-tipo">
                <div class="modal-body">
                    @include('prensa-tipos.fields')
                </div>
                <div class="modal-footer">
                    <button type="Submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@extends('layouts.app')
@section('content')
    <div class="container">
        <h3>Tipos de evento prensa</h3>
        <br>
        <button class="btn btn-success nuevo-tipo-prensa">Nuevo tipo</button>
        <br> <br>
        @include('prensa-tipos.modal-nuevo-tipo')
        @include('prensa-tipos.modal-editar-tipo')
        <table class="table table-bordered table-striped" id="table-tipos-prensa">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Opciones</th>
            </tr>
            </thead>
        </table>
    </div>
    @endsection()

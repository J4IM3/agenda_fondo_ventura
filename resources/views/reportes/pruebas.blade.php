@extends('layouts.app')
@section('content')
    <div class="container">


    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Hora inicio</th>
            <th>Hora fin</th>
            <th>Nombre evento</th>
        </tr>
        </thead>
        <tbody>
        @foreach($eventos as $evento)
        <tr>
            <td>{{ $evento->fecha }}</td>
            <td>{{ $evento->hora_inicio }}</td>
            <td>{{ $evento->hora_fin }}</td>
            <td>{{ $evento->nombre_evento }}</td>
        </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    @endsection
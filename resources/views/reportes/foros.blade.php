@extends('layouts.app')
@section('content')
    <div class="container">
        <h2>Reportes por foros</h2>
        <br>
        <div class="card text-center">
            <div class="card-header" style="background-color: #4298DE;">
                Elegir reporte
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Seleccionar por fecha</h5>
                                <form action="{{ route('reporte-foro-fecha') }}" method="POST">
                                    <div class="form-row">

                                        <div class="col-md-12 ">
                                            <div class="col-md-12 input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt"></i></span>
                                                </div>
                                                <input type="date" name="fecha"  class="form-control " required placeholder="Ingresar fecha" aria-label="Nombre" aria-describedby="basic-addon1">
                                            </div>
                                        </div>
                                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                        <div class="col-md-12 col-sm-12" >
                                            <button type="submit" class="btn btn-success ">Exportar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="card-footer text-muted">
                                Nos retorna un documento de excel con todos los foros,asi como eventos y horarios dependiendo de la fecha seleccinada
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Seleccionar por foro</h5>
                                <form action="{{ route('reporte-por-foro') }}" method="POST">
                                    <div class="form-row">

                                        <div class="col-md-12 ">
                                            <div class="col-md-12 input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1"><i class="fab fa-audible"></i></span>
                                                </div>
                                                <select name="foro_id" id="" class="form-control">
                                                    <option value="" >Seleccionar foro</option>
                                                    @foreach($foros as $foro)
                                                        <option value="{{ $foro->id }}">{{ $foro->nombre }}</option>
                                                        @endforeach
                                                </select>
                                            </div>

                                        </div>

                                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                        <div class="col-md-12 col-sm-12" >
                                            <button type="submit" class="btn btn-success ">Exportar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="card-footer text-muted">
                                Nos retorna un documento de excel con el foro seleccionado asi como sus horarios y eventos
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

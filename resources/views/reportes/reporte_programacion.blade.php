@extends('layouts.app')
@section('content')
    <div class="container">


        <div class="card">
            <div class="card-header">
                Reporte por dia
            </div>
            <div class="card-body">
                <form action="{{ route('reporte-programacion') }}" method="POST">
                    <div class="form-row">

                        <div class="col-md-5 offset-md-2 float-right ">
                            <div class="col-md-12 input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="text" name="fecha"  class="form-control datepicker" required placeholder="Ingresar fecha" aria-label="Nombre" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <div class="col-md-4 col-sm-12 float-md-left" >
                            <button type="submit" class="btn btn-primary ">Buscar</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>



    </div>
    
    
    @endsection
<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" name="nombre"  class="form-control item-nombre-anfitrion" placeholder="Nombre" aria-label="Nombre" aria-describedby="basic-addon1">
</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-name"></span>
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone"></i></span>
    </div>
    <input type="text" name="telefono"  required class="form-control item-telefono-anfitrion" placeholder="Telefono" aria-label="Nombre" aria-describedby="basic-addon1">
</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-telefono"></span>
</div>

<br>



@extends('layouts.app')
@section('content')
    <div class="container">
        <h3>Listado de anfitriones</h3>
        <button class="btn btn-success create-anfitrion">Nuevo</button>
        <br><br>
        @include("anfitrion.modal-create-anfitrion")
        @include("anfitrion.modal-edit-anfitrion")
        <table class="table table-striped table-bordered " id="table-anfitrion">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Telefono</th>
                <th>Opciones</th>
            </tr>
            </thead>
        </table>

    </div>
    @endsection

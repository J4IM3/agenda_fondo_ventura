@extends('layouts.app')
@section('content')
    <div class="container">
        <h3>Listado instituciones</h3>
        <br>
        <button class="btn btn-success crear-institucion">Agregar institución</button>
        <br> <br>
        @include('instituciones.modal-crear-institucion')
        @include('instituciones.modal-editar-institucion')
        <table class="table table-bordered table-striped table-responsive-sm table-responsive-md" id="table-instituciones">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Opciones</th>
                </tr>
            </thead>
        </table>
    </div>
    @endsection()

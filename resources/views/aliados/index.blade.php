@extends('layouts.app')
@section('content')
    <div class="container">
        <h3>Hospedaje - Alimentación</h3>
        <button class="btn btn-success new-aliado ">Nuevo</button>
        <br><br>
        @include('aliados.moda-new-aliado')
        @include('aliados.moda-edit-aliado')
        <table class="table table-bordered table-striped" id="table_aliados">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Direccion</th>
                    <th>Teléfono</th>
                    <th>Contacto</th>
                    <th>Opciones</th>
                </tr>
            </thead>
        </table>

    </div>
    @endsection

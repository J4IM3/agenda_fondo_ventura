<div class="col-md-12 row">

    <input type="hidden" name="id"  required class="form-control item-id-aliados" aria-label="Nombre" aria-describedby="basic-addon1">

    <div class="col-md-12 input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-text-width"></i></span>
        </div>
        <input type="text" name="name"  required class="form-control item-name-aliados" placeholder="Ingresar nombre" aria-label="Nombre" aria-describedby="basic-addon1">
    </div>
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-name-aliados"></span>
    </div>

    <div class="col-md-12 input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-map-marked-alt"></i></span>
        </div>
        <input type="text" name="direccion" required class="form-control item-direccion-aliados" placeholder="Ingresar dirección" aria-label="Nombre" aria-describedby="basic-addon1">
    </div>
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-direccion-aliados"></span>
    </div>


    <div class="col-md-12 input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone"></i></span>
        </div>
        <input type="text" name="telefono" required class="form-control item-telefono-aliados" placeholder="Ingresar telefono" aria-label="Nombre" aria-describedby="basic-addon1">
    </div>
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-telefono-aliados"></span>
    </div>

    <div class="col-md-12 input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-tie" style="color:blue"></i></span>
        </div>
        <input type="text" name="contacto" required class="form-control item-contacto-aliados" placeholder="Ingresar contacto" aria-label="Nombre" aria-describedby="basic-addon1">
    </div>
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-contacto-aliados"></span>
    </div>

    <div class="col-md-12 input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="far fa-hand-pointer"></i></span>
        </div>
        <select name="tipo" required class="form-control item-tipo-aliados">
            <option value="">Elegir tipo de registro</option>
            <option value="restaurant">Restaurant</option>
            <option value="hotel">Hotel</option>
        </select>
    </div>
    <div class="col-md-12">
        <span class="badge badge-danger" id="error-contacto-aliados"></span>
    </div>

</div>
